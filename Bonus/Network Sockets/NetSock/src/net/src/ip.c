/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

#define NIFACES 10

#include "../ip.h"

#include "../../dev/ifcom.h"
#include "../ether.h"
#include "../if.h"
#include "../protocol.h"
#include "../socket.h"

#include <stddef.h>
#include <string.h>

extern void debug_printline(const char* line);
extern void debug_print(const char* line);
extern void debug_dump(unsigned char* data, int len);

extern int sock_filter(int ps, int family, int type, int proto, struct iface* ifs, unsigned char* data);
extern int sock_data_recv(struct socket* sock, unsigned char* data, int len);
extern int sock_if_list(int type, struct iface* ifs[], int len);

struct iface* iflist[NIFACES + 1];

static unsigned short ip_id_count = 57920; /* just needs to be a random number. Is incremented every send to a new destination */

int ip_dissect(int psw, int family, int type, int proto, struct iface* ifs, unsigned char* data)
{
    /* skip passed ethernet header */
    struct ip* ipdata = (struct ip*)(data + sizeof(struct ether_header));

    /* Check if the IP address can be processed */
    if (ifs->ifr_flags & IFF_PROMISC ||                            /* Promiscuous mode OR */
        (ipdata->ip_dst.s_addr == ifs->ifr_addr.s_addr) ||         /* Destination is our ip OR */
        ((ifs->ifr_flags & IFF_BROADCAST) &&                       /* Broadcast is allowed AND */
          ((ipdata->ip_dst.s_addr == ifs->ifr_broadaddr.s_addr) || /* The destination is our local broadcast address OR */
            (ipdata->ip_dst.s_addr == INADDR_BROADCAST))))         /* The destination is a global broadcast */
    {
        if (ifs->ifr_flags & IFF_DEBUG)
            debug_printline("IP: Received IP Packet");

        switch (ipdata->ip_p)
        {
        case IPPROTO_UDP:
            {
                if (ifs->ifr_flags & IFF_DEBUG)
                    debug_printline("IP: Received UDP Packet");

                proto_dissect(PS_IP_UDP, PF_INET, SOCK_DGRAM, IPPROTO_UDP, ifs, (unsigned char*)ipdata);

                break;
            }
        case IPPROTO_UDPLITE:
            {
                if (ifs->ifr_flags & IFF_DEBUG)
                    debug_printline("IP: Received UDPLite Packet");

                proto_dissect(PS_IP_UDPLITE, PF_INET, SOCK_DGRAM, IPPROTO_UDPLITE, ifs, (unsigned char*)ipdata);

                break;
            }
        default:
            break;
        }

        sock_filter(PS_IP_ALL, PF_INET, SOCK_RAW, IPPROTO_RAW, ifs, (unsigned char*)ipdata);
        return 0;
    }
    if (ifs->ifr_flags & IFF_DEBUG)
        debug_printline("IP: Rejected Packet (wrong address)");
    return -1;
}

int ip_build(int psw, int family, int type, int proto, struct iface* ifs, unsigned char* data, int len, struct sockaddr* src_addr, struct sockaddr* dst_addr)
{
    struct ip* ipdata = (struct ip*)(data);
    unsigned char* payload = (unsigned char*)(data);
    int payload_len = len;

    /* sanity check on data size, we somewhat pretend we don't know about lower
     * protocols, so don't worry about those right now */
    if (len > ETH_DATA_LEN)
        return -1;

    /* step 1: add ip header if there's an actual defined protocol */
    if (proto != IPPROTO_RAW)
    {
        /* if there's no source, then we can't send */
        if (src_addr == NULL)
            return -1;

        /* if there's no destination, then we can't send */
        if (dst_addr == NULL)
            return -1;

        /* step 2: move data back to fit the ip header we're about to add */
        payload = (unsigned char*)(data + sizeof(struct ip));
        payload_len = len + sizeof(struct ip);

        if (payload_len > ETH_DATA_LEN)
            return -1;

        memmove(payload, data, len);

        /* step 3: fill in the ip header */
        ipdata->ip_vhl = 0x45;
        ipdata->ip_tos = 0x00;
        ipdata->ip_len = htons(len + sizeof(struct ip));
        ipdata->ip_id = htons(ip_id_count++);
        ipdata->ip_off = 0;
        ipdata->ip_ttl = 128; /* 128 */
        ipdata->ip_p = proto;
        ipdata->ip_sum = 0x0000; /* we run the sum just before handing it off as
                                    addresses may change */
        ipdata->ip_src.s_addr = ((struct sockaddr_in*)src_addr)->sin_addr.s_addr;
        ipdata->ip_dst.s_addr = ((struct sockaddr_in*)dst_addr)->sin_addr.s_addr;
    }

    /* On send, any == broadcast */
    if (ipdata->ip_dst.s_addr == INADDR_ANY)
        ipdata->ip_dst.s_addr = INADDR_BROADCAST;

    /* step 4: grab the current list of all interfaces */
    memset(&iflist[0], 0, sizeof(iflist));
    if_list(&iflist[0], NIFACES);

    int ret = 0;

    /* step 4: for each interface */
    int found_iface = 0;
    int sent_on[NIFACES];
    memset(sent_on, 0, sizeof(sent_on));

    int idx = 0;
    struct iface* ifr;
    while ((ifr = iflist[idx++]))
    {
        /* if this interface is marked as up */
        if (ifr->ifr_flags & IFF_UP)
        {
            /* packet is short enough to be sent on this interface */
            if (len + sizeof(struct ip) < ifr->ifr_mtu)
            {
                /* If there is an inet network on this interface */
                if (ifr->ifr_addr.s_addr &&
                      /* If the interface is in promiscious mode */
                      ifr->ifr_flags & IFF_PROMISC ||
                    /* or if our destination address is on a subnet used by this ifr */
                    ((ipdata->ip_dst.s_addr & ifr->ifr_netmask.s_addr) == (ifr->ifr_addr.s_addr & ifr->ifr_netmask.s_addr)) ||
                    /* or if our destination is the interface's broadcast address or global broadcast address*/
                    (ipdata->ip_dst.s_addr == ifr->ifr_broadaddr.s_addr || ipdata->ip_dst.s_addr == INADDR_BROADCAST) ||
                    /* or if our destination address is on a subnet available on the gateway connected to this ifr */
                    ((ipdata->ip_dst.s_addr & ifr->ifr_netmask.s_addr) == (ifr->ifr_gateaddr.s_addr & ifr->ifr_netmask.s_addr)))
                {
                    if (ipdata->ip_src.s_addr == INADDR_ANY)
                    {
                        ipdata->ip_src.s_addr = ifr->ifr_addr.s_addr;
                    }

                    /* if it's not raw (i.e. we made the header) set the source and
                     * calculate checksum */
                    if (proto != IPPROTO_RAW)
                    {
                        ipdata->ip_sum = 0x0000;
                        unsigned short* csp = (unsigned short*)ipdata; /* this is aligned. Go away GCC */
                        unsigned long cs = 0;
                        for (unsigned int i = 0;
                          i < (sizeof(struct ip) / sizeof(unsigned short)); i++)
                        {
                            cs += ntohs(*csp);
                            if (cs >> 16)
                                cs = (cs & 0xFFFF) + (cs >> 16);
                            csp++;
                        }
                        ipdata->ip_sum = htons((~cs & 0xFFFF));
                    }

                    if (ifr->ifr_type == IFT_ETHER || ifr->ifr_type == IFT_WLAN)
                    {
                        /*
                            Do something here to generate a source/destination MAC address from ARP caching
                        */

                        /* if this wasn't promisuous, flag that we found an iface */
                        if (!(ifr->ifr_flags & IFF_PROMISC))
                            found_iface++;

                        /* set temp flag that we sent on this one (even if promisc) */
                        sent_on[idx]++;

                        /* step 5: hand off to ethernet protocol on this interface */
                        if (proto_build(PS_ETH_ALL, PF_PACKET, SOCK_RAW, htons(ETH_P_IP), ifr, data, payload_len, NULL, NULL) < 0)
                        {
                            ret |= -1;
                        }
                    }
                }
            }
        }
    }

    /* step 5: if we haven't found a valid interface to send on, then we just send on all of them */
    if (!found_iface)
    {
        idx = 0;
        while ((ifr = iflist[idx++]))
        {
            /* if this interface is marked as up */
            if (ifr->ifr_flags & IFF_UP)
            {
                /* packet is short enough to be sent on this interface */
                if (len + sizeof(struct ip) < ifr->ifr_mtu)
                {
                    /* If there is an inet network on this interface */
                    if (ifr->ifr_addr.s_addr && !sent_on[idx])
                    {
                        if (ipdata->ip_src.s_addr == INADDR_ANY)
                        {
                            ipdata->ip_src.s_addr = ifr->ifr_addr.s_addr;
                        }

                        /* if it's not raw (i.e. we made the header) set the source and
                         * calculate checksum */
                        if (proto != IPPROTO_RAW)
                        {
                            ipdata->ip_sum = 0x0000;
                            unsigned short* csp = (unsigned short*)ipdata; /* this is aligned. Go away GCC */
                            unsigned long cs = 0;
                            for (unsigned int i = 0;
                              i < (sizeof(struct ip) / sizeof(unsigned short)); i++)
                            {
                                cs += ntohs(*csp);
                                if (cs >> 16)
                                    cs = (cs & 0xFFFF) + (cs >> 16);
                                csp++;
                            }
                            ipdata->ip_sum = htons((~cs & 0xFFFF));
                        }

                        if (ifr->ifr_type == IFT_ETHER || ifr->ifr_type == IFT_WLAN)
                        {
                            /*
                                Do something here to generate a source/destination MAC address from ARP caching
                            */

                            /* step 5: hand off to ethernet protocol on this interface */
                            if (proto_build(PS_ETH_ALL, PF_PACKET, SOCK_RAW, htons(ETH_P_IP), ifr, data, payload_len, NULL, NULL) < 0)
                            {
                                ret |= -1;
                            }
                        }
                    }
                }
            }
        }
    }

    return ret;
}

int ip_filter(struct socket* sock, struct iface* ifs, unsigned char* data)
{
    struct sockaddr_in* saddr = (struct sockaddr_in*)&sock->s_addr;
    struct sockaddr_in* daddr = (struct sockaddr_in*)&sock->s_daddr;

    struct ip* ipdata = (struct ip*)data;

    /* if we're bound to receive as a specific address, and this is not for that
     * address and not a broadcast */
    if (sock->s_flag & SF_BIND && saddr->sin_addr.s_addr != INADDR_ANY &&
        saddr->sin_addr.s_addr != ipdata->ip_dst.s_addr &&
        ipdata->ip_dst.s_addr != INADDR_BROADCAST &&
        ipdata->ip_dst.s_addr != ifs->ifr_broadaddr.s_addr)
    {
        return -1;
    }

    /* if we're connected to receive from a specific address, and this was not
     * sent by that address */
    if (sock->s_flag & SF_CONN && daddr->sin_addr.s_addr != INADDR_ANY &&
        daddr->sin_addr.s_addr != ipdata->ip_src.s_addr)
    {
        return -1;
    }

    /* if not raw protocol, and not blank protocol, and not matching the packet's
     * protocol */
    if (sock->s_rawproto != IPPROTO_RAW && sock->s_rawproto != IPPROTO_IP &&
        sock->s_rawproto != ipdata->ip_p)
    {
        return -1;
    }

    sock_data_recv(sock, data, ntohs(ipdata->ip_len));
    return 0;
}

/*
    Open a UDP socket (create new ephemeral port on socket s_addr)
*/
int ip_open(struct socket* sock, int flag)
{
    /* setup a default binding */
    ((struct sockaddr_in*)(&sock->s_addr))->sin_family = AF_INET;
    ((struct sockaddr_in*)(&sock->s_addr))->sin_port = 0;
    ((struct sockaddr_in*)(&sock->s_addr))->sin_addr.s_addr = htonl(INADDR_ANY);

    ((struct sockaddr_in*)(&sock->s_daddr))->sin_family = AF_INET;
    ((struct sockaddr_in*)(&sock->s_daddr))->sin_port = 0;
    ((struct sockaddr_in*)(&sock->s_daddr))->sin_addr.s_addr = htonl(INADDR_ANY);

    if (sock->s_rawproto == IPPROTO_RAW)
        sock->s_opts |= IP_HDRINCL;

    sock->s_flag &= ~(SF_BIND | SF_CONN);

    return 0;
}

/*
    Close a UDP socket (do nothing)
*/
int ip_close(struct socket* sock, int flag)
{
    return 0;
}

/*
    Bind a UDP socket to a specific port
*/
int ip_bind(struct socket* sock, const struct sockaddr* addr, int addrlen)
{
    /*
        If this is a UDP socket
    */
    if (sock->s_family == PF_INET && sock->s_type == SOCK_RAW && sock->s_protocol == IPPROTO_RAW)
    {
        /* and the right address length */
        if (addrlen >= sizeof(struct sockaddr_in))
        {
            /* and the address family is correct */
            if (addr->sa_family == AF_INET)
            {
                /* copy into socket address */
                memcpy(&sock->s_addr, addr, sizeof(struct sockaddr_in));

                sock->s_flag |= SF_BIND;
            }
            else if (addr->sa_family == AF_UNSPEC)
            {
                /* copy into socket address */
                memset(&sock->s_addr, 0, sizeof(struct sockaddr_in));

                /* setup a default binding */
                ((struct sockaddr_in*)(&sock->s_addr))->sin_family = AF_INET;
                ((struct sockaddr_in*)(&sock->s_addr))->sin_port = 0;
                ((struct sockaddr_in*)(&sock->s_addr))->sin_addr.s_addr =
                  htonl(INADDR_ANY);

                sock->s_flag &= ~SF_BIND;
            }
            else
            {
                return -1;
            }
        }
    }
    return 0;
}

/*
    Connect a UDP socket to a specific port and address
*/
int ip_connect(struct socket* sock, const struct sockaddr* addr, int addrlen)
{
    /*
        If this is a UDP socket
    */
    if (sock->s_family == PF_INET && sock->s_type == SOCK_RAW &&
        sock->s_protocol == IPPROTO_RAW)
    {
        /* and the right address length */
        if (addrlen >= sizeof(struct sockaddr_in))
        {
            /* and the address family is correct */
            if (addr->sa_family == AF_INET)
            {
                /* copy into socket destination address */
                memcpy(&sock->s_daddr, addr, sizeof(struct sockaddr_in));

                sock->s_flag |= SF_CONN;
            }
            else if (addr->sa_family == AF_UNSPEC)
            {
                /* copy into socket address */
                memset(&sock->s_daddr, 0, sizeof(struct sockaddr_in));

                /* setup a default binding */
                ((struct sockaddr_in*)(&sock->s_daddr))->sin_family = AF_INET;
                ((struct sockaddr_in*)(&sock->s_daddr))->sin_port = 0;
                ((struct sockaddr_in*)(&sock->s_daddr))->sin_addr.s_addr =
                  htonl(INADDR_ANY);

                sock->s_flag &= ~SF_CONN;
            }
            else
            {
                return -1;
            }
        }
    }
    return 0;
}

/*
    Receive bytes into buf from data and fill in src_addr as appropriate
*/
int ip_recvfrom(struct socket* sock, unsigned char* data, unsigned char* buf,
  int len, int flags, struct sockaddr* src_addr, int* addrlen)
{
    /*
        By this point, we have already checked what was valid before copying it
       into the packet buffer for this socket, so this should be 100% just for us.
    */
    if (sock->s_family == PF_INET)
    {
        struct ip* ipdata = (struct ip*)data;
        unsigned char* payload = data;
        int payload_len = ntohs(ipdata->ip_len);

        /* if we have a specific protocol, and we haven't said to keep the IP
         * header, then skip passed it */
        if (!(sock->s_opts & IP_HDRINCL))
        {
            payload += sizeof(struct ip);
            payload_len -= sizeof(struct ip);
        }

        if (src_addr && *addrlen >= sizeof(struct sockaddr_in))
        {
            struct sockaddr_in* saddr = (struct sockaddr_in*)src_addr;
            saddr->sin_family = AF_INET;
            saddr->sin_addr.s_addr = ipdata->ip_src.s_addr;
            *addrlen = sizeof(struct sockaddr_in);
        }

        int rlen = 0;
        for (; rlen < len && rlen < payload_len; rlen++)
        {
            buf[rlen] = payload[rlen];
        }

        return rlen;
    }
    return -1;
}

int ip_sendto(struct socket* sock, unsigned char* data, unsigned char* buf,
  int len, int flags, struct sockaddr* dst_addr, int* addrlen)
{
    if (sock->s_family == PF_INET)
    {
        struct ip* ipdata = (struct ip*)(data);

        /* step 1: copy the passed buffer into the data buffer */
        if (data != buf)
            memmove(data, buf, len);

        int proto = sock->s_rawproto;

        /* if a header is supposedly supplied already, spoof protocol to raw to
         * avoid build adding it */
        if (sock->s_opts & IP_HDRINCL)
        {
            proto = IPPROTO_RAW;
        }

        /* if we have a destination */
        if (dst_addr != NULL)
            return proto_build(PS_IP_ALL, sock->s_family, sock->s_type, proto, NULL, data, len, &sock->s_addr, dst_addr);

        /* if we're connected and weren't given a destination, the destination is
         * the connection */
        if (sock->s_flag & SF_CONN)
            return proto_build(PS_IP_ALL, sock->s_family, sock->s_type, proto, NULL, data, len, &sock->s_addr, &sock->s_daddr);

        /* lastly, don't set a destination */
        return proto_build(PS_IP_ALL, sock->s_family, sock->s_type, proto, NULL, data, len, &sock->s_addr, NULL);
    }
    return -1;
}
