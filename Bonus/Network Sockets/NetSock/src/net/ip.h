/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

#if !IP_H
#define IP_H 1

#include "inet.h"

#ifndef __packed
#define __packed __attribute__((__packed__))
#endif

#ifndef __aligned
#define __aligned(x) __attribute__((__aligned__(x)))
#endif

/* ------------------------------------------------------------------------- */

/* some stuff */
#define INADDR_ANY       (0x00000000)
#define INADDR_BROADCAST (0xFFFFFFFF)
#define INADDR_LOOPBACK  (0x0100007F) /* network order */

#define IPPROTO_IP       0   /* dummy for IP */
#define IPPROTO_ICMP     1   /* internet control message protocol */
#define IPPROTO_IGMP     2   /* group mgmt protocol */
#define IPPROTO_IPV4     4   /* IPv4-in-IPv4 tunnelling */
#define IPPROTO_TCP      6   /* transmission control protocol */
#define IPPROTO_EGP      8   /* exterior gateway protocol */
#define IPPROTO_PIGP     9   /* interior gateway protocol */
#define IPPROTO_PUP      12  /* PUP protocol */
#define IPPROTO_UDP      17  /* user datagram protocol */
#define IPPROTO_IDP      22  /* XNS IDP protocol */
#define IPPROTO_RDP      27  /* Reliable Data Protocol */
#define IPPROTO_TP       29  /* SO Transport Protocol Class 4 */
#define IPPROTO_DCCP     33  /* datagram congestion control protocol */
#define IPPROTO_IPV6     41  /* IPv6-in-IPv4 tunnelling */
#define IPPROTO_ROUTING  43  /* IPv6 routing header */
#define IPPROTO_FRAGMENT 44  /* IPv6 fragmentation header */
#define IPPROTO_RSVP     46  /* resource reservation */
#define IPPROTO_GRE      47  /* General Routing Encap. */
#define IPPROTO_ESP      50  /* SIPP Encap Sec. Payload */
#define IPPROTO_AH       51  /* SIPP Auth Header */
#define IPPROTO_NHRP     54  /* Next Hop Resolution */
#define IPPROTO_MOBILE   55  /* IP Mobility (minimal encapsulation) */
#define IPPROTO_ICMPV6   58  /* ICMPv6 */
#define IPPROTO_NONE     59  /* IPv6 no next header */
#define IPPROTO_DSTOPTS  60  /* IPv6 destination options */
#define IPPROTO_CFTP     62  /* CFTP */
#define IPPROTO_ND       77  /* Sun net disk proto (temp.) */
#define IPPROTO_EIGRP    88  /* Cisco/GXS IGRP */
#define IPPROTO_OSPF     89  /* Open shortest path first */
#define IPPROTO_MTP      92  /* Multicast Transport Protocol */
#define IPPROTO_BEETPH   94  /* IP option pseudo header for BEET	*/
#define IPPROTO_APES     99  /* Any private encryption scheme */
#define IPPROTO_PIM      103 /* Protocol Independent Multicast */
#define IPPROTO_COMP     108 /* Compression Header Protocol */
#define IPPROTO_VRRP     112 /* See also CARP. */
#define IPPROTO_PGM      113 /* PGM Reliable Transport Protocol */
#define IPPROTO_L2TP     115 /* Layer 2 Tunnelling Protocol */
#define IPPROTO_SMP      121 /* Simple message protocol */
#define IPPROTO_SCTP     132 /* Stream Control Transport Protocol */
#define IPPROTO_MOBILEV6 135 /* Mobility Extension Header for IPv6  */
#define IPPROTO_UDPLITE  136 /* user datagram protocol - lite */
#define IPPROTO_MPLS     137 /* MPLS in IP */
#define IPPROTO_ETHERNET 143 /* Segment Routing over IPv6 - registered 2020-01-31, expires 2021-01-31 */
#define IPPROTO_ANY      255 /* Dummy */
#define IPPROTO_RAW      255 /* For Raw IP sockets */

/* Tunneled/psuedo protocols follow here */
#define IPPROTO_MPTCP (256 + IPPROTO_TCP) /* Multipath TCP connection */

/* IP options */
#define IP_TOS            0x01
#define IPTOS_LOWDELAY    0x10
#define IPTOS_THROUGHPUT  0x08
#define IPTOS_RELIABILITY 0x04
#define IP_TTL            0x02
#define IP_HDRINCL        0x03
#define IP_OPTIONS        0x04

/* TCP options */
#define TCP_NODELAY   0x100 /* don't delay send */
#define TCP_MAXSEG    0x200
#define TCP_KEEPALIVE 0x300 /* send KEEPALIVE probes when idle*/
#define TCP_KEEPIDLE  0x400 /* Same as TCP_KEEPALIVE, but use seconds */
#define TCP_KEEPINTVL 0x500 /* Use seconds for get/setsockopt */
#define TCP_KEEPCNT   0x600 /* Use number of probes sent for get/setsockopt */

/* ------------------------------------------------------------------------- */

#define iphdr ip

struct ip {
    char ip_vhl;           /* version << 4 | header length >> 2 */
    char ip_tos;           /* type of service */
    unsigned short ip_len; /* total length */
    unsigned short ip_id;  /* identification */
    unsigned short ip_off; /* fragment offset field */
#define IP_RF      0x8000  /* reserved fragment flag */
#define IP_DF      0x4000 /* dont fragment flag */
#define IP_MF      0x2000 /* more fragments flag */
#define IP_OFFMASK 0x1FFF /* mask for fragmenting bits */
    unsigned char ip_ttl;  /* time to live */
    unsigned char ip_p;    /* protocol */
    unsigned short ip_sum; /* checksum */
    struct in_addr ip_src;
    struct in_addr ip_dst; /* source and dest address */
} __packed;

/* ------------------------------------------------------------------------- */

struct in6_addr {
    unsigned char s6_addr[16]; /* IPv6 address */
} __packed;

#define ip6hdr ip6

struct ip6 {
    unsigned int ip6_flow;   /* 4 bits version, 8 bits TC, 20 bits flow-ID */
    unsigned short ip6_plen; /* payload length */
    unsigned char ip6_nxt;   /* next header */
    unsigned char ip6_hlim;  /* hop limit */
    struct in6_addr ip6_src; /* source address */
    struct in6_addr ip6_dst; /* destination address */
} __packed;

/* ------------------------------------------------------------------------- */

#endif /*IP_H*/
