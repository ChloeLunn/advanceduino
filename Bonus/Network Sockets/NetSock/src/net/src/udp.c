/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

#include "../udp.h"

#include "../../dev/dev.h"
#include "../if.h"
#include "../protocol.h"
#include "../socket.h"

#include <stdlib.h>
#include <string.h>

extern void debug_printline(const char* line);
extern void debug_print(const char* line);
extern void debug_dump(unsigned char* data, int len);

#define EPHEMERAL_PORTS 49152

static volatile int ephemeral = EPHEMERAL_PORTS;

extern int sock_filter(int ps, int family, int type, int proto, struct iface* ifs, unsigned char* data);
extern int sock_data_recv(struct socket* sock, unsigned char* data, int len);

/*
    Just converts SOCK_INET to SOCK_DGRAM when everything matches
*/
int udp_dissect(int psw, int family, int type, int proto, struct iface* ifs, unsigned char* data)
{
    /* UDP only supports one kind of internal protocol at the moment (plain UDP),
     * so we can just hand off to our sockets */
    // debug_print("U");
    sock_filter(psw, family, type, proto, ifs, data);
    return 0;
}

int udp_build(int psw, int family, int type, int proto, struct iface* ifs, unsigned char* data, int len, struct sockaddr* src_addr, struct sockaddr* dst_addr)
{
    struct udphdr* udpdata = (struct udphdr*)(data);
    unsigned char* payload = (unsigned char*)(data + sizeof(struct udphdr));

    /* sanity check on data size, we pretend we don't know about lower protocols
     */
    if (len + sizeof(struct udphdr) > ETH_DATA_LEN)
        return -1;

    /* if there's no source, then we can't send */
    if (src_addr == NULL)
        return -1;

    /* if there's no destination, then we can't send */
    if (dst_addr == NULL)
        return -1;

    /* step 1: move data back to fit the udp header we're about to add */
    memmove(payload, data, len);

    /* step 2: fill in the udp header from the source and destination info */
    udpdata->uh_dport = (((struct sockaddr_in*)dst_addr)->sin_port);
    udpdata->uh_sport = (((struct sockaddr_in*)src_addr)->sin_port);

    if (proto == IPPROTO_UDPLITE)
        udpdata->uh_ulen = 0;
    else
        udpdata->uh_ulen = htons(len + sizeof(struct udphdr));

    udpdata->uh_sum = 0;
    /* set checksum to 0, as not worth the time. Not required. We can't actually
     * guarantee that this is correct anyway because of how the IP layer can
     * change the source and destination fields */

    if (family == PF_INET)
    {
        /* step 3: send off to the IP send to routine -> buffer pointer and the
         * internal data are currently the same */
        return proto_build(PS_IP_ALL, PF_INET, SOCK_DGRAM, proto, NULL /* no interface spec'd */, data, len + sizeof(struct udphdr), src_addr, dst_addr);
    }
    return -1;
}

/*
    Read the provided packet and see if it matches this socket
*/
int udp_filter(struct socket* sock, struct iface* ifs, unsigned char* data)
{
    struct sockaddr_in* saddr = (struct sockaddr_in*)&sock->s_addr;
    struct sockaddr_in* daddr = (struct sockaddr_in*)&sock->s_daddr;

    struct ip* ipdata = (struct ip*)data;
    struct udphdr* udpdata = (struct udphdr*)(data + sizeof(struct ip));

    /* filter out packets not meant for us that were passed through by promiscious for raw sockets */
    if (ifs->ifr_flags & IFF_PROMISC)
    {
        if ((ipdata->ip_dst.s_addr != ifs->ifr_addr.s_addr) &&         /* Destination is not our ip AND NOT */
            !((ifs->ifr_flags & IFF_BROADCAST) &&                      /* Broadcast is allowed AND */
              ((ipdata->ip_dst.s_addr == ifs->ifr_broadaddr.s_addr) || /* The destination is our local broadcast address OR */
                (ipdata->ip_dst.s_addr == INADDR_BROADCAST))))         /* The destination is a global broadcast */
        {
            if (ifs->ifr_flags & IFF_DEBUG)
                debug_printline("UDP: Rejected Packet (let through by promisc)");
            return -1;
        }
    }

    /* if this socket's port matches the destination of the udp packet */
    if (saddr->sin_port != udpdata->uh_dport)
    {
        if (ifs->ifr_flags & IFF_DEBUG)
            debug_printline("UDP: Rejected Packet (wrong port for socket)");
        return -1;
    }

    /* if we're bound to receive as a specific address, and this is not for that
     * address, and also not a broadcast */
    if (sock->s_flag & SF_BIND &&                           /* We're bound */
        saddr->sin_addr.s_addr != INADDR_ANY &&             /* and to a specific address */
        saddr->sin_addr.s_addr != ipdata->ip_dst.s_addr &&  /* and the packet mismatches this */
        ipdata->ip_dst.s_addr != INADDR_BROADCAST &&        /* and the packet wasn't broadcast globally */
        ipdata->ip_dst.s_addr != ifs->ifr_broadaddr.s_addr) /* and the packet wasn't broadcast locally */
    {
        if (ifs->ifr_flags & IFF_DEBUG)
            debug_printline("UDP: Rejected Packet (wrong bind address for socket)");
        return -1;
    }

    /* if we're connected to receive from a specific address, and this was not
     * sent by that address */
    if (sock->s_flag & SF_CONN && daddr->sin_addr.s_addr != INADDR_ANY &&
        daddr->sin_addr.s_addr != ipdata->ip_src.s_addr)
    {
        if (ifs->ifr_flags & IFF_DEBUG)
            debug_printline("UDP: Rejected Packet (wrong connect address for socket)");

        return -1;
    }

    /* if we're connected to receive from a specific source port, and this was not
     * sent by that port */
    if (sock->s_flag & SF_CONN && daddr->sin_port != 0 &&
        daddr->sin_port != udpdata->uh_sport)
    {
        if (ifs->ifr_flags & IFF_DEBUG)
            debug_printline("UDP: Rejected Packet (wrong connect port for socket)");
        return -1;
    }

    if (ifs->ifr_flags & IFF_DEBUG)
        debug_printline("UDP: Received UDP Packet (loading into socket!)");

    sock_data_recv(sock, data, ntohs(ipdata->ip_len));
    return 0;
}

/*
    Open a UDP socket (create new ephemeral port on socket s_addr)
*/
int udp_open(struct socket* sock, int flag)
{
    /* setup a default binding */
    ((struct sockaddr_in*)(&sock->s_addr))->sin_family = AF_INET;
    ((struct sockaddr_in*)(&sock->s_addr))->sin_port = htons(++ephemeral);
    ((struct sockaddr_in*)(&sock->s_addr))->sin_addr.s_addr = htonl(INADDR_ANY);

    ((struct sockaddr_in*)(&sock->s_daddr))->sin_family = AF_INET;
    ((struct sockaddr_in*)(&sock->s_daddr))->sin_port = ((struct sockaddr_in*)(&sock->s_addr))->sin_port;
    ((struct sockaddr_in*)(&sock->s_daddr))->sin_addr.s_addr = htonl(INADDR_ANY);

    sock->s_flag &= ~(SF_BIND | SF_CONN);

    return 0;
}

/*
    Close a UDP socket (do nothing)
*/
int udp_close(struct socket* sock, int flag)
{
    return 0;
}

/*
    Bind a UDP socket to a specific port
*/
int udp_bind(struct socket* sock, const struct sockaddr* addr, int addrlen)
{
    /*
        If this is a UDP socket
    */
    if (sock->s_family == PF_INET && sock->s_type == SOCK_DGRAM &&
        (sock->s_protocol == IPPROTO_UDP || sock->s_protocol == IPPROTO_UDPLITE))
    {
        /* and the right address length */
        if (addrlen >= sizeof(struct sockaddr_in))
        {
            /* and the address family is correct */
            if (addr->sa_family == AF_INET)
            {
                /* copy into socket address */
                memcpy(&sock->s_addr, addr, sizeof(struct sockaddr_in));

                sock->s_flag |= SF_BIND;
            }
            else if (addr->sa_family == AF_UNSPEC)
            {
                /* copy into socket address */
                memset(&sock->s_addr, 0, sizeof(struct sockaddr_in));

                ((struct sockaddr_in*)(&sock->s_addr))->sin_family = AF_INET;
                ((struct sockaddr_in*)(&sock->s_addr))->sin_port = htons(++ephemeral);
                ((struct sockaddr_in*)(&sock->s_addr))->sin_addr.s_addr = htonl(INADDR_ANY);

                sock->s_flag &= ~SF_BIND;
            }
            else
            {
                return -1;
            }
        }
    }
    return 0;
}

/*
    Connect a UDP socket to a specific port and address
*/
int udp_connect(struct socket* sock, const struct sockaddr* addr, int addrlen)
{
    /*
        If this is a UDP socket
    */
    if (sock->s_family == PF_INET && sock->s_type == SOCK_DGRAM &&
        (sock->s_protocol == IPPROTO_UDP ||
          sock->s_protocol == IPPROTO_UDPLITE))
    {
        /* and the right address length */
        if (addrlen >= sizeof(struct sockaddr_in))
        {
            /* and the address family is correct */
            if (addr->sa_family == AF_INET)
            {
                /* copy into socket destination address */
                memcpy(&sock->s_daddr, addr, sizeof(struct sockaddr_in));

                sock->s_flag |= SF_CONN;
            }
            else if (addr->sa_family == AF_UNSPEC)
            {
                /* copy into socket address */
                memset(&sock->s_daddr, 0, sizeof(struct sockaddr_in));

                ((struct sockaddr_in*)(&sock->s_daddr))->sin_family = AF_INET;
                ((struct sockaddr_in*)(&sock->s_daddr))->sin_port = ((struct sockaddr_in*)(&sock->s_addr))->sin_port;
                ((struct sockaddr_in*)(&sock->s_daddr))->sin_addr.s_addr = htonl(INADDR_ANY);

                sock->s_flag &= ~SF_CONN;
            }
            else
            {
                return -1;
            }
        }
    }
    return 0;
}

/*
    Receive bytes into buf from data and fill in src_addr as appropriate
*/
int udp_recvfrom(struct socket* sock, unsigned char* data, unsigned char* buf, int len, int flags, struct sockaddr* src_addr, int* addrlen)
{
    /*
        By this point, we have already checked what was valid before copying it
       into the packet buffer for this socket, so this should be 100% just for us.
    */
    if (sock->s_family == PF_INET)
    {
        struct ip* ipdata = (struct ip*)data;
        struct udphdr* udpdata = (struct udphdr*)(data + sizeof(struct ip));
        unsigned char* payload = (unsigned char*)(data + sizeof(struct ip) + sizeof(struct udphdr));

        if (src_addr && *addrlen >= sizeof(struct sockaddr_in))
        {
            struct sockaddr_in* saddr = (struct sockaddr_in*)src_addr;
            saddr->sin_family = AF_INET;
            saddr->sin_addr.s_addr = ipdata->ip_src.s_addr;
            saddr->sin_port = udpdata->uh_sport;
            *addrlen = sizeof(struct sockaddr_in);
        }

        if (sock->s_protocol == IPPROTO_UDP)
        {
            int rlen = 0;
            for (; rlen < len && rlen < (ntohs(udpdata->uh_ulen) - sizeof(struct udphdr)); rlen++)
            {
                buf[rlen] = payload[rlen];
            }

            return rlen;
        }
        else if (sock->s_protocol == IPPROTO_UDPLITE)
        {
            int rlen = 0;
            for (; rlen < len; rlen++)
            {
                buf[rlen] = payload[rlen];
            }

            return rlen;
        }
    }
    return -1;
}

int udp_sendto(struct socket* sock, unsigned char* data, unsigned char* buf, int len, int flags, struct sockaddr* dst_addr, int* addrlen)
{
    if (sock->s_family == PF_INET)
    {
        /* sanity check on data size, we pretend we don't know about lower
         * protocols, so don't worry about those right now */
        if (len + sizeof(struct udphdr) > ETH_DATA_LEN)
            return -1;

        /* step 1: copy the passed buffer into the payload */
        if (data != buf)
            memmove(data, buf, len);

        /* step 2: send off to the UDP build routine */

        /* if we have a destination */
        if (dst_addr != NULL)
            return proto_build(PS_IP_UDP, sock->s_family, sock->s_type, sock->s_protocol, NULL, data, len, &sock->s_addr, dst_addr);

        /* if we're connected and weren't given a destination, the destination is
         * the connection */
        if (sock->s_flag & SF_CONN)
            return proto_build(PS_IP_UDP, sock->s_family, sock->s_type, sock->s_protocol, NULL, data, len, &sock->s_addr, &sock->s_daddr);

        /* lastly, don't set a destination */
        return proto_build(PS_IP_UDP, sock->s_family, sock->s_type, sock->s_protocol, NULL, data, len, &sock->s_addr, NULL);
    }
    return -1;
}
