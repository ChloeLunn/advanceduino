/* Serial port functions as C wrappers, with cooked tty options */

#include "term.h"

#if !H_CSERIAL
#define H_CSERIAL 1

#ifdef __cplusplus
extern "C" {
#endif

    int cserial_open();
    int cserial_close();

    int cserial_connected();
    void cserial_keepalive();

    int cserial_setopt(struct term_opts* opts);
    int cserial_getopt(struct term_opts* opts);

    int cserial_getc();
    int cserial_gets(char* buf, int len);

    int cserial_putc(int c);
    int cserial_puts(const char* buf, int len);

    int cserial_printf(const char* fmt, ...);

#ifdef __cplusplus
}
#endif
#endif
