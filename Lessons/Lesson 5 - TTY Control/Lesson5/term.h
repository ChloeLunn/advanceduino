/* PORTABLE TTY CONTROL LAYER */

#if !H_TERM
#define H_TERM 1

#ifndef __packed
#define __packed __attribute__((packed))
#endif

struct term_config;

#define CTRL(c) ((c) == '?' ? 127 : (c)&037)

/* maximum line length (receive only, send is cooked OTF) */
#define COOKED_MAX 255

/* struct way of editing control chars */
struct tc {
    char t_erase;         /* erase last character */
    char t_erase2;        /* erase last character */
#define CERASE  CTRL('?') /* erase previous char */
#define CERASE2 CTRL('H') /* erase previous char */

    char t_kill;        /* erase entire line */
#define CKILL CTRL('U') /* erase entire line */

    char t_intr;        /* interrupt */
#define CINTR CTRL('C') /* issue sigint */

    char t_quit;         /* quit */
#define CQUIT CTRL('\\') /* issue sigquit */

    char t_start;        /* start output */
    char t_stop;         /* stop output */
#define CSTART CTRL('Q') /* start transmission */
#define CSTOP  CTRL('S') /* stop transmission */

    char t_eof;        /* end-of-file */
#define CEOF CTRL('D') /* end of file/stream/entry */

    union {
        char t_brk; /* input delimiter (like nl) */
        char t_eol;
    } __packed;
#define CEOL (0377) /* end of line delimeter in queue */
#define CBRK CEOL /* what character to use between input lines in buffer */

    char t_susp;         /* stop process signal */
    char t_dsusp;        /* delayed stop process signal */
#define CSUSP  CTRL('Z') /* issue sigtstp */
#define CDSUSP CTRL('Y') /* issue sigtstp when read() reaches it */

    char t_rprnt;          /* reprint line */
#define CRPRNT   CTRL('R') /* reprint current input line */
#define CREPRINT CRPRNT

    union {
        char t_flush;   /* flush output (toggles) */
        char t_discard; /* discard input */
    } __packed;
#define CFLUSH   CTRL('O') /* drain output queue & discard input queue */
#define CDISCARD CFLUSH

    char t_werase;        /* word erase */
#define CWERASE CTRL('W') /* erase the last word */

    char t_lnext;        /* literal next character */
#define CLNEXT CTRL('V') /* next char should be put in queue as is */

    char t_esc;      /* escape character */
#define CCESC ('\\') /* next char should be processed with escape table */

    char t_eot;   /* end of transmission */
#define CEOT CEOL /* what character to return for no data */

    char t_end_of_list;
} __packed;

/* number of control characters */
#define NCCS (sizeof(struct tc) / sizeof(char))

typedef int (*term_open_t)(void);
typedef int (*term_config_t)(struct term_config* term);
typedef int (*term_read_t)(void);
typedef int (*term_write_t)(int c);
typedef int (*term_close_t)(void);
typedef int (*term_available_t)(void);

/* handle a terminal input event, callback character is the character that prompted the callback */
typedef void (*term_handler_t)(int callback_char);

#define COOKED_RAW  0 /* uncooked */
#define COOKED_NL   1 /* cooked due to new line */
#define COOKED_FULL 2 /* cooked due to buffer full */

/* terminal configuration from the highest level */
struct term_opts {
    char* prompt;                   /* prompt to use if echo is on */
    int lcase;                      /* allow lowercase */
    int echo;                       /* echo characters */
    int xon;                        /* XON/XOFF flow control */
    int speed;                      /* baud rate */
    int timeout;                    /* timeout in ms */
    int raw;                        /* handle the tty raw (no cooking, just put on queue) */
    char cdelay;                    /* character delay multiplier */
    int cr_imp_lf;                  /* treat CRs as having an implicit lf */
    term_handler_t line_callback;   /* called whenver a complete line is entered */
    term_handler_t full_callback;   /* called whenver the buffer is completely full */
    term_handler_t cancel_callback; /* called whenever a cancel input occurs */
    term_handler_t erase_callback;  /* called whenever an erase line occurs */
    struct tc control_chars;        /* control character list */
    int columns;                    /* number of char columns on terminal */
    int rows;                       /* number of char lines on terminal */
    int tabwidth;                   /* number of spaces to expand a tab to */
    int softnewline;                /* do newline if we think we hit the last char in the row */
};

/* Terminal configuration storage */
struct term_config {
    /* prompt to use if echo is on */
    int prpnt;
    char* prompt;

    /* flags/config -- for lower driver controller to set to */
    int speed;   /* baud rate */
    int timeout; /* timeout in ms */
    char cdelay; /* character delay multiplier */

    /* character list controls for tty input layer */
    union {
        struct tc control_chars;
        char c_cc[NCCS]; /* so we can do an isany on this list */
    } __packed;

    /* Used by tty control layer */
    int raw;       /* handle the tty raw (no cooking, just put on queue) */
    int lcase;     /* allow lowercase */
    int echo;      /* echo characters */
    int xon;       /* XON/XOFF flow control */
    int cr_imp_lf; /* treat CRs as having an implicit lf */

    char* ptr;                  /* current read/write position pointer */
    int cooked;                 /* we have a readable cooked line */
    char queue[COOKED_MAX + 1]; /* 1 extra so that we can set it to 0 if full */
    int col_pos;                /* current output column */
    int row_pos;                /* current output row */

    int columns;     /* number of char columns on terminal */
    int rows;        /* number of char lines on terminal */
    int tabwidth;    /* number of spaces to expand a tab to */
    int softnewline; /* do newline if we think we hit the last char in the row */

    /* links to lower level driver funcs, set by upper layer */
    term_open_t open;
    term_config_t config;
    term_read_t read;
    term_write_t write;
    term_close_t close;
    term_available_t available;

    /* essentially signal handlers */
    term_handler_t line_callback;   /* called whenver a complete line is entered */
    term_handler_t full_callback;   /* called whenver the buffer is completely full */
    term_handler_t cancel_callback; /* called whenever a cancel input occurs */
    term_handler_t erase_callback;  /* called whenever an erase line occurs */
};

#ifdef __cplusplus
extern "C" {
#endif

#define DEFAULT_CHARS_LIST                                                                                                                          \
    {                                                                                                                                               \
        CERASE, CERASE2, CKILL, CINTR, CQUIT, CSTART, CSTOP, CEOF, (char)CBRK, CSUSP, CDSUSP, CRPRNT, CFLUSH, CWERASE, CLNEXT, CCESC, (char)CEOT, 0 \
    }

    static const int default_baud = (115200);
    static const int default_timeout = (1000);
    static const struct tc default_controlchars = DEFAULT_CHARS_LIST;

    /* set to default, then call lower driver open & config */
    int ttyopen(struct term_config* term);
    /* alias for calling lower driver close */
    int ttyclose(struct term_config* term);
    /* put a character on output stream */
    int ttyputc(struct term_config* term, int c);
    /* put a string on output stream */
    int ttyputs(struct term_config* term, const char* buf, int length);
    /* get the next character on the input string */
    int ttygetc(struct term_config* term);
    /* get the current input string */
    int ttygets(struct term_config* term, char* buf, int length);
    /* sets up the defaults */
    void ttyinit(struct term_config* term);
    /* needs to be called regularly, but prob not in systick! */
    void ttycook(struct term_config* term);
    /* update lower level config */
    void ttyconfig(struct term_config* term);

    int ttysetopts(struct term_config* term, struct term_opts* opts);
    int ttygetopts(struct term_config* term, struct term_opts* opts);

#ifdef __cplusplus
}
#endif
#endif
