/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

#if !H_PROTOCOL
#define H_PROTOCOL

#include "if.h"
#include "ip.h"
#include "socket.h"

/* check if this data applies to this protocol, and trigger the level up receives as appropriate */
typedef int (*proto_dissect_t)(int psw, int family, int type, int proto, struct iface* ifs, unsigned char* data);

/* build a packet's headers from the passed information */
typedef int (*proto_build_t)(int psw, int family, int type, int proto, struct iface* ifs, unsigned char* data, int len, struct sockaddr* src_addr, struct sockaddr* dst_addr);

/* read the provided packet data and see if it applies to the provided socket */
typedef int (*proto_filter_t)(struct socket* sock, struct iface* ifs, unsigned char* data);

/* set up socket information upon opening one of this protocol */
typedef int (*proto_open_t)(struct socket* sock, int flag);

/* read data out of the socket buffer and into data, fill in src_addr as appropriate */
typedef int (*proto_recvfrom_t)(struct socket* sock, unsigned char* data, unsigned char* buf, int len, int flags, struct sockaddr* src_addr, int* addrlen);

/* send data out from data, using dst_addr as appropriate */
typedef int (*proto_sendto_t)(struct socket* sock, unsigned char* data, unsigned char* buf, int len, int flags, struct sockaddr* src_addr, int* addrlen);

/* close down anything that needs to be handled in-protocol */
typedef int (*proto_close_t)(struct socket* sock, int flag);

/* option settings/gettings */
typedef int (*proto_setopt_t)(struct socket* sock, int level, int option_name, const void* option_value, int option_len);
typedef int (*proto_getopt_t)(struct socket* sock, int level, int option_name, void* option_value, int option_len);

/* bind an address to the socket for this protocol (stored in socket->s_addr) */
typedef int (*proto_bind_t)(struct socket* sock, const struct sockaddr* addr, int addrlen);

/*
   Add a 'patch' function. 
   Called on family, type, protocol before open

   Allows protocols to fix them such that things like

   PF_INET SOCK_DGRAM 0  get converted to AF_INET SOCK_DGRAM IPPROTO_UDP

   -- not needed; this is provided by socket()
*/

struct proto {
    /* protocol id info */
    int p_family;
    int p_type;
    int p_protocol;

    /* protocol dissection and filtering */
    proto_dissect_t p_dissect;
    proto_build_t p_build;
    proto_filter_t p_filter;

    /* open and close for protocol info */
    proto_open_t p_open;
    proto_close_t p_close;

    proto_recvfrom_t p_recvfrom;
    proto_sendto_t p_sendto;

    proto_bind_t p_bind;
    proto_bind_t p_connect;

    proto_setopt_t p_setopt;
    proto_getopt_t p_getopt;
};

#define PS_ETH_ANY    0 /* IF <-> Ethernet */
#define PS_ETH_ALL    1 /* Ethernet <-> Generic Protocol */
#define PS_ETH_IP     2 /* Ethernet <-> IP */
#define PS_IP_ALL     3
#define PS_IP_UDP     4
#define PS_IP_UDPLITE 5
#define PS_MAX        6

extern const struct proto protocols[PS_MAX];

extern int proto_find(int psw, int family, int type, int proto);
extern int proto_find_sock(int psw, struct socket* sock);

/* Protocol specific parsing to allow moving packets down layers */
extern int proto_dissect(int psw, int family, int type, int proto, struct iface* ifs, unsigned char* data);

extern int proto_build(int psw, int family, int type, int proto, struct iface* ifs, unsigned char* data, int len, struct sockaddr* src_addr, struct sockaddr* dst_addr);

/* Protocol specific open handling, e.g. add ephemeral port to udp or tcp sockets */
extern int proto_open(int psw, struct socket* sock, int flag);

/* Protocol specific close handling */
extern int proto_close(int psw, struct socket* sock, int flag);

/* read data and check if it matches sock or not, calls sock_data_recv to add this data to the socket buffer */
extern int proto_filter(int psw, struct socket* sock, struct iface* ifs, unsigned char* data);

/* perform a receive from for a specific socket */
extern int proto_recvfrom(int psw, struct socket* sock, unsigned char* data, unsigned char* buf, int len, int flags, struct sockaddr* src_addr, int* addrlen);

/* perform a send to for a specific socket */
extern int proto_sendto(int psw, struct socket* sock, unsigned char* data, unsigned char* buf, int len, int flags, struct sockaddr* dst_addr, int* addrlen);

extern int proto_setopt(int psw, struct socket* sock, int level, int option_name, const void* option_value, int option_len);
extern int proto_getopt(int psw, struct socket* sock, int level, int option_name, void* option_value, int option_len);

/*
    Bind the receiver to a specific address
*/
extern int proto_bind(int psw, struct socket* sock, const struct sockaddr* addr, int addrlen);

/* 
    Bind the sender to a specific address
*/
extern int proto_connect(int psw, struct socket* sock, const struct sockaddr* addr, int addrlen);

#endif
