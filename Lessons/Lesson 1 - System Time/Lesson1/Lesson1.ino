#include <Ethernet.h>
#include <SPI.h>
#include <time.h>

#define MICROS_PER_SEC   (1000000)
#define MICROS_PER_CLOCK (1000)

/*
    This is example code showing how to implement basic system time control
    with NTP updates.

    This example should work on any AVR, SAMD21, or SAMD51 Arduino or
    Adafruit board.
    
    However, you will need an Ethernet shield/hat/etc. to do the NTP.
    It may be fairly easy to convert the ethernet to wifi, but I haven't tried
    this. It also assumes that the host is little endian.

    Admittedly, I've not actually tested it on AVRs....
*/

/* default system time (Mon Jan 1st, 2001, 00:00) */
const long long EPOCH_DEFAULT_SYS_TOD = 978307200;

/* days of week */
const char* c_days[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

/* month names */
const char* c_months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

/*
    This is our system time in seconds and microseconds
    since Tues 1st Jan 1970 00:00:00.000000
*/
struct timeval sys_tv = {EPOCH_DEFAULT_SYS_TOD, 0};

/* Same thing, but this time as a local posix time - we only update this as required */
struct tm sys_tm;

/* convert a time into a time/date string */
char* time_to_string(const struct tm* tm, char* buf)
{
    if (buf == NULL)
    {
        return NULL;
    }
    if (tm == NULL)
    {
        return NULL;
    }
    sprintf(buf, "%s %s %02d %02d:%02d:%02d %04d\n", c_days[tm->tm_wday], c_months[tm->tm_mon - 1], tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec, tm->tm_year + 1900);
    return buf;
}

/* convert a string into a time */
struct tm* string_to_time(struct tm* tm, const char* buf)
{
    if (buf == NULL)
    {
        return NULL;
    }
    if (tm == NULL)
    {
        return NULL;
    }
    sscanf(buf, "%s %s %02d %02d:%02d:%02d %04d\n", c_days[tm->tm_wday], c_months[tm->tm_mon - 1], tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec, tm->tm_year);
    tm->tm_year -= 1900;
    return tm;
}

/* this function is automatically called once every millisecond */
int sysTickHook(void)
{
    /* increment the system time-of-day micros */
    sys_tv.tv_usec += MICROS_PER_CLOCK;

    /* if we've reached 1 second */
    if (sys_tv.tv_usec >= MICROS_PER_SEC)
    {
        /* decrement the micros */
        sys_tv.tv_usec -= MICROS_PER_SEC;

        /* increment the system time of day seconds value */
        sys_tv.tv_sec += 1;
    }

    return 1;
}

void setup()
{
    /* Do initial set of sys_tm to make it match the default time */
    epoch_to_time(sys_tv.tv_sec, &sys_tm);

    while (!Serial)
    {
        delay(100);
    }

    Serial.println("");
}

unsigned long long last_print = 0;

void loop()
{
    /* If we haven't printed in 1 second */
    if (sys_tv.tv_sec - last_print > 1)
    {
        /* update tm struct */
        epoch_to_time(sys_tv.tv_sec, &sys_tm);

        /* convert to a string */
        char print_buf[26];
        time_to_string(&sys_tm, print_buf);

        /* And print ('\r' so we reprint the same line) */
        Serial.printf("\r\t%s (%ums)", print_buf, sys_tv.tv_usec / 1000);
    }

    /* if there's data on the serial port */
    if (Serial.available())
    {
        char c = (char)Serial.read();

        /* if we received an 'e' that means enter a date */
        if (c == 'e')
        {
            Serial.printf("\r\n>\t");
        }
    }
}
