/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

#define NIFACES 10

#ifndef NULL
#define NULL (void*)0
#endif

#include "../ether.h"

#include "../../dev/dev.h"
#include "../../dev/ifcom.h"
#include "../if.h"
#include "../inet.h"
#include "../ip.h"
#include "../protocol.h"
#include "../socket.h"

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern void debug_printline(const char* line);
extern void debug_print(const char* line);
extern void debug_dump(unsigned char* data, int len);

extern int ip_dissect(int ps, int family, int type, int proto, struct iface* ifs, unsigned char* data);
extern int sock_filter(int ps, int family, int type, int proto, struct iface* ifs, unsigned char* data);
extern int sock_data_recv(struct socket* sock, unsigned char* data, int len);

int memzero(unsigned char* buf, int len)
{
    while (len--)
    {
        if (*buf++)
            return 1;
    }
    return 0;
}

int ether_dissect(int psw, int family, int type, int proto, struct iface* ifs, unsigned char* data)
{
    /* skip passed interface header */
    struct ether_header* ethdata = (struct ether_header*)(data + sizeof(struct ifhdr));

    // in case phy chip hasn't done mac addr filtering
    if (ifs->ifr_flags & IFF_PROMISC ||                                          /* Promiscuous */
        (ethdata->h_dest[0] == 0xFF && ethdata->h_dest[ETH_ALEN - 1] == 0xFF) || /* The destination address is a broadcast address */
        memcmp(ethdata->h_dest, ifs->ifr_hwaddr.m_addr, ETH_ALEN) == 0)          /* The destination address is this if's address */
    {
        if (ifs->ifr_flags & IFF_DEBUG)
            debug_printline("ETH: Received Ether Packet");

        switch (ntohs(ethdata->h_proto))
        {
        case ETH_P_IP6:
            {
                if (ifs->ifr_flags & IFF_DEBUG)
                    debug_printline("ETH: Received IP6 Packet");

                break;
            }
        case ETH_P_ARP:
            {
                if (ifs->ifr_flags & IFF_DEBUG)
                    debug_printline("ETH: Received ARP Packet");

                break;
            }
        case ETH_P_IP:
            {
                if (ifs->ifr_flags & IFF_DEBUG)
                    debug_printline("ETH: Received IP Packet");

                // This is a specific protocol to dissect further, so no need to look for
                // matching sockets
                proto_dissect(PS_ETH_IP, PF_PACKET, SOCK_RAW, htons(ETH_P_IP), ifs, (unsigned char*)ethdata);
                break;
            }
        default:
            break;
        }

        // Send to generic ethernet raw sockets
        sock_filter(PS_ETH_ALL, PF_PACKET, SOCK_RAW, htons(ETH_P_ALL), ifs, data);

        return 0;
    }
    else
    {
        if (ifs->ifr_flags & IFF_DEBUG)
            debug_printline("ETH: Rejected Packet (wrong address)");
    }

    return -1;
}
int ether_build(int psw, int family, int type, int proto, struct iface* ifr, unsigned char* data, int len, struct sockaddr* src_addr, struct sockaddr* dst_addr)
{
    struct ether_header* etherdata = (struct ether_header*)(data);
    unsigned char* payload = (unsigned char*)(data);
    int payload_len = len;

    if (len > ETH_FRAME_LEN)
        return -1;

    /* if there's no source, no destination, and no interface to send on, then we can't send */
    if (src_addr == NULL && dst_addr == NULL && ifr == NULL)
        return -1;

    /* step 1: if this isn't a raw transaction, then we will need to add a header */
    if (proto != ETH_P_ALL && proto != ETH_P_INTERNAL_ANY)
    {
        /* step 2: move data back to fit the ethernet header we're about to add */
        payload = (unsigned char*)(data + sizeof(struct ether_header));
        payload_len = len + sizeof(struct ether_header);

        memmove(payload, data, len);

        /* sanity check on data size */
        if (payload_len > ETH_FRAME_LEN)
            return -1;

        /* set protocol */
        etherdata->h_proto = proto;

        /* if we have a destination, use it, otherwise broadcast */
        if (dst_addr)
        {
            memcpy(etherdata->h_source, ((struct sockaddr_ll*)dst_addr)->sll_addr, ETH_ALEN);
        }
        else
        {
            memset(etherdata->h_dest, 0xFF, ETH_ALEN);
        }

        /* if we have a source, use it, otherwise set to 0s and it'll automatically use the interface one */
        if (src_addr)
        {
            memcpy(etherdata->h_source, ((struct sockaddr_ll*)src_addr)->sll_addr, ETH_ALEN);
        }
        else
        {
            memset(etherdata->h_source, 0x00, ETH_ALEN);
        }
    }

    /* if we've been told to send on a specific interface already, then do this */
    if (ifr != NULL)
    {
        /* if the source wasn't set, set it to this interface */
        if (memzero(etherdata->h_source, ETH_ALEN) == 0)
            memcpy(etherdata->h_source, ifr->ifr_hwaddr.m_addr, ETH_ALEN);

        /* and send */
        return if_transmit(ifr, data, payload_len);
    }

    /* we're bound to a specific interface, find it and send on it */
    if (src_addr && ((struct sockaddr_ll*)src_addr)->sll_ifindex != IF_IDX_ANY)
    {
        struct iface* ifs[NIFACES + 1];
        memset(&ifs[0], 0, sizeof(ifs));
        if_list(&ifs[0], NIFACES);

        /* for each interface */
        int ifidx = 0;
        while ((ifr = ifs[ifidx]) != NULL)
        {
            ifidx++;

            /* if the index matches the wanted index */
            if (ifr->ifr_ifindex == ((struct sockaddr_ll*)src_addr)->sll_ifindex)
            {
                /* if this interface is marked as up */
                if (ifr->ifr_flags & IFF_UP)
                {
                    if (ifr->ifr_type == IFT_ETHER || ifr->ifr_type == IFT_WLAN)
                    {
                        /* packet is short enough to be sent on this interface */
                        if (len < ifr->ifr_mtu)
                        {
                            /* if the source wasn't set, set it to this interface */
                            if (memzero(etherdata->h_source, ETH_ALEN) == 0)
                                memcpy(etherdata->h_source, ifr->ifr_hwaddr.m_addr, ETH_ALEN);

                            return if_transmit(ifr, data, payload_len);
                        }
                    }
                }

                break;
            }
        }

        return -1;
    }

    /* if the interface is null, then we need to fire off on all promiscuous interfaces and any that match the source address */
    else
    {
        int ret = 0;

        unsigned char temp_source[ETH_ALEN];
        memcpy(temp_source, etherdata->h_source, ETH_ALEN);

        /* step 3.1.1: grab a list of all interfaces */
        struct iface* ifs[NIFACES + 1];
        memset(&ifs[0], 0, sizeof(ifs));
        if_list(&ifs[0], NIFACES);

        /* step 4: for each interface */
        int ifidx = 0;
        while ((ifr = ifs[ifidx]) != NULL)
        {
            ifidx++;

            /* if this interface is marked as up */
            if (ifr->ifr_flags & IFF_UP)
            {
                /* packet is short enough to be sent on this interface */
                if (len < ifr->ifr_mtu)
                {
                    if (ifr->ifr_type == IFT_ETHER || ifr->ifr_type == IFT_WLAN)
                    {
                        /* If the interface is in promiscious mode */
                        if (ifr->ifr_flags & IFF_PROMISC)
                        {
                            /* if the source wasn't set, set it to this interface */
                            if (memzero(temp_source, ETH_ALEN) == 0)
                                memcpy(etherdata->h_source, ifr->ifr_hwaddr.m_addr, ETH_ALEN);

                            if (if_transmit(ifr, data, payload_len) < 0)
                                ret |= -1;
                        }
                        /* if this interface is the one for the source address */
                        else if (memcmp(ifr->ifr_hwaddr.m_addr, temp_source, ETH_ALEN) == 0)
                        {
                            if (if_transmit(ifr, data, payload_len) < 0)
                                ret |= -1;
                        }
                    }
                }
            }
        }

        return ret;
    }

    return -1;
}

int ether_filter(struct socket* sock, struct iface* ifs, unsigned char* data)
{
    struct sockaddr_ll* saddr = (struct sockaddr_ll*)&sock->s_addr;
    struct sockaddr_ll* daddr = (struct sockaddr_ll*)&sock->s_daddr;

    struct iframe* ifdata = (struct iframe*)data;
    struct ethframe* ethdata = (struct ethframe*)data;

    /* if we're bound, the bound address isn't zeros, and this is not for that
     * address, and was not broadcast */
    if (sock->s_flag & SF_BIND && memzero(saddr->sll_addr, ETH_ALEN) != 0 &&
        memcmp(saddr->sll_addr, ethdata->ehead.h_dest, ETH_ALEN) != 0 &&
        !(ethdata->ehead.h_dest[0] == 0xFF && ethdata->ehead.h_dest[ETH_ALEN - 1] == 0xFF))
    {
        return -1;
    }

    /* if we're bound to receive a specific protocol, and this is not that
     * protocol */
    if (sock->s_flag & SF_BIND && saddr->sll_protocol != htons(ETH_P_ALL) &&
        saddr->sll_protocol != ethdata->ehead.h_proto)
    {
        return -1;
    }

    /* if we're bound to a specific interface, and this isn't that interface */
    if (sock->s_flag & SF_BIND && saddr->sll_ifindex != IF_IDX_ANY &&
        saddr->sll_ifindex != ifs->ifr_ifindex)
    {
        return -1;
    }

    /* if we're connected, the connected address isn't zeros, and this is not for
     * that address */
    if (sock->s_flag & SF_CONN && memzero(saddr->sll_addr, ETH_ALEN) != 0 &&
        memcmp(saddr->sll_addr, ethdata->ehead.h_source, ETH_ALEN) != 0)
    {
        return -1;
    }

    /* if we're bound to receive a specific protocol, and this is not that
     * protocol */
    if (sock->s_flag & SF_CONN && daddr->sll_protocol != htons(ETH_P_ALL) &&
        daddr->sll_protocol != ethdata->ehead.h_proto)
    {
        return -1;
    }

    /* if not bound, and not a raw protocol, and not blank protocol, and not
     * matching the packet's protocol */
    if (!(sock->s_flag & SF_BIND) && sock->s_rawproto != htons(ETH_P_ALL) &&
        sock->s_rawproto != htons(ETH_P_INTERNAL_ANY) &&
        sock->s_rawproto != ethdata->ehead.h_proto)
    {
        return -1;
    }

    sock_data_recv(sock, data, ntohs(ifdata->ifhead.h_len));
    return 0;
}

/*
    Open a UDP socket (create new ephemeral port on socket s_addr)
*/
int ether_open(struct socket* sock, int flag)
{
    /* setup a default binding */
    ((struct sockaddr_ll*)(&sock->s_addr))->sll_family = AF_PACKET;
    ((struct sockaddr_ll*)(&sock->s_addr))->sll_protocol = sock->s_rawproto;
    ((struct sockaddr_ll*)(&sock->s_addr))->sll_halen = ETH_ALEN;
    memset(((struct sockaddr_ll*)(&sock->s_addr))->sll_addr, 0, ETH_ALEN);

    ((struct sockaddr_ll*)(&sock->s_daddr))->sll_family = AF_PACKET;
    ((struct sockaddr_ll*)(&sock->s_daddr))->sll_protocol = sock->s_rawproto;
    ((struct sockaddr_ll*)(&sock->s_daddr))->sll_halen = ETH_ALEN;
    memset(((struct sockaddr_ll*)(&sock->s_daddr))->sll_addr, 0, ETH_ALEN);

    sock->s_flag &= ~(SF_BIND | SF_CONN);

    return 0;
}

/*
    Close a UDP socket (do nothing)
*/
int ether_close(struct socket* sock, int flag)
{
    return 0;
}

/*
    Bind a UDP socket to a specific port
*/
int ether_bind(struct socket* sock, const struct sockaddr* addr, int addrlen)
{
    /*
        If this is a UDP socket
    */
    if (sock->s_family == PF_PACKET && sock->s_type == SOCK_RAW &&
        sock->s_protocol == htons(ETH_P_ALL))
    {
        /* and the right address length */
        if (addrlen >= sizeof(struct sockaddr_ll))
        {
            /* and the address family is correct */
            if (addr->sa_family == AF_PACKET)
            {
                /* copy into socket address */
                memcpy(&sock->s_addr, addr, sizeof(struct sockaddr_ll));

                sock->s_flag |= SF_BIND;
            }
            else if (addr->sa_family == AF_UNSPEC)
            {
                /* copy into socket address */
                memset(&sock->s_addr, 0, sizeof(struct sockaddr_ll));

                /* recreate default binding */
                ((struct sockaddr_ll*)(&sock->s_addr))->sll_family = AF_PACKET;
                ((struct sockaddr_ll*)(&sock->s_addr))->sll_protocol = sock->s_rawproto;
                ((struct sockaddr_ll*)(&sock->s_addr))->sll_halen = ETH_ALEN;

                sock->s_flag &= ~SF_BIND;
            }
            else
            {
                return -1;
            }
        }
    }
    return 0;
}

/*
    Connect a UDP socket to a specific port and address
*/
int ether_connect(struct socket* sock, const struct sockaddr* addr, int addrlen)
{
    /*
        If this is a UDP socket
    */
    if (sock->s_family == PF_PACKET && sock->s_type == SOCK_RAW &&
        sock->s_protocol == htons(ETH_P_ALL))
    {
        /* and the right address length */
        if (addrlen >= sizeof(struct sockaddr_ll))
        {
            /* and the address family is correct */
            if (addr->sa_family == AF_PACKET)
            {
                /* copy into socket destination address */
                memcpy(&sock->s_daddr, addr, sizeof(struct sockaddr_ll));

                sock->s_flag |= SF_CONN;
            }
            else if (addr->sa_family == AF_UNSPEC)
            {
                /* copy into socket address */
                memset(&sock->s_daddr, 0, sizeof(struct sockaddr_ll));

                /* recreate default connection */
                ((struct sockaddr_ll*)(&sock->s_daddr))->sll_family = AF_PACKET;
                ((struct sockaddr_ll*)(&sock->s_daddr))->sll_protocol = sock->s_rawproto;
                ((struct sockaddr_ll*)(&sock->s_daddr))->sll_halen = ETH_ALEN;

                sock->s_flag &= ~SF_CONN;
            }
            else
            {
                return -1;
            }
        }
    }
    return 0;
}

/*
    Receive bytes into buf from data and fill in src_addr as appropriate
*/
int ether_recvfrom(struct socket* sock, unsigned char* data, unsigned char* buf, int len, int flags, struct sockaddr* src_addr, int* addrlen)
{
    /*
        By this point, we have already checked what was valid before copying it
       into the packet buffer for this socket, so this should be 100% just for us.
    */
    if (sock->s_family == PF_PACKET)
    {
        struct iframe* ifdata = (struct iframe*)data;
        struct ethframe* ethdata = (struct ethframe*)data;
        unsigned char* payload = ifdata->buf;
        int payload_len = (ntohs(ethdata->ifhead.h_len) - sizeof(struct ifhdr));

        /* copy the info from the header into the src_addr */
        if (src_addr && *addrlen >= sizeof(struct sockaddr_ll))
        {
            struct sockaddr_ll* saddr = (struct sockaddr_ll*)src_addr;
            saddr->sll_family = AF_PACKET;
            saddr->sll_protocol = ethdata->ehead.h_proto;
            saddr->sll_ifindex = IF_IDX_ANY;
            saddr->sll_halen = ETH_ALEN;

            /* assume packet was sent to us direct */
            saddr->sll_pkttype = PACKET_HOST;

            /* first byte of address is odd == multicast (only set if not this host)
             */
            if (ethdata->ehead.h_dest[0] & 0x01 == 0x01)
                saddr->sll_pkttype = PACKET_MULTICAST;

            /* if it starts and ends FF, assume broadcast */
            if (ethdata->ehead.h_dest[0] == 0xFF &&
                ethdata->ehead.h_dest[ETH_ALEN - 1] == 0xFF)
                saddr->sll_pkttype = PACKET_BROADCAST;

            /* if the source and destination address is all zeros then this means
             * loopback sent this packet */
            if (memzero(ethdata->ehead.h_source, ETH_ALEN) == 0 &&
                memzero(ethdata->ehead.h_dest, ETH_ALEN) == 0)
                saddr->sll_pkttype = PACKET_OUTGOING;

            memcpy(saddr->sll_addr, ethdata->ehead.h_source, ETH_ALEN);

            *addrlen = sizeof(struct sockaddr_ll);
        }

        /* if the protocol for this socket is not all/any then we skip past the ethernet header */
        if (sock->s_rawproto != ETH_P_ALL && sock->s_rawproto != ETH_P_INTERNAL_ANY)
        {
            payload = (unsigned char*)(payload + sizeof(struct ether_header));
            payload_len -= sizeof(struct ether_header);
        }

        /* finally, copy the payload into the receive buffer */
        int rlen = 0;
        for (; rlen < len && rlen < payload_len; rlen++)
        {
            buf[rlen] = payload[rlen];
        }

        return rlen;
    }
    return -1;
}

int ether_sendto(struct socket* sock, unsigned char* data, unsigned char* buf,
  int len, int flags, struct sockaddr* dst_addr, int* addrlen)
{
    if (sock->s_family == PF_PACKET)
    {
        struct ether_header* etherdata = (struct ether_header*)(data);

        /* step 1: copy the passed buffer into the data buffer */
        if (data != buf)
            memmove(data, buf, len);

        /* if we have a destination */
        if (dst_addr != NULL)
            return proto_build(PS_ETH_ALL, sock->s_family, sock->s_type,
              sock->s_rawproto, NULL, data, len, &sock->s_addr,
              dst_addr);

        /* if we're connected and weren't given a destination, the destination is
         * the connection */
        if (sock->s_flag & SF_CONN)
            return proto_build(PS_ETH_ALL, sock->s_family, sock->s_type,
              sock->s_rawproto, NULL, data, len, &sock->s_addr,
              &sock->s_daddr);

        /* lastly, don't set a destination */
        return proto_build(PS_ETH_ALL, sock->s_family, sock->s_type,
          sock->s_rawproto, NULL, data, len, &sock->s_addr, NULL);
    }
    return -1;
}
