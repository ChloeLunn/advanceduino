#!/bin/sh

#   How the compiler lines work:
#
#   arm-none-eabi-gcc     = this is pretty much the most raw version of gcc you can use for arm, it doesn't include any system headers, etc.
#   -mcpu=cortex-m_       = this tells it which instructions are available to our processors. A SAMD21 is m0, and SAMD51 is m4.
#   -mthumb               = the SAMDs only support thumb instructions, so make sure to flag this up to the compiler
#   -nostdlib -nolibc     = there's no standard library or libc, so warn the compiler it can't link against it's version if it has one
#   -ffreestanding        = this tells it that we've defined _start and don't need any "basic" components from libgcc because we've done this raw
#   -static               = we don't have a dynamic linker, so tell the compiler to do everything statically linked.
#                           This also disables -pie but keeps -fPIE (because -pie says to dynamically link).
#   -fPIE                 = Position Independent Executable. If you have a library, or no MMU (like SAMDs), then you need the code to be executable from any address.
#                           This flag is what tells the compiler it can't assume addresses during branches or data accesses.
#                           The difference between PIE and PIC is very little, it's just that PIC means that it is a library that will be linked up upon loading.
#                           ... We don't want that
#   -D__SAMDxx__          = If we need to have different syscall wrappers for different boards, we can set a define to say which board we're on
#   ___.c -o echo         = The source and output file.
#   -z mac-page-size=4096 = This option reduces the size of the memory required once loaded, as it forces it to align to 4096 bytes between loaded segments
#   -Ttext=0              = Set the loaded in-memory offset of the text section to 0, otherwise it will pad the
#                           beginning of this to put it towards the end of the 4K 'page' and take up more room


# For Cortex-M4 (SAMD51)
arm-none-eabi-gcc -O2 -mcpu=cortex-m4 -mthumb -nostdlib -nolibc -ffreestanding -static -fPIE -D__SAMD51__ echo.c -o echo -z max-page-size=4096 -Ttext=0

# For Cortex-M0 (SAMD21) -- NOT TESTED
#arm-none-eabi-gcc -O2 -mcpu=cortex-m0 -mthumb -nostdlib -nolibc -ffreestanding -static -fPIE -D__SAMD21__ echo.c -o echo -z max-page-size=4096 -Ttext=0

# uncomment these lines if you want a peek at the disassembly of the program to see more info about elfs
#arm-none-eabi-readelf -h -l ./echo  # print headers
#arm-none-eabi-objdump -D echo       # print dissassembly
