#include <time.h>

/* number of days in each month */
const unsigned char i_months[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

/* Sat Jan 1st, 2000, 00:00 */
#define EPOCH_MILLENIUM     946684800
#define EPOCH_BASE_YEAR     1970
#define MILLENIUM_BASE_YEAR 2000
#define POSIX_BASE_YEAR     1900
#define MONTHS_PER_YEAR     (12)
#define DAYS_PER_YEAR       (365)
#define DAYS_PER_LEAP_YEAR  (DAYS_PER_YEAR + 1)

#define HOUR_PER_DAY (24)

#define MIN_PER_HOUR (60)
#define MIN_PER_DAY  (HOUR_PER_DAY * MIN_PER_HOUR)

#define SECS_PER_MINUTE (60)
#define SECS_PER_HOUR   (MIN_PER_HOUR * SECS_PER_MINUTE)
#define SECS_PER_DAY    (HOUR_PER_DAY * SECS_PER_HOUR)

/* which month is february (gasp!) */
#define FEBRUARY 2

/* days between UNIX base and 2000 */
#define DAYSTO2000 ((DAYS_PER_YEAR * 30) + 7)

/* 4 year intervals include 1 leap year */
#define DAYS4YEARS ((DAYS_PER_YEAR * 4) + 1)

/* 100 year intervals include 24 leap years */
#define DAYS100YEARS ((DAYS_PER_YEAR * 100) + 24)

/* 400 year intervals include 97 leap years */
#define DAYS400YEARS ((DAYS_PER_YEAR * 400) + 97)

#define is_leap_year(year)    ((((year) % 4) == 0 && ((year) % 100) != 0) || (((year) % 400) == 0))
#define days_per_month(month) (i_months[(month)-1])
#define days_per_year(year)   (is_leap_year(year) ? DAYS_PER_LEAP_YEAR : DAYS_PER_YEAR)

/* time to epoch -- processor intensive */
time_t time_to_epoch(struct tm* dt)
{
    if (dt == NULL)
    {
        return -1;
    }

    unsigned long long secs;
    unsigned long long i;
    unsigned long long year;
    unsigned long long days;

    year = dt->tm_year;

    /* don't handle times before 1970 epoch */
    if (year < EPOCH_BASE_YEAR)
    {
        return -1;
    }

    days = 0;

    /* Add leap day */
    if (is_leap_year(year) && dt->tm_mon > FEBRUARY)
    {
        days++;
    }

    /* Add days per year from before the millenium */
    if (year < MILLENIUM_BASE_YEAR)
    {
        /* simple way for early years */
        for (i = EPOCH_BASE_YEAR; i < year; i++)
        {
            days += days_per_year(i);
        }
    }

    /* Add days per year after the millenium */
    else
    {
        /* years are properly aligned */
        days += DAYSTO2000;
        year -= MILLENIUM_BASE_YEAR;

        i = year / 400;
        days += i * DAYS400YEARS;
        year -= i * 400;

        i = year / 100;
        days += i * DAYS100YEARS;
        year -= i * 100;

        i = year / 4;
        days += i * DAYS4YEARS;
        year -= i * 4;

        /* remainder */
        for (i = dt->tm_year - year; i < dt->tm_year; i++)
        {
            days += days_per_year(i);
        }
    }

    /* Months */
    for (i = 1; i < dt->tm_mon; i++)
    {
        days += days_per_month(i);
    }
    days += (dt->tm_mday - 1);

    /* Seconds */
    secs = ((((unsigned long long)days * HOUR_PER_DAY + dt->tm_hour) * MIN_PER_HOUR + dt->tm_min) * SECS_PER_MINUTE + dt->tm_sec);

    /* check for value overflow */
    if ((time_t)secs < 0 || secs > 0x7FFFFFFFFFFFFFFFLL)
    {
        return -1;
    }

    return (long long)secs;
}

/* epoch to time -- processor intensive */
struct tm* epoch_to_time(const time_t epoch, struct tm* dt)
{
    if (dt == NULL)
    {
        return NULL;
    }

    /* we're note going to handle dates before the UNIX epoch, so if we get one that is, return with error. */
    if (epoch < 0)
    {
        return NULL;
    }

    int leap;
    time_t i;    /* temporary variable, mainly used for loops */
    time_t days; /* temporary variable used to whittle the number of days into other values */
    time_t rsec; /* remainder seconds */

    /* get a quick number of whole days */
    days = epoch / SECS_PER_DAY;

    /* and number of seconds left after that */
    rsec = epoch % SECS_PER_DAY;

    /* day of the week is pretty easy, we just need to find how many 7's go into the number of days; leap years don't affect days of the week. */
    dt->tm_wday = (days + 4) % 7;

    /* ~~~~ Calculate the tm posix year; this takes leaps years into account and breaks the number of whole days into years ~~~~*/
    /* time is after or is the millenium */
    if (epoch >= EPOCH_MILLENIUM)
    {
        days -= DAYSTO2000;
        dt->tm_year = MILLENIUM_BASE_YEAR;

        i = days / DAYS400YEARS;
        days -= i * DAYS400YEARS;
        dt->tm_year += i * 400;

        i = days / DAYS100YEARS;
        days -= i * DAYS100YEARS;
        dt->tm_year += i * 100;

        i = days / DAYS4YEARS;
        days -= i * DAYS4YEARS;
        dt->tm_year += i * 4;

        for (i = dt->tm_year; days >= days_per_year(i); i++)
        {
            days -= days_per_year(i);
        }
    }
    /* time is before the millenium */
    else
    {
        /* Subtract out whole years, counting them in i. */
        for (i = EPOCH_BASE_YEAR; days >= days_per_year(i); i++)
        {
            days -= days_per_year(i);
        }
    }
    dt->tm_year = i;                /* total CE year */
    dt->tm_year -= POSIX_BASE_YEAR; /* relative posix year */

    /* ~~~~ Calculate the month in the year; takes the left over days from years calc, converted to number of months in this year that the current days have seen ~~~~ */
    for (leap = 0, i = 1; days >= days_per_month(i) + leap; i++)
    {
        days -= days_per_month(i) + leap;
        if (i == 1 && is_leap_year(dt->tm_year))
        {
            leap = 1;
        }
        else
        {
            leap = 0;
        }
    }
    dt->tm_mon = i;

    /* ~~~~ Day in month is just the remainder, plus one to index from 1 instead of 0 ~~~~ */
    dt->tm_mday = days + 1;

    /* ~~~~ Day in the year: add up the number of days per month for before our current month ~~~~ */
    dt->tm_yday = 0;
    for (i = 1; i < dt->tm_mon; i++)
    {
        if (i == FEBRUARY && is_leap_year(dt->tm_year))
        {
            dt->tm_yday += days_per_month(i) + 1;
        }
        else
        {
            dt->tm_yday += days_per_month(i);
        }
    }
    /*
        And then add the remainder for the number of days through the current month.
        Remember that the tm_mday field is already indexed from 1, so we don't need to add another 1 to the tm_yday after this.
     */
    dt->tm_yday += dt->tm_mday;

    /* hours, min and seconds as remainder of seconds */
    dt->tm_hour = rsec / SECS_PER_HOUR;
    rsec = rsec % SECS_PER_HOUR;

    dt->tm_min = rsec / SECS_PER_MINUTE;
    rsec = rsec % SECS_PER_MINUTE;

    /* and the seconds comes out to whatever little bit of seconds we have left */
    dt->tm_sec = rsec;

    /* -1 is not known, 0 is no, 1 is yes, we don't have the info to make it known here, so we'll mark it as unknown */
    dt->tm_isdst = -1;

    return dt;
}
