/*
Copyright 2022 Chloe Lunn

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
    This example uses a UNIX-like tty settings struct to setup and control a readline
    function for a connected tty/terminal.

    The TTY cooker uses a LF to indicate end of line, if you're on a system
    with CR newlines, then you can do an LF using CTRL + J

    This example is independent of board architecture but requires both a USB Serial and
    a Uart Serial device in the arduino libraries for your board.

    You may need to alter the drivers.h definitions of these to match your board.

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! THIS EXAMPLE IS HEAVILY WORK-IN-PROGRESS, PLEASE IGNORE FOR NOW.... !!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    See tty.h for the definitions for various structs and controls used in this example
    See drivers.h for the UNIX-like driver wrappers for the Arduino libraries.

    This file is split into roughly 4 layers, starting at the bottom of this file:

    Layer 1 is the application layer, and includes setup/loop and the read/write. It is designed to do pre-cooked read/write of the terminal.

    Layer 2 is the TTY character handlers that are used to add/remove characters from the character lists as you read/write

    Layer 3 is the TTY level in/out filter which pulls characters in off the device and "cooks" them into character lists, and v.v. for outputs

    Layer 4 is the device driver level (which actually just wraps the arduino functions) and is used to pass TTY control to and
            from the routine(s) that actually does the electronics level comms.

    There is also the function ttyctrl which is used to pass control data between Layer 1 and Layers 2, 3, 4
*/

#include <Arduino.h>
#include <Variant.h>
#if defined(__CORTEX_M) && (__CORTEX_M == 0)
#include <core_cm0plus.h>
#elif defined(__CORTEX_M) && (__CORTEX_M == 4)
#include <core_cm4.h>
#endif
#include <string.h>

/* include the tty info, we're going to use it qto configure the port */
#include "term.h"

/* and include the serial device drivers, these offer us access to read/write/open/close and configuration calls */
#include "cserial.h"

/* do ioctl-like control of the tty */
void ttyctrl(struct tty_ioctl_args* arg, unsigned char flag)
{
    if (arg == NULL)
    {
        return;
    }

    /* Exercise: Why do we do all the sets before the gets? */

    /* process each flag bit in order */
    for (int BIT = 0; BIT < 8; BIT++)
    {
        /* Exercise: Why do we do each bit of the flag separately? */
        switch (flag & (1 << BIT))
        {
            /* Set and Get Serial port settings */
        case TTY_SETS:
            {
                ttys.t_timeout = arg->io_timeout;
                ttys.t_speed = arg->io_speed;
                ttys.t_mode = (arg->io_mode & TTY_SERMASK) | (arg->io_mode & ~TTY_SERMASK);
                /*
                    In UNIX systems with ioctl, the system call for ioctl will actually call an ioctl function in the driver table
                    and use this to do anything low level like change speed or change flow controls, registers, flush, etc.

                    But because this is a super simple example and the driver can directly access the changed settings, we're just going to have a function to push changes instead.
                */
                if (dev_table[ttys.t_dev].d_push)
                    dev_table[ttys.t_dev].d_push(ttys.t_dev);
                break;
            }
        case TTY_GETS:
            {
                arg->io_timeout = ttys.t_timeout;
                arg->io_speed = ttys.t_speed;
                arg->io_mode = (ttys.t_mode & TTY_SERMASK) | (ttys.t_mode & ~TTY_SERMASK);
                break;
            }

            /* Set and Get TTY size options */
        case TTY_SETSZ:
            {
                memset((void*)&ttys.t_size, (void*)&(arg->io_size), sizeof(struct tty_size));
                break;
            }
        case TTY_GETSZ:
            {
                memset((void*)&(arg->io_size), (void*)&ttys.t_size, sizeof(struct tty_size));
                break;
            }

            /* Set and get special control characters */
        case TTY_SETC:
            {
                ttychars(&(arg->io_ctrl));
                break;
            }
        case TTY_GETC:
            {
                memset((void*)&(arg->io_ctrl), (void*)&ttys.t_ctrl, sizeof(struct tty_ctrl_chars));
                break;
            }

            /* Set and get delay/device control  */
        case TTY_SETD:
            {
                ttys.t_mode = (arg->io_mode & TTY_OUTMASK) | (arg->io_mode & ~TTY_OUTMASK);
                ttys.t_mode = (arg->io_mode & TTY_INMASK) | (arg->io_mode & ~TTY_INMASK);
                ttys.t_cdelay - arg->io_cdelay;
                break;
            }
        case TTY_GETD:
            {
                arg->io_mode = (ttys.t_mode & TTY_OUTMASK) | (ttys.t_mode & ~TTY_OUTMASK);
                arg->io_mode = (ttys.t_mode & TTY_INMASK) | (ttys.t_mode & ~TTY_INMASK);
                arg->io_cdelay - ttys.t_cdelay;
                break;
            }

            /* flush the tty */
        case TTY_FLUSH:
            {
                ttyflush();
                break;
            }
        default:
            break;
        }
    }

    return;
}

/* ~~~~~~~~~~~~~~~~ PROGRAM CODE ~~~~~~~~~~~~~~~~ */

/*
    Unlike previous lessions we're writting this side as if it's more like a userspace application
*/
void setup()
{
    /* open the serial device */
    cserial_open();

    /* get the default settings */
    struct term_opts TermSettings;
    cserial_getopt(&TermSettings);

    /*
        You need to do a 'get' on these kinds of things because you never know whether something
        has been manually changed by the driver or if there's some hidden value that not doing a
        get on would wipe out and break.

        For example, not doing a 'get' on the termios control and instead just filling in a struct
        and just setting it can break a process on BSD based OSes, including MacOS!
    */

    /* set some basic config stuff that tells it how to handle the echo/prints */
    TermSettings.prompt = (char*)"> "; /* simple prompt (only shown with echo on and not raw) */
    TermSettings.lcase = 1;            /* lowercase enabled */
    TermSettings.echo = 1;             /* echo off */
    TermSettings.xon = 0;              /* xon flow control is off */
    TermSettings.raw = 0;              /* raw handling off */

    /* set the output formatter to 80x24, a common size, with 8-char tabs */
    TermSettings.columns = 80;
    TermSettings.rows = 24;
    TermSettings.tabwidth = 8;
    TermSettings.softnewline = 1; /* do newline if we think we hit the last char in the row */

    /* Then some hardware controls */
    TermSettings.speed = 115200; /* baud rate */
    TermSettings.timeout = 1000; /* read timeout */
    TermSettings.cdelay = 0;     /* no delay on xmit */

    /* and write out our changes back to the driver */
    cserial_setopt(&TermSettings);

    /*
        Writing changes also:

        1. If changing prompt, raw, or echo:
            Sets a prompt to be written next time a cook happens
            If echo is on, it will also print a newline when this happens

        2. If changing the size of the window:
            Resets the printing position to 1,1
            If echo is on then it also sends an <ESC>c to clear the terminal
    */

    /* we'll force a delay for a bit just for stability */
    delay(1000);
}

/* inifinite loop */
void loop()
{
    /*
        In a real kernel, the input handling is called via interrupt
        and routes to the specific TTY being used by the current process/user.

        The output is then called when flush or write are called, and the tty is waiting
        to drain the output queue. Some systems have this drain as a service, others a
        kernel process, some it's done "inline" with the system call, but the thread
        that made the call sleeps.

        We don't have to worry about this here we'll just use a special 'keepalive' function that handles this.
    */
    cserial_keepalive();
}
