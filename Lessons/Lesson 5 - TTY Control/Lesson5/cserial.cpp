/*
    This file wraps the USB serial port with the term library to do some more standard posix-y controls with cooked queues.
    It's basically what we were previously doing, but now done a bit better and in a way that lets it be a library
*/

#include "cserial.h"

#include "term.h"

#include <Arduino.h>
#include <ctype.h>
#include <string.h>

struct term_config uterm; /* usb terminal device settings */
int opened = 0;

/* ~~~~~~~~~~~~~~~~ LOW LEVEL USB DRIVER WRAPPERS used by the tty lib ~~~~~~~~~~~~~~~~ */

extern "C" int usb_open()
{
    Serial.begin(9600);
    return 0;
}

extern "C" int usb_close()
{
    Serial.end();
    return 0;
}

extern "C" int usb_read()
{
    return Serial.read();
}

extern "C" int usb_write(int c)
{
    Serial.write((char)(c & 0x7F));
    return 0;
}

extern "C" int usb_config(struct term_config* term)
{
    //   Serial.begin(term->speed);
    Serial.setTimeout(term->timeout);
    Serial.flush();
    return 0;
}

extern "C" int usb_available()
{
    return Serial.available();
}

/* ~~~~~~~~~~~~~~~~ HIGH LEVEL USB LIBRARY to be used by main/user facing functions ~~~~~~~~~~~~~~~~ */

/* open the serial port with our new interactive tty layer */
extern "C" int cserial_open()
{
    /* clear terminal settings */
    memset(&uterm, 0, sizeof(struct term_config));

    /* attach the usb wrapper functions to our term struct */
    uterm.open = &usb_open;
    uterm.write = &usb_write;
    uterm.read = &usb_read;
    uterm.close = &usb_close;
    uterm.config = &usb_config;
    uterm.available = &usb_available;

    /* set to default, then call lower driver open & config */
    ttyopen(&uterm);

    opened++;

    return 0;
}

extern "C" int cserial_close()
{
    int r = ttyclose(&uterm);

    if (opened > 0)
        opened--;

    return r;
}

extern "C" int cserial_connected()
{
    return (!!opened && Serial);
}

extern "C" void cserial_keepalive()
{
    ttycook(&uterm);
}

extern "C" int cserial_setopt(struct term_opts* opts)
{
    ttysetopts(&uterm, opts);
    ttyconfig(&uterm);
    return 0;
}

extern "C" int cserial_getopt(struct term_opts* opts)
{
    ttygetopts(&uterm, opts);
    return 0;
}

extern "C" int cserial_getc()
{
    return ttygetc(&uterm);
}

extern "C" int cserial_gets(char* buf, int len)
{
    return ttygets(&uterm, buf, len);
}

extern "C" int cserial_putc(int c)
{
    return ttyputc(&uterm, c);
}

extern "C" int cserial_puts(const char* buf, int len)
{
    return ttyputs(&uterm, buf, len);
}

extern "C" int cserial_printf(const char* fmt, ...)
{
    if (fmt == NULL)
        return -1;

    char s[512];
    va_list args;
    va_start(args, fmt);
    vsprintf(s, fmt, args);
    int r = cserial_puts(s, strlen(s));
    va_end(args);

    return r;
}
