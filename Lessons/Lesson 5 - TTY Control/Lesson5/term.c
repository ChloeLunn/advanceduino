/* PORTABLE TTY CONTROL LAYER */

#include "term.h"

#include "partab.h"

#include <ctype.h>
#include <string.h>

/* if character q is any one of the non-zero values in str */
int isany(const char* str, char q)
{
    char* f = (char*)str;
    while (*f)
    {
        if (q == *f++)
        {
            return 1;
        }
    }
    return 0;
}

int ttygetc(struct term_config* term)
{
    /* if in raw mode, just try and read the base layer */
    if (term->raw)
        return term->read();

    if (term->cooked)
    {
        int r = *term->ptr;
        term->ptr++;

        /* hit end */
        if (!*term->ptr || term->ptr == &term->queue[COOKED_MAX])
        {
            /* reset */
            term->ptr = &term->queue[0];
            memset(&term->queue[0], 0, COOKED_MAX);
            term->cooked = 0;
        }

        return r;
    }

    return -1; /* return "error" if no cooked line available */
}

int ttyputc(struct term_config* term, int c)
{
    /* if in raw mode, just try and write to the base layer */
    if (term->raw)
        return term->write(c);

    int r = 0;
    if (!term->lcase)
        r = term->write((char)toupper(c & 0x7F));
    else
        r = term->write((char)(c & 0x7F));

    if (c == '\n')
        ttyputc(term, '\r');

    /* change position based on info*/
    if (isprint(c) && !isblank(c) && !iscntrl(c))
    {
        /*
            If the character is printable, not a blank space, and not a control character
        */
        term->col_pos++;
    }
    else
    {
        switch (c)
        {
        case '\t':
            /*
                Expand tabs to spaces based on column position, tab width, and column size
            */
            break;
        case '\n':
            /* move down a row */
            term->row_pos++;
            break;
        case '\r':
            /* move to start of row */
            term->col_pos = 0;
            break;
        }
    }

    /* normalise column/row positions */
    if (term->col_pos >= term->columns)
    {
        term->col_pos = 0;

        term->row_pos++;
        if (term->softnewline)
        {
            ttyputc(term, '\n');
        }
    }

    if (term->row_pos >= term->rows)
    {
        term->row_pos = term->rows - 1;
        /* Exercise: implement the use of an options flag that says to send an erase command when the rows are full */
    }

    return r;
}

int ttyputs(struct term_config* term, const char* buf, int length)
{
    int tlen = strlen(buf);

    /* create a temporary buffer to force it to print no further than length */
    char pstring[length + 1];
    pstring[0] = 0;
    strncpy(pstring, buf, length);

    /* set the terminating 0 to stop the printf from overflowing the buffer */
    pstring[length] = 0;

    char* str = &pstring[0];

    while (*str)
        ttyputc(term, *str++);

    /* return the shortest length */
    if (tlen < length)
    {
        return tlen;
    }
    return length;
}

int ttygets(struct term_config* term, char* buf, int length)
{
    /* if in raw mode, just try and read what's available raw */
    if (term->raw)
    {
        /* this probably works in cooked too... */
        char* obuf = buf;

        while (term->available() && length)
        {
            int r = ttygetc(term);
            if (r < 0)
                break;

            *buf = (char)(r & 0x7F);

            buf++;
            length--;
        }
        return buf - obuf;
    }

    if (term->cooked)
    {
        /* read it into the passed buffer */
        int act_length = strlen(term->ptr) + 1;
        strncpy(buf, term->ptr, length);

        /* available length is less than the requested length */
        if (act_length <= length)
        {
            /* clear cooked */
            term->ptr = &term->queue[0];
            memset(&term->queue[0], 0, COOKED_MAX);
            term->cooked = 0;

            /* return the length we could actually read */
            return act_length;
        }

        /* available length is more than the requested */

        term->ptr += length; /*  just inc. what was read */

        /* hit end */
        if (!*term->ptr || term->ptr == &term->queue[COOKED_MAX])
        {
            /* reset */
            term->ptr = &term->queue[0];
            memset(&term->queue[0], 0, COOKED_MAX);
            term->cooked = 0;
        }

        return length;
    }

    return -1; /* return "error" if no cooked line available */
}

/* set some of the defaults up */
void ttyinit(struct term_config* term)
{
    /* reset the cooked tty vars */
    term->ptr = &term->queue[0];
    memset(&term->queue[0], 0, COOKED_MAX);
    term->cooked = 0;

    /* set default character control lists */
    memcpy((void*)(&term->control_chars), (void*)(&default_controlchars), sizeof(struct tc));

    /* default baud and timeout */
    term->speed = default_baud; /* Set the tty settings to match the driver defaults */
    term->timeout = default_timeout;

    /* set echo and lowercase on */
    term->echo = 1;
    term->lcase = 1;
    /* soft flow control and raw mode off */
    term->xon = 0;
    term->raw = 0;

    /* very basic prompt */
    term->prompt = "> ";
    term->prpnt = 0;

    /* cr implies lf */
    term->cr_imp_lf = 1;
}

/* needs to be called regularly, but prob not in systick! */
void ttycook(struct term_config* term)
{
    /* if raw control is on, then we don't care about cooking, we're just going to handle this differently */
    if (term->raw)
        return;

    /*
        if terminal is cooked, then we can't do anything until we read.
        the base USB driver will be buffering the raw contents, so no need to worry about lost data
     */
    if (!term->cooked)
    {
        /* print prompt if set and not done yet */
        if (term->echo && term->prompt && !term->prpnt)
        {
            term->prpnt = 1;
            ttyputs(term, term->prompt, strlen(term->prompt));
        }

        /* while we still have data to read off the raw driver's queue */
        while (!!term->available())
        {
            /* Read the available character */
            int r = term->read();

            /* if it was valid (-1 is invalid, 0 is always ignored) */
            if (r > 0)
            {
                /* ASCII 7-bit only */
                *term->ptr = r & 0x7F;

                int callback_char = *term->ptr;

                /* if lowercase is off, and read char is alpha and upper, then convert to lower */
                if (!term->lcase && isalpha(*term->ptr) && isupper(*term->ptr))
                {
                    *term->ptr = tolower(*term->ptr);
                }

                /* if cr implies lf, do that */
                if (term->cr_imp_lf && *term->ptr == '\r')
                {
                    *term->ptr = '\n';
                }

                /* if the previous character was an escape */
                if (*(term->ptr - 1) == term->control_chars.t_esc)
                {
                    /* use the input mapping table to unescape it in the cooked queue */
                    char tc = imaptab[*term->ptr];

                    /* if tc == 0 then there's no escape handling needed */
                    if (!tc)
                    {
                        term->ptr--; /* go back one */
                        *term->ptr = tc;
                    }
                }

                /* ~~~~~~~~~~~~~~~~ start of main processing list ~~~~~~~~~~~~~~~~~ */

                /* ignore CRs */
                if (*term->ptr == '\r')
                {
                    /* just clear it as not valid char */
                    *term->ptr = 0;
                }

                /* ignore NULs */
                else if (*term->ptr == '\0')
                {
                    /* just clear it as not valid char */
                    *term->ptr = 0;
                }

                /* if it's a control char */
                else if (isany(term->c_cc, *term->ptr))
                {
                    /* if a cancel input character */
                    if (*term->ptr == term->control_chars.t_eof || *term->ptr == term->control_chars.t_intr || *term->ptr == term->control_chars.t_quit)
                    {
                        /* do a newline to show visual confirmation of buffer wipeout */
                        if (term->echo)
                        {
                            /* no bell as not error */
                            ttyputc(term, '\n');
                        }

                        /* then reset us back to a "blank" cooked tty */
                        term->ptr = &term->queue[0];
                        memset(&term->queue[0], 0, COOKED_MAX);
                        term->cooked = 0;

                        /* print prompt next time cook is called */
                        term->prpnt = 0;

                        if (term->cancel_callback)
                        {
                            term->cancel_callback(callback_char);
                        }

                        return;
                    }

                    /* if a clear line character (there's got to be a better way to do this) */
                    else if (*term->ptr == term->control_chars.t_kill)
                    {
                        /* visual erase if echo is on */
                        if (term->echo)
                        {
                            /* basically backspace until we clear the entire line */
                            for (int i = 0; i < (term->ptr - &term->queue[0]); i++)
                            {
                                ttyputc(term, '\b');  // back
                            }
                            /* then fill with spaces */
                            for (int i = 0; i < (term->ptr - &term->queue[0]); i++)
                            {
                                ttyputc(term, ' ');  // back
                            }
                            /* and back again */
                            for (int i = 0; i < (term->ptr - &term->queue[0]); i++)
                            {
                                ttyputc(term, '\b');  // back
                            }
                        }

                        *term->ptr = 0;              /* set end of string */
                        term->cooked = 0;            /* flag buffer as uncooked */
                        term->ptr = &term->queue[0]; /* set ptr to begining to allow writes from beginning again */

                        if (term->erase_callback)
                        {
                            term->erase_callback(callback_char);
                        }

                        return;
                    }

                    /* if a backspace character */
                    else if (*term->ptr == term->control_chars.t_erase || *term->ptr == term->control_chars.t_erase2)
                    {
                        /* if we actually have data in the queue */
                        if (term->ptr > &term->queue[0])
                        {
                            *term->ptr = 0;  // clear the just entered backspace
                            term->ptr--;     // put us back
                            *term->ptr = 0;  // clear previous char

                            /* visual erase if echo is on */
                            if (term->echo)
                            {
                                ttyputc(term, '\b');  // back
                                ttyputc(term, ' ');   // print space over
                                ttyputc(term, '\b');  // back
                            }
                        }
                    }
                }

                /* if a new line -- finished reading! */
                else if (*term->ptr == '\n')
                {
                    /* echo a new line character */
                    if (term->echo)
                    {
                        ttyputc(term, '\n');
                    }

                    *term->ptr = 0;              /* set end of string (NL not kept)*/
                    term->cooked = COOKED_NL;    /* flag buffer as cooked */
                    term->ptr = &term->queue[0]; /* set ptr to begining to allow reads */

                    /* print prompt next time cook is called */
                    term->prpnt = 0;

                    if (term->line_callback)
                    {
                        term->line_callback(callback_char);
                    }

                    return;
                }

                /* if any other printable character */
                else if (isprint(*term->ptr))
                {
                    /* echo char if echo is on */
                    if (term->echo)
                    {
                        ttyputc(term, *term->ptr);
                    }

                    /* Increment the ptr to the next character */
                    term->ptr++;

                    /* if we've filled the buffer mark it cooked */
                    if (term->ptr >= &term->queue[COOKED_MAX])
                    {
                        /* do a newline to show visual confirmation of buffer wipeout */
                        if (term->echo)
                        {
                            ttyputc(term, 7); /* bell to show full rather than normal NL */
                            ttyputc(term, '\n');
                        }

                        *term->ptr = 0;              /* set end of string */
                        term->cooked = COOKED_FULL;  /* flag buffer as cooked */
                        term->ptr = &term->queue[0]; /* set ptr to begining to allow reads */

                        /*
                            REPLACE THIS WITH FLOW CONTROL!
                        */

                        /* print prompt next time cook is called */
                        term->prpnt = 0;

                        if (term->full_callback)
                        {
                            term->full_callback(callback_char);
                        }

                        // return; no return as we may have fixed the full buffer in the callback
                    }
                }

                /* unknown */
                else
                {
                    *term->ptr = 0;    // clear the unknown
                    ttyputc(term, 7);  // bell
                }
            }
        }
    }
}

/* push config to lower layer */
void ttyconfig(struct term_config* term)
{
    term->config(term);
}

/* set to default, then call lower driver open & config */
int ttyopen(struct term_config* term)
{
    ttyinit(term);
    int r = term->open();
    if (!r)
        ttyconfig(term);
    return r;
}

/* alias for calling lower driver close */
int ttyclose(struct term_config* term)
{
    return term->close();
}

int ttysetopts(struct term_config* term, struct term_opts* opts)
{
    int reset = 0;
    /* if any of is being changed, we essentially do a silent reset of the terminal */
    if (opts->raw != term->raw || opts->echo != term->echo || opts->prompt != term->prompt)
    {
        reset = 1;
    }

    term->prompt = opts->prompt;
    term->lcase = opts->lcase;
    term->raw = opts->raw;
    term->echo = opts->echo;
    term->xon = opts->xon;
    term->speed = opts->speed;
    term->timeout = opts->timeout;
    term->cdelay = opts->cdelay;
    memcpy((void*)(&term->control_chars), (void*)(&opts->control_chars), sizeof(struct tc));
    term->line_callback = opts->line_callback;
    term->full_callback = opts->full_callback;
    term->cancel_callback = opts->cancel_callback;
    term->erase_callback = opts->erase_callback;

    if (reset)
    {
        term->prpnt = 0;
        term->ptr = &term->queue[0];
        memset(&term->queue[0], 0, COOKED_MAX);
        term->cooked = 0;

        /* if echo is on, do a visual newline to visually confirm */
        if (term->echo)
        {
            ttyputc(term, '\n');
        }
    }

    return 0;
}

int ttygetopts(struct term_config* term, struct term_opts* opts)
{
    opts->prompt = term->prompt;
    opts->lcase = term->lcase;
    opts->echo = term->echo;
    opts->xon = term->xon;
    opts->raw = term->raw;
    opts->speed = term->speed;
    opts->timeout = term->timeout;
    opts->cdelay = term->cdelay;
    memcpy((void*)(&opts->control_chars), (void*)(&term->control_chars), sizeof(struct tc));
    opts->line_callback = term->line_callback;
    opts->full_callback = term->full_callback;
    opts->cancel_callback = term->cancel_callback;
    opts->erase_callback = term->erase_callback;
    return 0;
}
