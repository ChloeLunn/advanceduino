

#if !H_DNS
#define H_DNS 1

#include "ip.h"
#include "udp.h"

#ifndef __packed
#define __packed __attribute__((__packed__))
#endif

#ifndef __aligned
#define __aligned(x) __attribute__((__aligned__(x)))
#endif

#define DNS_PORT_UDP 53

/* ------------------------------------------------------------------------- */

//                                    1  1  1  1  1  1
//      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
//    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//    |                      ID                       |
//    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//    |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
//    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//    |                    QDCOUNT                    |
//    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//    |                    ANCOUNT                    |
//    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//    |                    NSCOUNT                    |
//    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//    |                    ARCOUNT                    |
//    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

#define DNS_QR_QUERY    0
#define DNS_QR_RESPONSE (1 << 15)

#define DNS_OPCODE_STANDARD 0

#define DNS_AA_FALSE 0
#define DNS_AA_TRUE  (1 << 10)

#define DNS_TC_FALSE 0
#define DNS_TC_TRUE  (1 << 9)

#define DNS_RD_FALSE 0
#define DNS_RD_TRUE  (1 << 8)

#define DNS_RA_FALSE 0
#define DNS_RA_TRUE  (1 << 7)

#define DNS_RCODE_MASK     (0x000F)
#define DNS_RCODE_ENONE    0
#define DNS_RCODE_EFRMT    1
#define DNS_RCODE_ESERV    2
#define DNS_RCODE_ENONAME  3
#define DNS_RCODE_ENOIMP   4
#define DNS_RCODE_EREFUSED 5

struct dnshdr {
    unsigned short dh_id;      /* id number */
    unsigned short dh_bf;      /* bitfield of flags, opcodes, return codes */
    unsigned short dh_qdcount; /* number of questions */
    unsigned short dh_ancount; /* number of answers */
    unsigned short dh_nscount; /* number of name server records (0 / ignore) */
    unsigned short dh_arcount; /* number of additional records (0 / ignore) */
} __packed;

#define DNS_QUESTION_TYPE_A      (0x0001)
#define DNS_QUESTION_CLASS_IN    (0x0001)
#define DNS_LEN_COMPRESSION_MASK (0xC0)

/* ------------------------------------------------------------------------- */

#endif
