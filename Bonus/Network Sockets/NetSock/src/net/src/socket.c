/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

#include "../socket.h"

#include "../../dev/dev.h"
#include "../if.h"
#include "../protocol.h"

#include <stdlib.h>
#include <string.h>

extern void debug_printline(const char* line);
extern void debug_print(const char* line);

#define NSOCKS     2 /* How many open sockets are allowed */
#define NRECVSTACK 2 /* How many packets to keep on receive */
#define NTRANSTACK 1 /* Unless the Phy is slow to transfer, this can likely stay as 1 */

/* file socket in kernel */
struct fsock {
    unsigned char s_rvalid[NRECVSTACK];
    unsigned char s_rbuf[NRECVSTACK][ETH_FRAME_LEN + sizeof(struct ifhdr)]; /* received packet buffers */
    int s_rptr;                                                             /* read pointer (which entry to read the next packet from) */
    int s_wptr;                                                             /* write pointer (which entry to write a packet into on receive)*/
    unsigned char s_tbuf[NTRANSTACK][ETH_FRAME_LEN + sizeof(struct ifhdr)]; /* single write buffer for writing out */
    int s_tptr;                                                             /* send buffer pointer */
    struct socket s_sock;                                                   /* socket settings */
} sockets[NSOCKS];

/* UTILS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

/* find a matching socket */
int sock_find(int family, int type, int proto, int start)
{
    for (int i = start; i < NSOCKS; i++)
    {
        /* same family */
        if (sockets[i].s_sock.s_family == family)
        {
            /* same type */
            if (sockets[i].s_sock.s_type == type)
            {
                /* same protocol */
                if (sockets[i].s_sock.s_protocol == proto)
                {
                    return i;
                }
            }
        }
    }
    return -1;
}

/* USERSPACE CALLED ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

/* this is "socket()" -> opens a socket */
int sock_open(int family, int type, int proto, int flag)
{
    /*
        it's sometimes possible to infer protocol from
        the other options if it's not given

        We only ever do this on open as then it will be set up for any
        other function
    */
    int saved_proto = proto;
    switch (family)
    {
    case PF_PACKET: /* PF_PACKETs can only be done raw and by receiving all ethernet packets */
        type = SOCK_RAW;
        proto = htons(ETH_P_ALL);
        break;
    case PF_INET:
        /* try and work out the protocol from the type */
        switch (type)
        {
        case SOCK_RAW:
            proto = IPPROTO_RAW;
            break;
        case SOCK_STREAM:
            if (proto == IPPROTO_IP)
                proto = IPPROTO_TCP;
            break;
        case SOCK_DGRAM:
            if (proto == IPPROTO_IP)
                proto = IPPROTO_UDP;
            break;
        default:
            return -1;
        }

        break;
    default:
        break;
    }

    /* try and find a free socket to reuse */
    for (int i = 0; i < NSOCKS; i++)
    {
        /* if we found a free one */
        if (sockets[i].s_sock.s_flag == SF_FREE)
        {
            sockets[i].s_sock.s_flag = SF_OPEN;
            sockets[i].s_sock.s_flag |= SF_BUSY;

            sockets[i].s_sock.s_family = family;
            sockets[i].s_sock.s_type = type;
            sockets[i].s_sock.s_protocol = proto;
            sockets[i].s_sock.s_rawproto = saved_proto;

            sockets[i].s_sock.s_opts = 0;

            /* clear buffer */
            memset(&sockets[i].s_rbuf, 0, sizeof(sockets[i].s_rbuf));

            /* set pointers */
            sockets[i].s_rptr = 0;
            sockets[i].s_wptr = 0;

            /* do protocol specific opens */
            int p = proto_find_sock(0, &sockets[i].s_sock);
            if (p < 0)
            {
                sockets[i].s_sock.s_flag &= ~SF_BUSY;
                return -1;
            }

            proto_open(p, &sockets[i].s_sock, flag);

            if (flag & SF_LOCK)
                sockets[i].s_sock.s_flag |= SF_LOCK;

            sockets[i].s_sock.s_flag &= ~SF_BUSY;
            return i;
        }
    }

    /* no dice.... */
    return -1;
}

/* closes a socket */
int sock_close(int sock, int flag)
{
    if (sock >= NSOCKS)
        return -1;

    if (sockets[sock].s_sock.s_flag == SF_FREE)
        return -1;

    while (sockets[sock].s_sock.s_flag & SF_BUSY)
        ;

    sockets[sock].s_sock.s_flag |= SF_BUSY;

    int p = proto_find_sock(0, &sockets[sock].s_sock);
    if (p >= 0)
        proto_close(p, &sockets[sock].s_sock, flag);

    sockets[sock].s_sock.s_flag &= ~SF_BUSY;

    sockets[sock].s_sock.s_flag = SF_FREE;

    return 0;
}

int sock_recvfrom(int sock, unsigned char* buf, int len, int flags, struct sockaddr* src_addr, int* addrlen)
{
    if (sock >= NSOCKS)
        return -1;

    if (sockets[sock].s_sock.s_flag == SF_FREE)
        return -1;

    while (sockets[sock].s_sock.s_flag & SF_BUSY)
        ;

    sockets[sock].s_sock.s_flag |= SF_BUSY;

    if (!sockets[sock].s_rvalid[sockets[sock].s_rptr])
    {
        sockets[sock].s_sock.s_flag &= ~SF_BUSY;
        return 0;
    }

    int p = proto_find_sock(0, &sockets[sock].s_sock);
    int r = -1;

    // debug_print("R");

    if (p >= 0)
        r = proto_recvfrom(p, &sockets[sock].s_sock, &sockets[sock].s_rbuf[sockets[sock].s_rptr][0], buf, len, flags, src_addr, addrlen);

    /* clear old data */
    sockets[sock].s_rvalid[sockets[sock].s_rptr] = 0;
    memset(&sockets[sock].s_rbuf[sockets[sock].s_rptr][0], 0, ETH_FRAME_LEN);
    /* move to next buffer */
    sockets[sock].s_rptr++;
    if (sockets[sock].s_rptr >= NRECVSTACK)
        sockets[sock].s_rptr = 0;

    sockets[sock].s_sock.s_flag &= ~SF_BUSY;

    return r;
}

int sock_sendto(int sock, unsigned char* buf, int len, int flags, struct sockaddr* dst_addr, int* addrlen)
{
    if (sock >= NSOCKS)
        return -1;

    if (sockets[sock].s_sock.s_flag == SF_FREE)
        return -1;

    while (sockets[sock].s_sock.s_flag & SF_BUSY)
        ;

    sockets[sock].s_sock.s_flag |= SF_BUSY;

    int p = proto_find_sock(0, &sockets[sock].s_sock);
    int r = -1;
    if (p >= 0)
    {
        r = proto_sendto(p, &sockets[sock].s_sock, &sockets[sock].s_tbuf[sockets[sock].s_tptr][0], buf, len, flags, dst_addr, addrlen);
    }
    /* move to next buffer */
    sockets[sock].s_tptr++;
    if (sockets[sock].s_tptr >= NTRANSTACK)
        sockets[sock].s_tptr = 0;

    sockets[sock].s_sock.s_flag &= ~SF_BUSY;

    return r;
}

int sock_bind(int sock, const struct sockaddr* addr, int addrlen)
{
    if (sock >= NSOCKS)
        return -1;

    if (sockets[sock].s_sock.s_flag == SF_FREE)
        return -1;

    while (sockets[sock].s_sock.s_flag & SF_BUSY)
        ;

    sockets[sock].s_sock.s_flag |= SF_BUSY;

    int p = proto_find_sock(0, &sockets[sock].s_sock);
    int r = -1;
    if (p >= 0)
        r = proto_bind(p, &sockets[sock].s_sock, addr, addrlen);

    sockets[sock].s_sock.s_flag &= ~SF_BUSY;

    return r;
}

int sock_connect(int sock, const struct sockaddr* addr, int addrlen)
{
    if (sock >= NSOCKS)
        return -1;

    if (sockets[sock].s_sock.s_flag == SF_FREE)
        return -1;

    while (sockets[sock].s_sock.s_flag & SF_BUSY)
        ;

    sockets[sock].s_sock.s_flag |= SF_BUSY;

    int p = proto_find_sock(0, &sockets[sock].s_sock);
    int r = -1;
    if (p >= 0)
        r = proto_connect(p, &sockets[sock].s_sock, addr, addrlen);

    sockets[sock].s_sock.s_flag &= ~SF_BUSY;

    return r;
}

/* NETWORK STACK CALLED ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

/* Called by proto dissects - finds matching protocols to let them do any special handling
   and puts the packet on them */
int sock_filter(int ps, int family, int type, int proto, struct iface* ifs, unsigned char* data)
{
    /*
        Find all sockets matching this family, type, proto, and pass them off to a protocol filter
    */

    int found = -1;
    int res = 0;

    while ((found = sock_find(family, type, proto, found + 1)) >= 0)
    {
        // debug_print("F");
        res = proto_filter(ps, &sockets[found].s_sock, ifs, data);

        /* when res < 0, this means no matching protocol, so we can leave */
        if (res < 0)
            break;
    }

    return 0;
}

/* Called by proto filters -> simply the common code for putting data onto a socket */
int sock_data_recv(struct socket* sock, unsigned char* data, int len)
{
    // debug_printline("SOCK: Socket has received data");

    for (int i = 0; i < NSOCKS; i++)
    {
        if (sock == &sockets[i].s_sock)
        {
            while (sockets[i].s_sock.s_flag & SF_BUSY)
                ;

            sockets[i].s_sock.s_flag |= SF_BUSY;

            sockets[i].s_rvalid[sockets[i].s_wptr] = 1;
            memcpy(&sockets[i].s_rbuf[sockets[i].s_wptr][0], data, len);
            sockets[i].s_wptr++;

            if (sockets[i].s_wptr >= NRECVSTACK)
                sockets[i].s_wptr = 0;

            sockets[i].s_sock.s_flag &= ~SF_BUSY;

            break;
        }
    }
    return 0;
}

int setsockopt(int sock, int level, int option_name, const void* option_value, int option_len)
{
    return 0;
}

int getsockopt(int sock, int level, int option_name, void* option_value, int option_len)
{
    return 0;
}
