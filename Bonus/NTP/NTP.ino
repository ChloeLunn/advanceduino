#include <ElapsedMillis.h>
#include <Ethernet.h>
#include <SPI.h>
#include <time.h>

/* See https://labs.apnic.net/?p=462 for loads of NTP info */

#define DEFAULT_NTP_SERVER "0.pool.ntp.org"

#define NTP_UDP_PORT     (123) /* UDP port for NTP transactions */
#define NTP_SYNC_SECONDS (60)  /* how often to sync time in seconds (let's go with 60), must be less than 128 */
#define NTP_TIMEOUT      (500) /* packet timeout in milliseconds */
#define NTP_PRECISION    (-18) /* -18 (236) is a resolution of 1 second */

/*
     *NIX systems use a base epoch of 00:00:00 1st Jan 1970
    POSIX and NTP use a base epoch of 00:00:00 1st Jan 1900

    So we define a constant to flick between different options
    like so:

        nix = ntp - NIX_OFFSET;
        ntp = nix + NIX_OFFSET;

    (70 years, + 17 leap years = (70*365 + 17)*86400 = 2208988800 seconds)

*/
#define NIX_OFFSET 2208988800LL

/* Swap bytes for a 32-bit value: Endianness swapper */
#define SWAB32(x) ((((x)&0xFF000000) >> 24) | (((x)&0x00FF0000) >> 16) | (((x)&0x0000FF00) << 16) | (((x)&0x000000FF) << 24))
/* In reality we need ntohl, but not all arduino boards have headers that do that properly without making the same assumption I have: that the host is little-endian */

/* A complete NTP packet as a struct */
struct __attribute__((packed)) ntp_packet {
    unsigned int n_leap_indicator : 2; /* Leap second indicator */
    unsigned int n_version : 3;        /* NTP version */
    unsigned int n_mode : 3;           /* Packet mode */
    char n_stratum;                    /* 1 = primary server, 2-15 = alternative servers */
    unsigned char n_poll;              /* maximum interval between messages in seconds */
    char n_precision;                  /* precision of clock in Log[2] seconds */
    unsigned long n_root_delay;        /* Delay between server request and source clock */
    unsigned long n_root_disp;         /* The maximum error due to clock frequency tolerance */
    char n_ref_ident[4];               /* Reference identity code */
    /*
    E.g.
        LOCL    uncalibrated local clock
        CESM    calibrated Cesium clock
        RBDM    calibrated Rubidium clock
        PPS     calibrated quartz clock or other pulse-per-second source
        IRIG    Inter-Range Instrumentation Group
        ACTS    NIST telephone modem service
        USNO    USNO telephone modem service<
        PTB     PTB (Germany) telephone modem service
        TDF     Allouis (France) Radio 164 kHz
        DCF     Mainflingen (Germany) Radio 77.5 kHz
        MSF     Rugby (UK) Radio 60 kHz
        WWV     Ft. Collins (US) Radio 2.5, 5, 10, 15, 20 MHz
        WWVB    Boulder (US) Radio 60 kHz
        WWVH    Kauai Hawaii (US) Radio 2.5, 5, 10, 15 MHz
        CHU     Ottawa (Canada) Radio 3330, 7335, 14670 kHz
        LORC    LORAN-C radionavigation system
        OMEG    OMEGA radionavigation system
        GPS     Global Positioning Service
    */
    unsigned long n_ref_time_sec;  /* time system clock was last set (full seconds) */
    unsigned long n_ref_time_frac; /* Fractional seconds */

    /*
        NTP fractional time splits the seconds into the first 32-bits and a decimal fractional
        in the second 32-bits. This means that the second fractions in ntp are in 1/2^32s of a second (agh!)
        This is the same kind of encoding used for fractional parts in IEEE Floats and Doubles, however
        the split is different and there's no exponential which is why we can't just cast to a double

        |0000|0000|0000|0000|0000|0000|0000|0000| . |0000|0000|0000|0000|0000|0000|0000|0000|
                 Integer Part                     |             Decimal Fractional Part
    */

    unsigned long n_origin_time_sec; /* time request left client for server */
    unsigned long n_origin_time_frac;

    unsigned long n_receive_time_sec; /* time request arrived at server */
    unsigned long n_receive_time_frac;

    unsigned long n_trans_time_sec; /* time server responded -- This is the one we read to get the time */
    unsigned long n_trans_time_frac;
};

/* Leap second indicators */
enum
{
    li_none = 0b00,     /* No leap second (ends as 23:58..23:59..00:00) */
    li_positive = 0b01, /* Today is 1 second longer than usual (ends as 23:59..23:60..00:00) */
    li_negative = 0b10, /* Today is 1 second shorter than tomorrow (ends as 23:57..23:58..00:00) */
    li_unsync = 0b11,   /* Client doesn't/isn't sync to leaps */
};

/* Current NTP version */
enum
{
    v_current = 0b100, /* Current NTP version is 4. */
};

/* NTP Modes */
enum
{
    m_reserved = 0b000,
    m_sym_active = 0b001,
    m_sym_passive = 0b010,
    m_client = 0b011,
    m_server = 0b100,
    m_broadcast = 0b101,
    m_control = 0b110,
    m_private = 0b111,
};

/* Stratum types */
enum
{
    s_unspecified = 0,
    s_primary = 1,
    s_secondary = 2, /* 2-15 */
    s_unsynched = 16,
    s_reserved = 17, /* 17-255 */
};

EthernetUDP NTP_UDP_SOCK;  // UDP socket
int NTP_SOCK_OPEN = 0;     /* have we already opened this socket */
struct timeval system_tv;

/*
    Get time from the NTP server:
        returns unix epoch in whole seconds
        but also sets timeval pointer with fractional seconds
 */
time_t ntp_gettime(struct timeval* tv)
{
    struct ntp_packet ntp_pbuf; /* packet buffer */

    time_t ntp_epoch = 0;      /* temporary var to store ntp epoch */
    time_t unix_epoch = 0;     /* temporary var to store unix epoch, we return this */
    time_t ntp_fractional = 0; /* temporary var to store ntp fractional seconds */

    /* Clear the ntp packet buffer */
    memset((void*)&ntp_pbuf, 0, sizeof(struct ntp_packet));

    /* Setup the NTP packet */
    ntp_pbuf.n_leap_indicator = li_unsync; /* Don't sync leap seconds locally */
    ntp_pbuf.n_version = v_current;        /* 4 */
    ntp_pbuf.n_mode = m_client;            /* We are an NTP client */
    ntp_pbuf.n_stratum = s_unspecified;    /* Don't have one of those */
    ntp_pbuf.n_poll = NTP_SYNC_SECONDS;    /* Once a minute */
    ntp_pbuf.n_precision = NTP_PRECISION;  /* 1-second resolution */

    /* We're not going to set the root delay and dispersion because we're a client */

    /* We can put anything here, but just for fun, here's a nixie tube number that's popular for ntp clock projects */
    ntp_pbuf.n_ref_ident[0] = '1';
    ntp_pbuf.n_ref_ident[1] = 'N';
    ntp_pbuf.n_ref_ident[2] = '1';
    ntp_pbuf.n_ref_ident[4] = '4';
    /* I would recommend "LCL\0" or "LOCL" if this was changed as this usually means unspecified local clock */

    /* We don't need to fill in the rest of the packet, as we're the client and not the server. */

    /* if we've yet to create the NTP socket, open it. */
    if (!NTP_SOCK_OPEN)
    {
        NTP_UDP_SOCK.begin(NTP_UDP_PORT);
        NTP_SOCK_OPEN++;
    }

    /* Then write out our request. The library handles the server URL DNS requests */
    NTP_UDP_SOCK.beginPacket(DEFAULT_NTP_SERVER, NTP_UDP_PORT);
    NTP_UDP_SOCK.write((unsigned char*)&ntp_pbuf, sizeof(struct ntp_packet));
    NTP_UDP_SOCK.endPacket();

    /*
        We're going to wait for our reply with millis for checking the time out,
        that way it's more portable if you just want this bit and not the whole system time
        code.
    */

    long time_out = millis();

    /* wait until a packet arrives, or if we run out of time */
    while (!NTP_UDP_SOCK.available() && !(millis() - time_out > NTP_TIMEOUT))
    {
        ;
    }

    /*
        Hopefully the only packet received on our NTP port socket is an actual NTP packet.
        We could probably do with some more checks here.
     */
    if (NTP_UDP_SOCK.parsePacket())
    {
        /* Read the data from the socket into our buffer struct */
        NTP_UDP_SOCK.read((unsigned char*)&ntp_pbuf, sizeof(struct ntp_packet));

        /* Load the trans time, and swap the big endian to little endian */
        ntp_epoch = SWAB32(ntp_pbuf.n_trans_time_sec);

        /*
            You may have noticed that ntp only gave us a 32-bit epoch, and that's true! Even though our system time uses 64-bit epochs.

            That means this code will stop working in 2036, when there will be a new NTP version/system that handles it in 64-bit because the ntp epoch roles over.

            Exercise: Implement a simple roll-over check here and use it to get more time out of the same time
        */

        /* And finally convert the ntp epoch into our system time's unix epoch */
        unix_epoch = (ntp_epoch - NIX_OFFSET);

        if (tv != NULL)
        {
            /* Endianess again. */
            ntp_fractional = SWAB32(ntp_pbuf.n_trans_time_frac);

            /* And now do the conversion from 1/2^32s of a second to decimal microseconds  */

            ntp_fractional *= 1000 * 1000; /* multiply to convert into microseconds when we do the.. */
            ntp_fractional >>= 32;         /* ...divide by 2^32 (bitshift for faster op) to go from bit-ish ntp to real decimal */

            /* and then we can save the fractional and unix tod into the tv struct */
            tv->tv_sec = unix_epoch;
            tv->tv_usec = ntp_fractional;
        }

        /* and return the simple unix epoch */
        return unix_epoch;
    }

    /*
        If we got here then something went wrong grabbing the time, so we need to return an error code.
        We know that an NTP request can't realistically give us a time before 1970 (the UNIX epoch), so we'll use -1 as an error return
    */
    return -1;
}

time_t last_ntp_check = 0;
void ntp_daemon(void)
{
    /* if we've reached the ntp re-fetch time (once per minute) */
    if (millis() - last_ntp_check > NTP_SYNC_SECONDS * 1000)
    {
        time_t epoch = ntp_gettime(&system_tv);
    }
}

time_t last_time_print = 0;
void print_daemon(void)
{
    if (millis() - last_time_print > 1000)
    {
        Serial.printf("The Time is ");
    }
}

void setup()
{
    while (!Serial)
    {
        delay(100);
    }
    Serial.println("Starting loop");
}

void loop()
{
    ntp_daemon();
}
