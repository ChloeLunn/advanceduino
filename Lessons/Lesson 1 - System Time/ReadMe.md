# Lesson 1 - System Time

## The System Tick

The core of all timekeeping and how preemptive multitasking (that is, tasks are swapped around based on time running) relies on the "system tick" interrupt.

This is a timer interrupt of which some variation exists on all modern processors, which is triggered (often) every millisecond of execution time.

We're going to use this to set up a clock which will then print out the current date and time onto the terminal/serial port.

To do this, we need to tap into this interrupt.

Luckily for us, the Arduino library does something pretty neat and lets us tap into the systick interrupt regardless of the underlying architecture and the way you need to set an interrupt handler on that target.

You just need to make sure that you have a function "int sysTickHook(void)" to do it 

On ARM, we could create a function void SysTick_Handler(void) to do the same thing, but the Arduino/Adafruit libraries don't properly mark it as weak, so we can't do that.

We would have to create a new function, say void Nu_SysTick_Handler(void), and then use the call NVIC_SetHandler(SysTick_IRQn, Nu_SysTick_Handler); to tell it to use the new handler.

As this would be a messy work around, and we don't know which libraries use the systick or values that it sets, we'll avoid doing it that way.

### How do we use this?

Essentially, in order to keep time, all we need to do is increment a milliseconds counter every time this interrupt is triggered. 

Then once we've run out of how many milliseconds there are in a second, we start incrementing our seconds.

But we don't start incrementing minutes, hours, days, and so forth in this function. That's because calculating those adds time while we're in the interrupt which can mean that we end up processing for more than a millisecond and so the clock drifts as the interrupt can't keep up.

```
int sysTickHook(void)
{
    /* increment the system time-of-day micros */
    sys_tv.tv_usec += MICROS_PER_CLOCK;

    /* if we've reached 1 second */ 
    if (sys_tv.tv_usec >= MICROS_PER_SEC)
    {
        /* decrement the micros */
        sys_tv.tv_usec -= MICROS_PER_SEC;

        /* increment the system time of day seconds value */
        sys_tv.tv_sec += 1;
    }

    return 1;
}
```

Note that we used >= and -= in case a different interrupt or manual time setting has shifted the timing. This assumes that if something did shift it, then it's always less than a second total shift.

## Data structures

To store our system time at the lowest level, we're going to use a structure called a 'timeval'. This is actually a now out-dated structure, but it stores time in a combination of seconds and microseconds, which allows for enough resolution for the systick, but is closer to our 1ms resolution than the more modern timespec which uses nanoseconds.

This structure looks something like this:

```
struct timeval {
    time_t tv_sec;       /* seconds */
    suseconds_t tv_usec; /* and microseconds */
};
```

As you can see, not too complicated. The types ending with _t are just different types of integer, so no need to worry about them.

We're also going to use a POSIX time structure in our interface that does printing of the time and getting/setting the time. 

This is a 'human readable' structure and simply breaks the time up into how we would normally use it for storing things on calendars and schedules.

```
/* POSIX time structure */
struct tm {
    int tm_sec;   /* seconds in minute */
    int tm_min;   /* minutes in hour */
    int tm_hour;  /* hour in day */
    int tm_mday;  /* day in month */
    int tm_wday;  /* day in week */
    int tm_mon;   /* month in year */
    int tm_year;  /* year since 1900 */
    int tm_yday;  /* day in year */
    int tm_isdst; /* daylight Saving Time flag */
};
```

The daylight savings flag is just a number that denotes different types or zero when not in use, and we're not going to worry about it in this lesson.

The values are:

1. USA style dst
2. Australian style dst
3. Western European dst
4. Middle European dst
5. Eastern European dst
6. Canadian style dst

## The tick hook
