/*
Copyright 2022 Chloe Lunn

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#if !H_DRIVERS
#define H_DRIVERS 1

/* include the tty info, we're going to use it in our driver setups */
#include "tty.h"

/* These may need changing for your device, you can comment them out to disable the driver for them */
#define USB_SER  Serial
#define UART_SER Serial1

/*
    I've been a bad girl and included actual C function definitions in this header...
    Remember that you're not really supposed to do that unless functions are declared as static
    So that you don't get more than one definition of them across multiple objects compiled
    against the same header.
 */

/* Nice and common default speed... though I have used a UNIX system where the default was only 100 before! */
#define default_baud (9600)

/* ~~~~~~~~~~~~~~~~ USB DRIVER CODE ~~~~~~~~~~~~~~~~ */

#if defined(USB_SER)

/* When we open a device, we need to create a basic  */
int usb_open(int dev)
{
    /* if the device is not a valid number, then we want to return with an error */
    if (major(dev) != 0)
        return -1;

    if (minor(dev) != 0)
        return -1;

    ttys.t_dev = dev; /* link the tty to this device */

    USB_SER.begin(default_baud); /* Start the arduino driver */

    ttys.t_speed = default_baud; /* Set the tty settings to match the driver defaults */
    ttys.t_timeout = 1000;
    USB_SER.setTimeout(ttys.t_timeout);

    /* We can't &=~ the whole block because we want to keep the TTY_WAITOPEN flag */
    ttys.t_mode &= ~TTY_ODDP;
    ttys.t_mode &= ~TTY_EVENP;

    /*
        Exercise: On a SAMD the usb serial speed is not real, it doesn't care what the baudrate is.
        Can you implement a way of saying that we don't care about baudrate in the tty settings?
    */
}

int usb_close(int dev)
{
    /* if the device is not a valid number, then we want to return with an error */
    if (major(dev) != 0)
        return -1;

    if (minor(dev) != 0)
        return -1;

    /* Close it on the Arduino end */
    USB_SER.end();

    /* This links the tty to the NULL driver, so it's dereferenced from functions if it's used again before another open */
    ttys.t_dev = num_drivers;
}

int usb_read(int dev)
{
    /* if the device is not a valid number, then we want to return with an error */
    if (major(dev) != 0)
        return -1;

    if (minor(dev) != 0)
        return -1;

    /*
        The Serial.read() function already returns a -1 when there's a problem, so we don't need to
        wrap this any further.
     */
    return USB_SER.read();
}

int usb_write(int dev, int c)
{
    /* if the device is not a valid number, then we want to return with an error */
    if (major(dev) != 0)
        return -1;

    if (minor(dev) != 0)
        return -1;

    USB_SER.write(c);
    return 0;
}

int usb_push(int dev)
{
    /* if the device is not a valid number, then we want to return with an error */
    if (major(dev) != 0)
        return -1;

    if (minor(dev) != 0)
        return -1;

    /* Change serial settings to match the latest tty settings */
    USB_SER.begin(ttys.t_speed);
    USB_SER.setTimeout(ttys.t_timeout);

    /* Speed is the only one we're going to control, so we'll ignore the others. */

    USB_SER.flush(); /* And flush any pending */
}

#endif

/* ~~~~~~~~~~~~~~~~ SERIAL DRIVER CODE ~~~~~~~~~~~~~~~~ */

#if defined(UART_SER)

/* When we open a device, we need to create a basic  */
int ser_open(int dev)
{
    /* if the device is not a valid number, then we want to return with an error */
    if (major(dev) != 1)
        return -1;

    if (minor(dev) != 0)
        return -1;

    ttys.t_dev = dev; /* link the tty to this device */

    UART_SER.begin(default_baud); /* Start the arduino driver */

    ttys.t_speed = default_baud; /* Set the tty settings to match the driver defaults */
    ttys.t_timeout = 1000;
    UART_SER.setTimeout(ttys.t_timeout);
    ttys.t_mode &= ~TTY_ODDP;
    ttys.t_mode &= ~TTY_EVENP;
}

int ser_close(int dev)
{
    /* if the device is not a valid number, then we want to return with an error */
    if (major(dev) != 1)
        return -1;

    if (minor(dev) != 0)
        return -1;

    /* Close it on the Arduino end */
    UART_SER.end();

    /* This links the tty to the NULL driver, so it's dereferenced from functions if it's used again before another open */
    ttys.t_dev = num_drivers;
}

int ser_read(int dev)
{
    /* if the device is not a valid number, then we want to return with an error */
    if (major(dev) != 1)
        return -1;

    if (minor(dev) != 0)
        return -1;

    /*
        The Serial1.read() function already returns a -1 when there's a problem, so we don't need to
        wrap this any further.
     */
    return UART_SER.read();
}

int ser_write(int dev, int c)
{
    /* if the device is not a valid number, then we want to return with an error */
    if (major(dev) != 1)
        return -1;

    if (minor(dev) != 0)
        return -1;

    UART_SER.write(c);
    return 0;
}

int ser_push(int dev)
{
    /* if the device is not a valid number, then we want to return with an error */
    if (major(dev) != 1)
        return -1;

    if (minor(dev) != 0)
        return -1;

    /* Change serial settings to match the latest tty settings */
    UART_SER.begin(ttys.t_speed);
    UART_SER.setTimeout(ttys.t_timeout);

    /* Speed is the only one we're going to control, so we'll ignore the others. */

    UART_SER.flush(); /* And flush any pending */
}
#endif

/* ~~~~~~~~~~~~~~~~ MINI DEVICE DRIVER TABLE ~~~~~~~~~~~~~~~~ */

/*
    This is the bare minimum required for a character device in a *NIX
    Kernel.

    Tables like this still exist even in the latest BSD, Darwin, and Linux Kernels
    and are the simplest form of driver in a UNIX-like system.

    The macros are often available on both user and kernel side code, and are used
    to make device numbers, or to disect device numbers into their component parts.

    Typically the major number is the device driver number for the table below,
    and the minor number is then the unit inside that.

    For example, if we were to define more Serial devices, then we may only have the single
    common driver calls, and table entry, but instead have the different serial uarts on
    different minor numbers within that driver.
*/

#define make_dev(major, minor) (short)((((major)&0xFF) << 8) | ((minor)&0xFF))
#define major(dev)             (unsigned char)(((dev) >> 8) & 0xFF)
#define minor(dev)             (unsigned char)(((dev) >> 0) & 0xFF)

#define default_system_console (make_dev(0, 0)) /* Which device do we want to initialise as the main system control console */

/* Simple driver layout */
struct char_driver {
    int (*d_open)(int);       /* Open the device */
    int (*d_close)(int);      /* Close the device */
    int (*d_read)(int);       /* Read from the device */
    int (*d_write)(int, int); /* Write a value to the device */
    int (*d_push)(int);       /* push any tty device changes to the driver */
};

struct char_driver dev_table[] = {
#if defined(USB_SER)
  {usb_open, usb_close, usb_read, usb_write, usb_push}, /* Major 0 == USB Serial (Arduino Serial or SerialUsb) */
#else
  {NULL, NULL, NULL, NULL, NULL}
#endif
#if defined(UART_SER)
  {ser_open, ser_close, ser_read, ser_write, ser_push}, /* Major 1 == UART Serial (Arduino Serial1) */
#else
  {NULL, NULL, NULL, NULL, NULL}
#endif
  {NULL, NULL, NULL, NULL, NULL} /* Throw a blank device in there, for padding and for the num_drivers calc */
};

/* how many drivers are available */
static const int num_drivers = (sizeof(dev_table) / sizeof(struct char_driver));

#endif
