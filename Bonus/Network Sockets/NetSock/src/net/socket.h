/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

#if !SOCKET_H
#define SOCKET_H 1

#include "ip.h"

#ifndef __packed
#define __packed __attribute__((__packed__))
#endif

#ifndef __aligned
#define __aligned(x) __attribute__((__aligned__(x)))
#endif

/* SOCKET STRUCT ----------------------------------------------------------- */

struct sockproto {
    char sp_family;             /* address family */
    unsigned short sp_protocol; /* protocol */
} __packed;

struct sockaddr_in {
    char sin_family;         /* AF_INET */
    unsigned short sin_port; /* port in network byte order */
    struct in_addr sin_addr; /* internet address */
    char sin_zero[8];
} __packed;

struct sockaddr_in6 {
    char sin6_family;           /* AF_INET6 */
    unsigned short sin6_port;   /* port number */
    unsigned int sin6_flowinfo; /* IPv6 flow information */
    struct in6_addr sin6_addr;  /* IPv6 address */
    unsigned int sin6_scope_id; /* Scope ID (new in Linux 2.4) */
} __packed;

struct sockaddr {
    char sa_family;   /* address family: AF_INET */
    char sa_data[24]; /* addressing data */
} __packed;

struct sa_endpoints {
    int sae_srcif;                /* optional source interface */
    struct sockaddr* sae_srcaddr; /* optional source address */
    int sae_srcaddrlen;           /* size of source address */
    struct sockaddr* sae_dstaddr; /* destination address */
    int sae_dstaddrlen;           /* size of destination address */
} __packed;

struct socket {
    int s_family;            /* protocol family PF_xxx */
    int s_type;              /* protocol type SOCK_xxx */
    int s_protocol;          /* sub protocol e.g. IPPROTO_XXX */
    int s_rawproto;          /* sub protocol, as passed */
    unsigned short s_flag;   /* socket flags */
    unsigned short s_opts;   /* socket options SO_xxx*/
    struct sockaddr s_addr;  /* this socket's address information */
    struct sockaddr s_daddr; /* destination socket's address information */
};

/* DEFINITIONS ------------------------------------------------------------- */

/* Socket types */
#define SOCK_RAW    0 /* raw socket to the chosen AF level */
#define SOCK_STREAM 1 /* stream (TCP) socket */
#define SOCK_DGRAM  2 /* datagram (UDP) socket */

/* Supported address families */
#define AF_UNSPEC 0 /* none, remove the info */
#define AF_INET   1 /* Internet IP Protocol */
#define AF_PACKET 2 /* Ethernet/WLAN packet */

/* Protocol families, same as address families */
#define PF_UNSPEC AF_UNSPEC
#define PF_INET   AF_INET
#define PF_PACKET AF_PACKET

/* Socket flags */
#define SF_FREE 0x00 /* socket is free for use */
#define SF_OPEN 0x01 /* socket is open */
#define SF_LOCK 0x02 /* socket is locked from use */
#define SF_BUSY 0x04 /* socket is busy being processed */
#define SF_BIND 0x08 /* socket s_addr is bound */
#define SF_CONN 0x10 /* socket s_daddr is connected */

/* Level number for (get/set)sockopt() to apply to socket itself */
#define SOL_SOCKET 07777 /* options for socket level */
#define SOL_RAW    00    /* Raw Packet, no options */
#define SOL_INET   01    /* Raw IP Packet */
#define SOL_TCP    02    /* TCP packet */
#define SOL_UDP    04    /* UDP packet */

/* Socket Options flags per-socket */
#define SO_IPMASK      0x00000FF /* IP sock opts */
#define SO_TCPMASK     0x0000F00 /* IP sock opts */
#define SO_GENMASK     0xFFFF000
#define SO_DEBUG       0x0001000 /* turn on debugging info recording */
#define SO_ACCEPTCONN  0x0002000 /* socket has had listen() */
#define SO_REUSEADDR   0x0004000 /* allow local address reuse */
#define SO_KEEPALIVE   0x0008000 /* keep connections alive */
#define SO_DONTROUTE   0x0010000 /* just use interface addresses */
#define SO_BROADCAST   0x0020000 /* permit sending of broadcast msgs */
#define SO_USELOOPBACK 0x0040000 /* bypass hardware when possible */
#define SO_LINGER      0x0080000 /* linger on close if data present */
#define SO_OOBINLINE   0x0100000 /* leave received OOB data in line */
#define SO_DONTLINGER  (unsigned int)(~SO_LINGER)
#define SO_PEERCRED    0x0200000 /* same as getpeereid */
#define SO_PASSCRED    0x0400000 /* enable receiving of credentials */
#define SO_SNDBUF      0x1001000 /* send buffer size */
#define SO_RCVBUF      0x1002000 /* receive buffer size */
#define SO_SNDLOWAT    0x1003000 /* send low-water mark */
#define SO_RCVLOWAT    0x1004000 /* receive low-water mark */
#define SO_SNDTIMEO    0x1005000 /* send timeout */
#define SO_RCVTIMEO    0x1006000 /* receive timeout */
#define SO_ERROR       0x1007000 /* get error status and clear */
#define SO_TYPE        0x1008000 /* get socket type */

/* ------------------------------------------------------------------------- */

#endif /*SOCKET_H*/
