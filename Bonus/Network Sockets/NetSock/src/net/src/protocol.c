/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

#include "../protocol.h"

#include "../socket.h"
//
#include "./ether_proto.h"
#include "./ip_proto.h"
#include "./udp_proto.h"

#include <stddef.h>

extern void debug_printline(const char* line);

/*
    Protocol table MUST be ordered lower layer to higher layer.
    This is because the switching is optimised such that dissect only looks at protocols higher than itself in the switchway
*/
const struct proto protocols[PS_MAX] = {
  /* Interface called dissectors */
  [PS_ETH_ANY] = {PF_PACKET, SOCK_RAW, htons(ETH_P_INTERNAL_ANY), ether_dissect, ether_build},

  /* Ethernet protocol dissectors */
  [PS_ETH_ALL] = {PF_PACKET, SOCK_RAW, htons(ETH_P_ALL), ether_dissect, ether_build, ether_filter, ether_open, ether_close, ether_recvfrom, NULL, ether_bind, ether_connect},
  [PS_ETH_IP] = {PF_PACKET, SOCK_RAW, htons(ETH_P_IP), ip_dissect, ip_build},

  /* IP protocol dissectors */
  [PS_IP_ALL] = {PF_INET, SOCK_RAW, IPPROTO_RAW, ip_dissect, ip_build, ip_filter, ip_open, ip_close, ip_recvfrom, ip_sendto, ip_bind, ip_connect},
  [PS_IP_UDP] = {PF_INET, SOCK_DGRAM, IPPROTO_UDP, udp_dissect, udp_build, udp_filter, udp_open, udp_close, udp_recvfrom, udp_sendto, udp_bind, udp_connect},
  [PS_IP_UDPLITE] = {PF_INET, SOCK_DGRAM, IPPROTO_UDPLITE, udp_dissect, udp_build, udp_filter, udp_open, udp_close, udp_recvfrom, udp_sendto, udp_bind, udp_connect}, /* the udp and udplite code is compatible, so no differences here */
};

#define PROTOCOLS_MAX PS_MAX

int proto_find_sock(int psw, struct socket* sock)
{
    for (int i = psw; i < PROTOCOLS_MAX; i++)
    {
        if (protocols[i].p_family == sock->s_family)
            if (protocols[i].p_type == sock->s_type)
                if (protocols[i].p_protocol == sock->s_protocol)
                    return i;
    }
    return -1;
}

int proto_find(int psw, int family, int type, int proto)
{
    int idx = -1;

    for (int i = psw; i < PROTOCOLS_MAX && idx < 0; i++)
    {
        if (protocols[i].p_family == family)
            if (protocols[i].p_type == type)
                if (protocols[i].p_protocol == proto)
                    idx = i;
    }

    if (idx < 0)
        return -1;

    return idx;
}

int proto_dissect(int psw, int family, int type, int proto, struct iface* ifs, unsigned char* data)
{
    if (protocols[psw].p_dissect)
        return protocols[psw].p_dissect(psw, family, type, proto, ifs, data);

    return -1;
}

int proto_build(int psw, int family, int type, int proto, struct iface* ifs, unsigned char* data, int len, struct sockaddr* src_addr, struct sockaddr* dst_addr)
{
    if (protocols[psw].p_build)
        return protocols[psw].p_build(psw, family, type, proto, ifs, data, len, src_addr, dst_addr);

    return -1;
}

int proto_open(int psw, struct socket* sock, int flag)
{
    if (protocols[psw].p_open)
        return protocols[psw].p_open(sock, flag);

    return -1;
}

int proto_close(int psw, struct socket* sock, int flag)
{
    if (protocols[psw].p_close)
        return protocols[psw].p_close(sock, flag);

    return -1;
}

int proto_filter(int psw, struct socket* sock, struct iface* ifs, unsigned char* data)
{
    if (protocols[psw].p_filter)
        return protocols[psw].p_filter(sock, ifs, data);

    return -1;
}

int proto_setopt(int psw, struct socket* sock, int level, int option_name, const void* option_value, int option_len)
{
    if (protocols[psw].p_setopt)
        return protocols[psw].p_setopt(sock, level, option_name, option_value, option_len);

    return -1;
}

int proto_getopt(int psw, struct socket* sock, int level, int option_name, void* option_value, int option_len)
{
    if (protocols[psw].p_getopt)
        return protocols[psw].p_getopt(sock, level, option_name, option_value, option_len);

    return -1;
}

int proto_bind(int psw, struct socket* sock, const struct sockaddr* addr, int addrlen)
{
    if (protocols[psw].p_bind)
        return protocols[psw].p_bind(sock, addr, addrlen);

    return -1;
}

int proto_connect(int psw, struct socket* sock, const struct sockaddr* addr, int addrlen)
{
    if (protocols[psw].p_connect)
        return protocols[psw].p_connect(sock, addr, addrlen);

    return -1;
}

int proto_recvfrom(int psw, struct socket* sock, unsigned char* data, unsigned char* buf, int len, int flags, struct sockaddr* src_addr, int* addrlen)
{
    if (protocols[psw].p_recvfrom)
        return protocols[psw].p_recvfrom(sock, data, buf, len, flags, src_addr, addrlen);

    return -1;
}

int proto_sendto(int psw, struct socket* sock, unsigned char* data, unsigned char* buf, int len, int flags, struct sockaddr* dst_addr, int* addrlen)
{
    if (protocols[psw].p_sendto)
        return protocols[psw].p_sendto(sock, data, buf, len, flags, dst_addr, addrlen);

    return -1;
}
