
#include <Arduino.h>

extern "C"
{
#include "./dev/dev.h"
#include "./net/if.h"
#include "./net/socket.h"

    int sock_open(int family, int type, int proto, int flag);
    int sock_bind(int sock, const struct sockaddr* addr, int addrlen);
    int sock_connect(int sock, const struct sockaddr* addr, int addrlen);
    int sock_recvfrom(int sock, unsigned char* buf, int len, int flags, struct sockaddr* src_addr, int* addrlen);
    int sock_sendto(int sock, unsigned char* buf, int len, int flags, struct sockaddr* dst_addr, int* addrlen);

    void debug_printline(const char* line)
    {
        Serial.println(line);
    }

    void debug_print(const char* line)
    {
        // return;
        Serial.print(line);
    }

    void debug_dump(unsigned char* data, int len)
    {
        Serial.printf("dumped payload = \r\n");
        for (int i = 0; i < len; i++)
        {
            Serial.printf("%02X ", data[i]);
        }
        Serial.printf("\r\n");
    }
}

int sock = -1;
unsigned char packet_buffer[500];

extern "C" void setup()
{
    Serial.begin(9600);
    while (!Serial)
        ;

    Serial.println("Initialising Wiznet");

    /* initialise the device driver -> normally done by kernel */
    if (netsw[DEV_MAJOR_WZ].nw_init(makedev(DEV_MAJOR_WZ, 0), 0, 0) < 0)
    {
        Serial.println("Failed to initialise phy. Stopping.");
        while (1)
            ;
    }
    /*
        Note that init only initialises the phy, it doesn't actually bring it online for receiving packets.
        That requires an IOCTL that sets the IFF_UP flag
    */

    /* set a mac address -> normally ioctl */
    netsw[DEV_MAJOR_WZ].nw_ifs[0]->ifr_hwaddr.m_addr[0] = 0x22;
    netsw[DEV_MAJOR_WZ].nw_ifs[0]->ifr_hwaddr.m_addr[1] = 0x00;
    netsw[DEV_MAJOR_WZ].nw_ifs[0]->ifr_hwaddr.m_addr[2] = 0x00;
    netsw[DEV_MAJOR_WZ].nw_ifs[0]->ifr_hwaddr.m_addr[3] = 0x00;
    netsw[DEV_MAJOR_WZ].nw_ifs[0]->ifr_hwaddr.m_addr[4] = 0x00;
    netsw[DEV_MAJOR_WZ].nw_ifs[0]->ifr_hwaddr.m_addr[5] = 0x22;

    /* set ip addresses -> normally ioctl */
    netsw[DEV_MAJOR_WZ].nw_ifs[0]->ifr_addr.s_addr = htonl(0xC0A80114);      /* 192.168.1.20 */
    netsw[DEV_MAJOR_WZ].nw_ifs[0]->ifr_broadaddr.s_addr = htonl(0xC0A801FF); /* 192.168.1.255 */
    netsw[DEV_MAJOR_WZ].nw_ifs[0]->ifr_netmask.s_addr = htonl(0xFFFFFF00);   /* 255.255.255.0 */
    netsw[DEV_MAJOR_WZ].nw_ifs[0]->ifr_gateaddr.s_addr = htonl(0xC0A801FE);  /* 192.168.1.254 */
    netsw[DEV_MAJOR_WZ].nw_ifs[0]->ifr_dnsaddr.s_addr = htonl(0xC0A801FE);   /* 192.168.1.254 */

    /* set broadcast allowed, promiscious, debug, and set interface as up -> normally ioctl */
    netsw[DEV_MAJOR_WZ].nw_ifs[0]->ifr_flags |= IFF_BROADCAST;
    // netsw[DEV_MAJOR_WZ].nw_ifs[0]->ifr_flags |= IFF_PROMISC;  // this doesn't do much on a wiznet chip
    // netsw[DEV_MAJOR_WZ].nw_ifs[0]->ifr_flags |= IFF_DEBUG;   // this just adds debug prints on low level handlers
    netsw[DEV_MAJOR_WZ].nw_ifs[0]->ifr_flags |= IFF_UP;

    /* Bring the wiznet chip up, configure registers to match config/addresses */
    if (dev_ioctl(makedev(DEV_MAJOR_WZ, 0), 0, NULL, 0) < 0)
    {
        Serial.println("Failed to ioctl phy. Stopping.");
        while (1)
            ;
    }

    Serial.println("Opening UDP Socket");

    /* open a UDP socket */
    sock = sock_open(PF_INET, SOCK_DGRAM, IPPROTO_UDP, 0);

    if (sock < 0)
    {
        Serial.println("Failed to open socket. Stopping.");
        while (1)
            ;
    }

    Serial.println("Binding Socket to UDP port 21928");

    struct sockaddr_in saddr;
    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    saddr.sin_port = htons(21928);

    if (sock_bind(sock, (struct sockaddr*)&saddr, sizeof(struct sockaddr_in)) < 0)
    {
        Serial.println("Failed to bind socket. Stopping.");
        while (1)
            ;
    }
}

extern "C" void loop()
{
    // Serial.println("Polling hardware");

    dev_poll(makedev(DEV_MAJOR_WZ, 0), 0);

    static struct sockaddr_in saddr;
    static int slen = sizeof(struct sockaddr_in);
    static int rlen = -1;

    // Serial.println("Attempting Receive From");

    rlen = sock_recvfrom(sock, &packet_buffer[0], sizeof(packet_buffer), 0, (struct sockaddr*)&saddr, &slen);

    if (rlen < 0)
    {
        Serial.println("Failed to receive on socket.");
    }
    else if (rlen > 0)
    {
        Serial.printf("\r\n"
                      "UDP Packet received:\r\n");
        Serial.printf("====================\r\n");
        Serial.printf("saddr.sin_family = %i\r\n", saddr.sin_family);
        Serial.printf("saddr.sin_port   = %i\r\n", ntohs(saddr.sin_port));
        Serial.printf("saddr.sin_addr   = %08X\r\n", ntohl(saddr.sin_addr.s_addr));
        Serial.printf("udp.length       = %i\r\n", rlen);
        Serial.printf("udp.payload      = ");
        for (int i = 0; i < rlen; i++)
        {
            Serial.printf("%02X ", packet_buffer[i]);
        }
        Serial.printf("\r\n");

        /* bounce the message back, but change the first byte */
        Serial.printf("Pinging message back to sender\r\n");
        rlen = sock_sendto(sock, &packet_buffer[0], rlen, 0, (struct sockaddr*)&saddr, &slen);
        if (rlen < 0)
        {
            Serial.println("Failed to send on socket.");
        }
    }
}
