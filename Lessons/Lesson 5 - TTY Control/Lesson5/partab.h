#if !H_PARTAB
#define H_PARTAB 1

#define CTRL(c)   ((c)&037)
#define UNCTRL(c) ((c) + 0100)

/* ASCII control characters */
#define NUL  CTRL('@')
#define SOH  CTRL('A')
#define STX  CTRL('B')
#define ETX  CTRL('C')
#define EOT  CTRL('D')
#define ENQ  CTRL('E')
#define ACK  CTRL('F')
#define BEL  CTRL('G')
#define BS   CTRL('H')
#define TAB  CTRL('I')
#define HT   TAB
#define LF   CTRL('J')
#define VT   CTRL('K')
#define FF   CTRL('L')
#define CR   CTRL('M')
#define SO   CTRL('N')
#define SI   CTRL('O')
#define DLE  CTRL('P')
#define DC1  CTRL('Q')
#define XON  DC1
#define DC2  CTRL('R')
#define DC3  CTRL('S')
#define XOFF DC3
#define DC4  CTRL('T')
#define NAK  CTRL('U')
#define SYN  CTRL('V')
#define ETB  CTRL('W')
#define CAN  CTRL('X')
#define EM   CTRL('Y')
#define SUB  CTRL('Z')
#define ESC  CTRL('[')
#define FS   CTRL('\\')
#define GS   CTRL(']')
#define RS   CTRL('^')
#define US   CTRL('_')
#define DEL  (0177)

#define PARMASK  0200 /* Parity mask */
#define PARISEVN 0000 /* Even parity */
#define PARISODD 0200 /* Odd parity */

#define DELAYMASK 0177 /* Delay mask */

/* Parity lookup */
static const unsigned char partab[] = {

  /* regular ascii */
  0001, 0201, 0201, 0001, 0201, 0001, 0001, 0201, /* nul - bel */
  0202, 0004, 0003, 0201, 0005, 0206, 0201, 0001, /*  bs - si  */
  0201, 0001, 0001, 0201, 0001, 0201, 0201, 0001, /* dle - etb */
  0001, 0201, 0201, 0001, 0201, 0001, 0001, 0201, /* can - us  */
  0200, 0000, 0000, 0200, 0000, 0200, 0200, 0000, /* ' ' - ''' */
  0000, 0200, 0200, 0000, 0200, 0000, 0000, 0200, /* '(' - '/' */
  0000, 0200, 0200, 0000, 0200, 0000, 0000, 0200, /* '0' - '7' */
  0200, 0000, 0000, 0200, 0000, 0200, 0200, 0000, /* '8' - '?' */
  0200, 0000, 0000, 0200, 0000, 0200, 0200, 0000, /* '@' - 'G' */
  0000, 0200, 0200, 0000, 0200, 0000, 0000, 0200, /* 'H' - 'O' */
  0000, 0200, 0200, 0000, 0200, 0000, 0000, 0200, /* 'P' - 'W' */
  0200, 0000, 0000, 0200, 0000, 0200, 0200, 0000, /* 'X' - '_' */
  0000, 0200, 0200, 0000, 0200, 0000, 0000, 0200, /* '`' - 'g' */
  0200, 0000, 0000, 0200, 0000, 0200, 0200, 0000, /* 'h' - 'o' */
  0200, 0000, 0000, 0200, 0000, 0200, 0200, 0000, /* 'p' - 'w' */
  0000, 0200, 0200, 0000, 0200, 0000, 0000, 0201, /* 'x' - del */

  /* 128+ graphical character set (ignore, but pass through) */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007, /*  */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007, /*  */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007, /*  */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007, /*  */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007, /*  */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007, /*  */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007, /*  */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007, /*  */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007, /*  */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007, /*  */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007, /*  */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007, /*  */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007, /*  */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007, /*  */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007, /*  */
  0007, 0007, 0007, 0007, 0007, 0007, 0007, 0007  /*  */
};

/* mask, bit to set/clear to swap cases (| = upper &~ == lower) */
#define CASEMASK 0040 /* ONLY USE ON ALPHAS */

/*
 * N.B. Inputs in lcase mode are converted from uppercase to
 * lower before being stuck through this table, which is why
 * it appears to change lower cases to upper cases on escaping
 * uppercases...
 *
 * Input mapping table - if an entry is non-zero, when the
 * corresponding character is typed preceded by "\" the escaped
 * sequence is replaced by the contents at that character as index.
 * Mostly used for upper-case only terminals.
 *
 * e.g. typing \A -> !lcase makes it \a -> imabtab changes it to A
 *      typing A -> !lcase makes it a -> imaptab does nothing (no escape)
 *      typing a -> lcase does nothing
 */
static const unsigned char imaptab[] = {
  /* regular ascii */
  000, 000, 000, 000, 000, 000, 000, 000, /* nul - bel */
  000, 000, 000, 000, 000, 000, 000, 000, /*  bs - si  */
  000, 000, 000, 000, 000, 000, 000, 000, /* dle - etb */
  000, 000, 000, 000, 000, 000, 000, 000, /* can - us  */
  000, '|', 000, 000, 000, 000, 000, '`', /* ' ' - ''' */
  '{', '}', 000, 000, 000, 000, 000, 000, /* '(' - '/' */
  000, 000, 000, 000, 000, 000, 000, 000, /* '0' - '7' */
  000, 000, 000, 000, 000, 000, 000, 000, /* '8' - '?' */
  000, 000, 000, 000, 000, 000, 000, 000, /* '@' - 'G' */
  000, 000, 000, 000, 000, 000, 000, 000, /* 'H' - 'O' */
  000, 000, 000, 000, 000, 000, 000, 000, /* 'P' - 'W' */
  000, 000, 000, 000, 000, 000, '~', 000, /* 'X' - '_' */
  // below here only processed when lcase flag is unset
  000, 'A', 'B', 'C', 'D', 'E', 'F', 'G', /* '`' - 'g' */
  'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', /* 'h' - 'o' */
  'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', /* 'p' - 'w' */
  'X', 'Y', 'Z', 000, 000, 000, 000, 000, /* 'x' - del */
};

/*
 * Output mapping table - if an entry is non-zero, when the
 * corresponding character by index is to be printed, the character
 * is replaced by the contents at that character preceded by a '\'
 * Mostly used for upper-case only terminals.
 * 
 * e.g. sending an A -> table changes it to \A -> !lcase changes it to \A
 *      sending an a -> table does nothing -> !lcase changes it to A
 */
static const unsigned char omaptab[] = {
  /* regular ascii */
  000, 000, 000, 000, 000, 000, 000, 000, /* nul - bel */
  000, 000, 000, 000, 000, 000, 000, 000, /*  bs - si  */
  000, 000, 000, 000, 000, 000, 000, 000, /* dle - etb */
  000, 000, 000, 000, 000, 000, 000, 000, /* can - us  */
  000, '|', 000, 000, 000, 000, 000, '`', /* ' ' - ''' */
  '{', '}', 000, 000, 000, 000, 000, 000, /* '(' - '/' */
  000, 000, 000, 000, 000, 000, 000, 000, /* '0' - '7' */
  000, 000, 000, 000, 000, 000, 000, 000, /* '8' - '?' */
  000, 'A', 'B', 'C', 'D', 'E', 'F', 'G', /* '@' - 'G' */
  'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', /* 'H' - 'O' */
  'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', /* 'P' - 'W' */
  'X', 'Y', 'Z', 000, 000, 000, 000, 000, /* 'X' - '_' */
  000, 000, 000, 000, 000, 000, 000, 000, /* '`' - 'g' */
  000, 000, 000, 000, 000, 000, 000, 000, /* 'h' - 'o' */
  000, 000, 000, 000, 000, 000, 000, 000, /* 'p' - 'w' */
  000, 000, 000, 000, 000, 000, '~', 000, /* 'x' - del */
};

#endif
