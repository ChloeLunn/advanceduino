# AdvanceDuino

***Important*** This is 99% just me playing with parts of my kernel development before I add them, so the examples are a bit weird.

More advanced Arduino examples than your average library; How to write an OS in 10 steps

This repo contains some example Arduino "sketches" for various boards that show how to do some more interesting things than the default IDE shows you.

Each sketch is a self-contained lesson on a particular aspect of operating system development, but as you go through them some lessons are incorporated together into a larger project.

The idea is that by the end of 10 lessons, you'll have a basic understanding of some OS principals, as well as a small toy OS that could form the base of an RTOS project

## Targets, boards, and hardware

To keep portability, examples are built around standard Arduino libraries so that you don't need to write any low-level drivers

HOWEVER, only ARM Cortex-M boards, specifically SAMD21 and SAMD51 boards, have been used in development and testing and many parts of the software explicitly expect these chips.

Some lessons only apply to the SAMD51 due to features not found on the SAMD21, however compile-time flags mean that these are optional on those boards.

Most development was tested on a SAMD51 Adafruit Grand Central M4, and a custom SAMD21 board with an Arduino compatible bootloader.

## Setup

### You will need:

1. A compatible board - I suggest the Adafruit Grand Central M4 for best compatibility

2. The Arduino IDE - I use VSCode with the Arduino extension, but these should work with the standard IDE

3. An installation of arm-none-eabi-gcc for the external apps

### arm-none-eabi-gcc can be installed on:

#### Mac:

1. Homebrew and the arm-embedded package [name to be checked]
2. Macports: `sudo port install arm-none-eabi-gcc`

#### Linux:

Usually by installing a package named gcc-arm-none-eabi

#### Windows

I suggest using WSL if on windows and then installing the linux package

#### BSD: 

?

## License

The complete example files are under MIT License, but feel free to use snippets without worrying about license or accreditation as they're very basic building blocks

For brevity, the license is not included inside source code files. However, all files in this repo are covered under the [LICENSE](LICENSE) file unless otherwise stated

## Lessons:

1. System Time - Tap into the system tick interrupt and form a simple time-keeper

2. Multitasking - Learn how to implement simple context-switching coop multitasking

3. System Calls - Write a system call handler and caller functions

4. External Apps - Run an application off an SD card, uses lesson 3 as a base

5. TTY Control - Wrap your serial port in better TTY handling and issue signals

6. Memory Protection [SAMD51 ONLY] - Learn how to protect regions of memory and set their permissions

7. Preemptive Multitasking - Learn how to use the special PendSV interrupt to do foreground and background tasks

8. Filesystems and Pipes - Add a simple per-process file handling and add simple pipes

9. Command Line - Learn how to break down TTY input to execute simple commands

10. Your-own-DOS - Combine lessons 1-9 to make a simple threaded DOS system


### Completed Lessons

Only 2. 3. and 4.

### WIP Lessions

Num 1. and 5.

### To be converted into a lesson

7, 8, 9, 10
