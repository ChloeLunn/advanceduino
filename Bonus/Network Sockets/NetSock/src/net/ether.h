/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

#if !H_ETHER
#define H_ETHER 1

#ifndef __packed
#define __packed __attribute__((__packed__))
#endif

#ifndef __aligned
#define __aligned(x) __attribute__((__aligned__(x)))
#endif

/* ------------------------------------------------------------------------- */

#define ETH_ALEN      6    /* Octets in one ethernet addr */
#define ETH_HLEN      14   /* Total octets in header  */
#define ETH_DATA_LEN  1500 /* Max. octets in frame exc header */
#define ETH_FRAME_LEN 1514 /* Max. octets in frame inc header */
#define ETH_DATA_MIN  68   /* Min. octets in frame */

#define ETH_P_INTERNAL_ANY 0xFFFF
#define ETH_P_ALL          0x0003 /* All ethernet types */

#define ETH_P_LOOP 0x0060 /* Ethernet Loopback packet */
#define ETH_P_IP   0x0800 /* Internet Protocol packet */
#define ETH_P_ARP  0x0806 /* Address Resolution packet */
#define ETH_P_IP6  0x86DD /* Internet Protocol v6 packet */

#define ETH_P_PUP         0x0200 /* PUP protocol */
#define ETH_P_REVARP      0x8035 /* reverse Addr. resolution protocol */
#define ETH_P_VLAN        0x8100 /* IEEE 802.1Q VLAN tagging */
#define ETH_P_PAE         0x888e /* EAPOL PAE/802.1x */
#define ETH_P_RSN_PREAUTH 0x88c7 /* 802.11i / RSN Pre-Authentication */
#define ETH_P_PTP         0x88f7 /* IEEE 1588 Precision Time Protocol */

/* ------------------------------------------------------------------------- */

struct ether_header {
    unsigned char h_dest[ETH_ALEN];   /* destination eth addr */
    unsigned char h_source[ETH_ALEN]; /* source ether addr */
    unsigned short h_proto;           /* packet type */
} __packed;

struct mac_addr {
    unsigned char m_addr[ETH_ALEN];
} __packed;

/* ------------------------------------------------------------------------- */

#endif
