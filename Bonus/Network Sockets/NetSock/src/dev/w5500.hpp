/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

/*

    Low level raw wiznet w5500 handling

*/

#pragma once
#if !H_RAW_WIZ
#define H_RAW_WIZ 1

#include <Arduino.h>

/* Command bits */
#define ACC_READ  (0x00 << 2)
#define ACC_WRITE (0x01 << 2)

#define CREG_PTR (0x00 << 3)
#define SREG_PTR (0x01 << 3)
#define TX_PTR   (0x02 << 3)
#define RX_PTR   (0x03 << 3)

/* Main Phy Registers */
#define REG_MR       0x0000 /* Master config register */
#define REG_GAR      0x0001 /* Gateway IP Address */
#define REG_SUBR     0x0005 /* Subnet Mask Address */
#define REG_SHAR     0x0009 /* Hardware (MAC) Address */
#define REG_SIPR     0x000F /* Source IP Address */
#define REG_INTLEVEL 0x0013 /* Interrupt level */
#define REG_IR       0x0015 /* Interrupt */
#define REG_IMR      0x0016 /* Interrupt mask */
#define REG_SIR      0x0017 /* Socket interrupt */
#define REG_SIMR     0x0018 /* Socket interrupt mask */
#define REG_RTR      0x0019 /* Timeout */
#define REG_RCR      0x001B /* Retry count -- check it's not 0x0019 */
#define REG_PATR     0x001C /* PPPoE Authen type */
#define REG_VER52    0x001F /* W5200 version reg */
#define REG_PTIMER   0x0028 /* PPP LCP Request Timer */
#define REG_PMAGIC   0x0029 /* PPP LCP Magic Number */
#define REG_PHYCFGR  0x002E /* Phy config register: dflt = 10111xxx */
#define REG_VER55    0x0039 /* w5500 version reg */

/* MR register bits */
#define MR_NONE  0x00
#define MR_RST   0x80 /* Soft reset */
#define MR_WOL   0x20
#define MR_PB    0x10
#define MR_PPPOE 0x08 /* Enable PPPoE */
#define MR_FARP  0x02 /* Force ARP every packet */

/* Phy status/config bits */
#define PHYCFGR_LNK 0x01

/* values returned by getLNK */
#define LNK_UNKOWN (-1)
#define LNK_UP     (1)
#define LNK_DWN    (0)

/* Socket config registers */
#define Sn_MR         0x0000 /* Socket master config */
#define Sn_CR         0x0001 /* Command */
#define Sn_IR         0x0002 /* Interrupt */
#define Sn_SR         0x0003 /* Status */
#define Sn_PORT       0x0004 /* Source Port */
#define Sn_DHAR       0x0006 /* Destination Hardw Addr */
#define Sn_DIPR       0x000C /* Destination IP Addr */
#define Sn_DPORT      0x0010 /* Destination Port */
#define Sn_MSSR       0x0012 /* Max Segment Size */
#define Sn_PROTO      0x0014 /* Protocol in IP RAW Mode */
#define Sn_TOS        0x0015 /* IP TOS */
#define Sn_TTL        0x0016 /* IP TTL */
#define Sn_RXBUF_SIZE 0x001E /* RX Memory Size (W5200/W5500 only) */
#define Sn_TXBUF_SIZE 0x001F /* RX Memory Size (W5200/W5500 only) */
#define Sn_TX_FSR     0x0020 /* TX Free Size */
#define Sn_TX_RD      0x0022 /* TX Read Pointer */
#define Sn_TX_WR      0x0024 /* TX Write Pointer */
#define Sn_RX_RSR     0x0026 /* RX Free Size */
#define Sn_RX_RD      0x0028 /* RX Read Pointer */
#define Sn_RX_WR      0x002A /* RX Write Pointer (supported?) */
#define Sn_IMR        0x002C /* IR Mask */
#define Sn_FRAG       0x002D /*  */
#define Sn_KPALVTR    0x002F /*  */

/* Socket main register bits */
#define Sn_MR_CLOSE  0x00
#define Sn_MR_TCP    0x21
#define Sn_MR_UDP    0x02
#define Sn_MR_IPRAW  0x03
#define Sn_MR_MACRAW 0x04
#define Sn_MR_PPPOE  0x05
#define Sn_MR_ND     (1 << 5) /* No delay */
#define Sn_MR_MF     (1 << 6) /* HW Mac filter */
#define Sn_MR_MULTI  (1 << 7) /* Multicasting */
#define Sn_MR_ALIGN  (1 << 8) /* Alignment */

/* Socket command register bits */
#define Sn_CR_OPEN      0x01
#define Sn_CR_LISTEN    0x02
#define Sn_CR_CONNECT   0x04
#define Sn_CR_DISCON    0x08
#define Sn_CR_CLOSE     0x10
#define Sn_CR_SEND      0x20
#define Sn_CR_SEND_MAC  0x21
#define Sn_CR_SEND_KEEP 0x22
#define Sn_CR_RECV      0x40

/* Socket interrupt register bits */
#define Sn_IR_SEND_OK 0x10
#define Sn_IR_TIMEOUT 0x08
#define Sn_IR_RECV    0x04
#define Sn_IR_DISCON  0x02
#define Sn_IR_CON     0x01

/* Socket status register bits */
#define Sn_SR_CLOSED      0x00
#define Sn_SR_INIT        0x13
#define Sn_SR_LISTEN      0x14
#define Sn_SR_SYNSENT     0x15
#define Sn_SR_SYNRECV     0x16
#define Sn_SR_ESTABLISHED 0x17
#define Sn_SR_FIN_WAIT    0x18
#define Sn_SR_CLOSING     0x1A
#define Sn_SR_TIME_WAIT   0x1B
#define Sn_SR_CLOSE_WAIT  0x1C
#define Sn_SR_LAST_ACK    0x1D
#define Sn_SR_UDP         0x22
#define Sn_SR_IPRAW       0x32
#define Sn_SR_MACRAW      0x42
#define Sn_SR_PPPOE       0x5F

#define MAC_RAW_MTU 1514 /* ALWAYS */

class wz_driver
{
  public:
    /* Initialise the network phy (returns 0 for error) */
    static uint8_t init(int ss);

    /* get the LNK bit status (link unknown/on/off) */
    static int getLNK();

    /* GENERAL REGISTER R/W */

    static void write8(uint16_t addr, uint8_t data);
    static uint8_t read8(uint16_t addr);
    static void write16(uint16_t addr, uint16_t data);
    static uint16_t read16(uint16_t addr);
    static uint16_t writeN(uint16_t _addr, uint8_t _cb, const uint8_t* _buf, uint16_t _len);
    static uint16_t readN(uint16_t _addr, uint8_t _cb, uint8_t* _buf, uint16_t _len);
    static uint16_t write(uint16_t addr, const uint8_t* buf, uint16_t len);
    static uint8_t write(uint16_t addr, uint8_t data)
    {
        return write(addr, &data, 1);
    }
    static uint16_t read(uint16_t addr, uint8_t* buf, uint16_t len);
    static uint8_t read(uint16_t addr)
    {
        uint8_t data;
        read(addr, &data, 1);
        return data;
    }

    // Mode
    static inline void writeMR(uint8_t _data)
    {
        write8(REG_MR, _data);
    }
    static inline uint8_t readMR()
    {
        return read8(REG_MR);
    }

    // Gateway IP address
    static inline uint16_t writeGAR(const uint8_t* buff)
    {
        return write(REG_GAR, buff, 4);
    }
    static inline uint16_t readGAR(uint8_t* buff)
    {
        return read(REG_GAR, buff, 4);
    }

    // Subnet mask address
    static inline uint16_t writeSUBR(const uint8_t* buff)
    {
        return write(REG_SUBR, buff, 4);
    }
    static inline uint16_t readSUBR(uint8_t* buff)
    {
        return read(REG_SUBR, buff, 4);
    }

    // Source MAC address
    static inline uint16_t writeSHAR(const uint8_t* buff)
    {
        return write(REG_SHAR, buff, 6);
    }
    static inline uint16_t readSHAR(uint8_t* buff)
    {
        return read(REG_SHAR, buff, 6);
    }

    // Source IP address
    static inline uint16_t writeSIPR(const uint8_t* buff)
    {
        return write(REG_SIPR, buff, 4);
    }
    static inline uint16_t readSIPR(uint8_t* buff)
    {
        return read(REG_SIPR, buff, 4);
    }

    // Interrupt
    static inline void writeIR(uint8_t data)
    {
        write8(REG_IR, data);
    }
    static inline uint8_t readIR()
    {
        return read8(REG_IR);
    }

    // Interrupt Mask
    static inline void writeIMR(uint8_t data)
    {
        write8(REG_IMR, data);
    }
    static inline uint8_t readIMR()
    {
        return read8(REG_IMR);
    }

    // Timeout (W5500)
    static inline void writeRTR(uint16_t data)
    {
        write16(REG_RTR, data);
    }
    static inline uint16_t readRTR()
    {
        return read16(REG_RTR);
    }

    // Socket Interrupt (W5500)
    static inline void writeSIR(uint8_t data)
    {
        write8(REG_SIR, data);
    }
    static inline uint8_t readSIR()
    {
        return read8(REG_SIR);
    }

    // Socket Interrupt Mask (W5500)
    static inline void writeSIMR(uint8_t data)
    {
        write8(REG_SIMR, data);
    }
    static inline uint8_t readSIMR()
    {
        return read8(REG_SIMR);
    }

    // Retry count
    static inline void writeRCR(uint8_t data)
    {
        write8(REG_RCR, data);
    }
    static inline uint8_t readRCR()
    {
        return read8(REG_RCR);
    }

    // Authentication type address in PPPoE mode
    static inline void writePATR(uint8_t data)
    {
        write8(REG_PATR, data);
    }
    static inline uint8_t readPATR()
    {
        return read8(REG_PATR);
    }

    // PPP LCP Request Timer
    static inline void writePTIMER(uint8_t data)
    {
        write8(REG_PTIMER, data);
    }
    static inline uint8_t readPTIMER()
    {
        return read8(REG_PTIMER);
    }

    // PPP LCP Magic Number
    static inline void writePMAGIC(uint8_t data)
    {
        write8(REG_PMAGIC, data);
    }
    static inline uint8_t readPMAGIC()
    {
        return read8(REG_PMAGIC);
    }

    // Chip Version Register (W5200 only)
    static inline void writeVERSIONR_W5200(uint8_t data)
    {
        write8(REG_VER52, data);
    }
    static inline uint8_t readVERSIONR_W5200()
    {
        return read8(REG_VER52);
    }

    // Chip Version Register (W5500 only)
    static inline void writeVERSIONR_W5500(uint8_t data)
    {
        write8(REG_VER55, data);
    }
    static inline uint8_t readVERSIONR_W5500()
    {
        return read8(REG_VER55);
    }

    // PHY Configuration register, default: 10111xxx
    static inline void writePHYCFGR_W5500(uint8_t data)
    {
        write8(REG_PHYCFGR, data);
    }
    static inline uint8_t readPHYCFGR_W5500()
    {
        return read8(REG_PHYCFGR);
    }

    /* SOCKET REGISTERS R/W */

    /* execute a command on a socket */
    static void execSnCR(int s, uint8_t _cmd);

    /* read a socket's Rx Buffer */
    static void readSn_RxBuf(int s, uint8_t* data, uint16_t len, uint8_t peek = 0);

    /* write a socket's Tx Buffer */
    static void writeSn_TxBuf(int s, const uint8_t* data, uint16_t len, uint16_t data_offset = 0);

    /* Get the free space in a socket's Tx buffer */
    static uint16_t getSnTX_FSR(int s);

    /* Get the used space in a socket's Rx buffer */
    static uint16_t getSnRX_RSR(int s);

    static inline uint8_t readSn8(int s, uint16_t addr)
    {
        return read(CH_BASE + (s * CH_SIZE) + addr);
    }
    static inline uint8_t writeSn8(int s, uint16_t addr, uint8_t data)
    {
        return write(CH_BASE + (s * CH_SIZE) + addr, data);
    }
    static inline uint16_t readSnN(int s, uint16_t addr, uint8_t* buf, uint16_t len)
    {
        return read(CH_BASE + (s * CH_SIZE) + addr, buf, len);
    }
    static inline uint16_t writeSnN(int s, uint16_t addr, uint8_t* buf, uint16_t len)
    {
        return write(CH_BASE + (s * CH_SIZE) + addr, buf, len);
    }
    static inline void writeSn16(int s, uint16_t addr, uint16_t _data)
    {
        uint8_t buf[2];
        buf[0] = _data >> 8;
        buf[1] = _data & 0xFF;
        writeSnN(s, addr, buf, 2);
    }
    static inline uint16_t readSn16(int s, uint16_t addr)
    {
        uint8_t buf[2];
        readSnN(s, addr, buf, 2);
        return (buf[0] << 8) | buf[1];
    }

#define __SOCKET_REGISTER8(name, address)                \
    static inline void write##name(int s, uint8_t _data) \
    {                                                    \
        writeSn8(s, address, _data);                     \
    }                                                    \
    static inline uint8_t read##name(int s)              \
    {                                                    \
        return readSn8(s, address);                      \
    }
#define __SOCKET_REGISTER16(name, address)                \
    static inline void write##name(int s, uint16_t _data) \
    {                                                     \
        writeSn16(s, address, _data);                     \
    }                                                     \
    static inline uint16_t read##name(int s)              \
    {                                                     \
        return readSn16(s, address);                      \
    }
#define __SOCKET_REGISTER_N(name, address, size)             \
    static inline uint16_t write##name(int s, uint8_t* buff) \
    {                                                        \
        return writeSnN(s, address, buff, size);             \
    }                                                        \
    static inline uint16_t read##name(int s, uint8_t* buff)  \
    {                                                        \
        return readSnN(s, address, buff, size);              \
    }

    static inline void writeSnMR(int s, uint8_t _data)
    {
        writeSn8(s, Sn_MR, _data);
    }
    static inline uint8_t readSnMR(int s)
    {
        return readSn8(s, Sn_MR);
    }

    // Command
    static inline void writeSnCR(int s, uint8_t _data)
    {
        writeSn8(s, Sn_CR, _data);
    }
    static inline uint8_t readSnCR(int s)
    {
        return readSn8(s, Sn_CR);
    }

    // Interrupt
    static inline void writeSnIR(int s, uint8_t _data)
    {
        writeSn8(s, Sn_IR, _data);
    }
    static inline uint8_t readSnIR(int s)
    {
        return readSn8(s, Sn_IR);
    }

    // Status
    static inline void writeSnSR(int s, uint8_t _data)
    {
        writeSn8(s, Sn_SR, _data);
    }
    static inline uint8_t readSnSR(int s)
    {
        return readSn8(s, Sn_SR);
    }

    // Source Port
    static inline void writeSnPORT(int s, uint16_t _data)
    {
        writeSn16(s, Sn_PORT, _data);
    }
    static inline uint16_t readSnPORT(int s)
    {
        return readSn16(s, Sn_PORT);
    }

    // Destination Hardw Addr
    static inline uint16_t writeSnDHAR(int s, uint8_t* buff)
    {
        return writeSnN(s, Sn_DHAR, buff, 6);
    }
    static inline uint16_t readSnDHAR(int s, uint8_t* buff)
    {
        return readSnN(s, Sn_DHAR, buff, 6);
    }

    // Destination IP Addr
    static inline uint16_t writeSnDIPR(int s, uint8_t* buff)
    {
        return writeSnN(s, Sn_DIPR, buff, 4);
    }
    static inline uint16_t readSnDIPR(int s, uint8_t* buff)
    {
        return readSnN(s, Sn_DIPR, buff, 4);
    }

    // Destination Port
    static inline void writeSnDPORT(int s, uint16_t _data)
    {
        writeSn16(s, Sn_DPORT, _data);
    }
    static inline uint16_t readSnDPORT(int s)
    {
        return readSn16(s, Sn_DPORT);
    }

    // Max Segment Size
    static inline void writeSnMSSR(int s, uint16_t _data)
    {
        writeSn16(s, Sn_MSSR, _data);
    }
    static inline uint16_t readSnMSSR(int s)
    {
        return readSn16(s, Sn_MSSR);
    }

    // Protocol in IP RAW Mode
    static inline void writeSnPROTO(int s, uint8_t _data)
    {
        writeSn8(s, Sn_PROTO, _data);
    }
    static inline uint8_t readSnPROTO(int s)
    {
        return readSn8(s, Sn_PROTO);
    }
    __SOCKET_REGISTER8(SnTOS, Sn_TOS)             // IP TOS
    __SOCKET_REGISTER8(SnTTL, Sn_TTL)             // IP TTL
    __SOCKET_REGISTER8(SnRX_SIZE, Sn_RXBUF_SIZE)  // RX Memory Size (W5200/W5500 only)
    __SOCKET_REGISTER8(SnTX_SIZE, Sn_TXBUF_SIZE)  // RX Memory Size (W5200/W5500 only)
    __SOCKET_REGISTER16(SnTX_FSR, Sn_TX_FSR)      // TX Free Size
    __SOCKET_REGISTER16(SnTX_RD, Sn_TX_RD)        // TX Read Pointer
    __SOCKET_REGISTER16(SnTX_WR, Sn_TX_WR)        // TX Write Pointer
    __SOCKET_REGISTER16(SnRX_RSR, Sn_RX_RSR)      // RX Free Size
    __SOCKET_REGISTER16(SnRX_RD, Sn_RX_RD)        // RX Read Pointer
    __SOCKET_REGISTER16(SnRX_WR, Sn_RX_WR)        // RX Write Pointer (supported? yes)
    __SOCKET_REGISTER8(SnIMR, Sn_IMR)             // IR mask

    static uint8_t softReset(void);

    static const uint16_t SSIZE = 16384;
    static const uint16_t SMASK = SSIZE - 1;

    /* force the Rx buffer pointer for a socket to update so it clears the used space */
    inline void flushRxBuf(int s, uint16_t len)
    {
        uint16_t ptr;
        ptr = readSnRX_RD(s);
        ptr += len;
        writeSnRX_RD(s, ptr);
    }

    /* force the Tx buffer pointer for a socket to update so it sends out the used space */
    inline void flushTxBuf(int s, uint16_t len)
    {
        uint16_t ptr;
        ptr = readSnTX_RD(s);
        ptr += len;
        writeSnTX_RD(s, ptr);
    }

  private:
    /* socket addr offset info */
    static const uint16_t CH_BASE = 0x1000;
    static const uint16_t CH_SIZE = 0x0100;

    /* init routines */
    static uint8_t m_init(void);
    static uint8_t initW5500(void);
    static bool initialised;
    static uint8_t chip;

    /* chip select */
    static uint8_t ss_pin;
#if defined(__SAMD21G18A__) || defined(__SAMD51__)
    static volatile uint32_t* ss_pin_reg;
    static uint32_t ss_pin_mask;
    inline static void initSS()
    {
        ss_pin_reg = portModeRegister(digitalPinToPort(ss_pin));
        ss_pin_mask = digitalPinToBitMask(ss_pin);
        pinMode(ss_pin, OUTPUT);
    }
    inline static void setSS()
    {
        *(ss_pin_reg + 5) = ss_pin_mask;
    }
    inline static void resetSS()
    {
        *(ss_pin_reg + 6) = ss_pin_mask;
    }
#else
    inline static void initSS()
    {
        pinMode(ss_pin, OUTPUT);
    }
    inline static void setSS()
    {
        digitalWrite(ss_pin, LOW);
    }
    inline static void resetSS()
    {
        digitalWrite(ss_pin, HIGH);
    }
#endif
};

extern wz_driver WZ;

#endif
