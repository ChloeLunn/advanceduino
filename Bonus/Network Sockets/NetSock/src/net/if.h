/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

#if !H_IF
#define H_IF 1

#include "ether.h"
#include "ip.h"
#include "socket.h"

#ifndef __packed
#define __packed __attribute__((__packed__))
#endif

#ifndef __aligned
#define __aligned(x) __attribute__((__aligned__(x)))
#endif

/* ------------------------------------------------------------------------- */

struct sockaddr_ll {
    char sll_family;             /* PF_PACKET (assuming Ethernet) */
    unsigned short sll_protocol; /* ETH_P_XXX protocol */
    unsigned char sll_ifindex;   /* IF index (0 = ANY) */
    unsigned short sll_hatype;   /* ARP hardware type */
    unsigned char sll_pkttype;   /* packet type */
    unsigned char sll_halen;     /* Address length */
    unsigned char sll_addr[8];   /* Address */
} __packed;

#define PACKET_HOST      1 /* Packet addressed to this host */
#define PACKET_BROADCAST 2 /* Packet is a broadcast */
#define PACKET_MULTICAST 3 /* Packet is a multicast */
#define PACKET_OTHERHOST 4 /* Packet is not for this host, caught by promiscious */
#define PACKET_OUTGOING  5 /* Packet caught by loopback */

/* ------------------------------------------------------------------------- */

#define IFNAMSIZ    10
#define IFHWADDRLEN 6

#define IFF_UP          0x0001 /* Interface is up  (userspace marked it useable) */
#define IFF_BROADCAST   0x0002 /* Broadcast address valid */
#define IFF_DEBUG       0x0004 /* Turn on debugging */
#define IFF_LOOPBACK    0x0008 /* Is a loopback net */
#define IFF_POINTOPOINT 0x0010 /* Interface is point-to-point link */
#define IFF_NOTRAILERS  0x0020 /* Avoid use of trailers */
#define IFF_RUNNING     0x0040 /* Resources allocated */
#define IFF_NOARP       0x0080 /* No address resolution protocol */
#define IFF_PROMISC     0x0100 /* Receive all packets */
#define IFF_ALLMULTI    0x0200 /* Receive all multicast packets */
#define IFF_MASTER      0x0400 /* Master of a load balancer */
#define IFF_SLAVE       0x0800 /* Slave of a load balancer */
#define IFF_MULTICAST   0x1000 /* Supports multicast */
#define IFF_PORTSEL     0x2000 /* Can set media type */
#define IFF_AUTOMEDIA   0x4000 /* Auto media select active */
#define IFF_DYNAMIC     0x8000 /* Dialup device with changing addresses */

/* ------------------------------------------------------------------------- */

struct ifhdr {
    unsigned short h_len;
} __packed;

struct iframe {
    struct ifhdr ifhead;  /* interface header (length in network order) */
    unsigned char buf[1]; /* packet buffer */
} __packed;

struct ethframe {
    struct ifhdr ifhead;       /* interface header (length in network order) */
    struct ether_header ehead; /* ethernet header */
    unsigned char buf[1];      /* packet buffer */
} __packed;

struct ipframe {
    struct ifhdr ifhead;       /* interface header (length in network order) */
    struct ether_header ehead; /* ethernet header */
    union {
        struct {
            struct ip iphead;     /* ip header */
            unsigned char buf[1]; /* packet buffer */
        };
        struct {
            struct ip6 ip6head;    /* ipv6 header */
            unsigned char buf6[1]; /* ipv6 packet buffer */
        };
    };
} __packed;

struct ip6frame {
    struct ifhdr ifhead;       /* interface header (length in network order) */
    struct ether_header ehead; /* ethernet header */
    struct ip6 ip6head;        /* ipv6 header */
    unsigned char buf[1];      /* ipv6 packet buffer */
} __packed;

/* ------------------------------------------------------------------------- */

struct ifmap {
    unsigned int mem_start;
    unsigned int mem_end;
    unsigned short base_addr;
    unsigned char irq;
    unsigned char dma;
    unsigned char port;
} __packed;

struct ifaddr {
    struct sockaddr ifa_addr;
    union {
        struct sockaddr ifu_broadaddr;
        struct sockaddr ifu_dstaddr;
    } __packed;
    struct ifaddr* ifa_next;
} __packed;

struct ifreq {
    char ifr_name[IFNAMSIZ];
    union {
        struct sockaddr ifr_addr;
        struct sockaddr ifr_gateaddr;
        struct sockaddr ifr_broadaddr;
        struct sockaddr ifr_netmask;
        struct sockaddr ifr_hwaddr;
        short ifr_flags;
        int ifr_ifindex;
        int ifr_metric;
        int ifr_mtu;
        struct ifmap ifr_map;
        char ifr_slave[IFNAMSIZ];
        char ifr_newname[IFNAMSIZ];
        unsigned char* ifr_data;
    } __packed;
} __packed;

struct ifconf {
    int ifc_len;
    union {
        unsigned char* ifcu_buf;
        struct ifreq* ifcu_req;
    } __packed;
} __packed;

/* ------------------------------------------------------------------------- */

#define IFS_WOPEN  (0000002) /* Waiting for open to complete */
#define IFS_ISOPEN (0000004) /* Device is open */
#define IFS_RXWAIT (0000010) /* Data waiting on receive buffer */
#define IFS_TXWAIT (0000020) /* Data waiting on transmit buffer */
#define IFS_BUSY   (0000040) /* Device busy */
#define IFS_LINKUP (0000100) /* Device is linked up */

#define IFT_ETHER (0000001)
#define IFT_WLAN  (0000002)

#define IF_IDX_ANY 0 /* pseudo index for the 'any' interface */

/* iface in kernel */
struct iface {
    /* Interface ID information */
    unsigned short ifr_dev;
    unsigned short ifr_ifindex;
    char ifr_name[IFNAMSIZ];

    union {
        struct mac_addr ifr_hwaddr;
        unsigned char ifr_linkaddr[IFHWADDRLEN];
    } __packed;

    /* IP Layer addressing/routing (may not apply to all interface types) */
    struct in_addr ifr_addr;
    struct in_addr ifr_gateaddr;
    struct in_addr ifr_broadaddr;
    struct in_addr ifr_netmask;
    struct in_addr ifr_dnsaddr;

    /* Routing & control variables */
    unsigned short ifr_status; /* status flags for driver level */
    unsigned short ifr_flags;  /* settings flags for common code and higher levels */
    unsigned short ifr_metric;
    unsigned short ifr_mtu;
    unsigned short ifr_type;

    /* Last frame received data buffer */
    struct iframe* ifr_data;
}; /* NOT packed... needs alignment on data */

/* ------------------------------------------------------------------------- */

#endif
