/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

/*

    Low level raw wiznet w5500 handling

*/

#include <Arduino.h>
#include <SPI.h>

#define SPI_ETHERNET_SETTINGS SPISettings(14000000, MSBFIRST, SPI_MODE0)

#include "../w5500.hpp"

wz_driver WZ;

bool wz_driver::initialised = false;
uint8_t wz_driver::chip = 0;
uint8_t wz_driver::ss_pin = 10;
#if defined(__SAMD21G18A__) || defined(__SAMD51__)
volatile uint32_t* wz_driver::ss_pin_reg;
uint32_t wz_driver::ss_pin_mask;
#endif

uint8_t wz_driver::init(int ss)
{
    ss_pin = ss;
    return m_init();
}

uint8_t wz_driver::m_init(void)
{
    if (initialised)
        return 1;

    delay(560);

    SPI.begin();
    initSS();
    resetSS();
    SPI.beginTransaction(SPI_ETHERNET_SETTINGS);

    if (initW5500())
    {
        writeSnRX_SIZE(0, SSIZE >> 10);
        writeSnTX_SIZE(0, SSIZE >> 10);
        for (int i = 1; i < 8; i++)
        {
            writeSnRX_SIZE(i, 0);
            writeSnTX_SIZE(i, 0);
        }
    }
    else
    {
        chip = 0;
        SPI.endTransaction();
        return 0;
    }

    SPI.endTransaction();
    initialised = true;
    return 1;
}

uint8_t wz_driver::initW5500(void)
{
    chip = 55;

    /* reset the chip just to make sure it's clean */
    if (!softReset())
        return 0;

    /* does chip support PPPoE? */
    writeMR(MR_PPPOE);
    if (readMR() != MR_PPPOE)
        return 0;

    /* does chip support ?? */
    writeMR(MR_PB);
    if (readMR() != MR_PB)
        return 0;

    /* Does chip support turning all options off? (reset without reset) */
    writeMR(MR_NONE);
    if (readMR() != MR_NONE)
        return 0;

    /* we detect the w5200 first because it can freak out if we carry on as a w5500 */
    int ver = readVERSIONR_W5200();
    if (ver == 3)
        return 0;

    ver = readVERSIONR_W5500();
    if (ver != 4)
        return 0;

    return 1;
}

// Soft reset the Wiznet chip, by writing to its MR register reset bit
uint8_t wz_driver::softReset(void)
{
    uint16_t count = 0;

    // write to reset bit
    writeMR(MR_RST);

    // then wait for soft reset to complete (should be <20ms)
    for (int i = 0; i < 20; i++)
    {
        uint8_t mr = readMR();

        if (mr == 0)
            return 1;

        delay(1); /* throttle it slightly */
    }

    return 0;
}

int wz_driver::getLNK()
{
    uint8_t phystatus;

    if (!m_init())
        return LNK_UNKOWN;

    switch (chip)
    {
    case 55:
        SPI.beginTransaction(SPI_ETHERNET_SETTINGS);
        phystatus = readPHYCFGR_W5500();
        SPI.endTransaction();
        if (phystatus & 0x01)
            return LNK_UP;
        return LNK_DWN;
    default:
        return LNK_UNKOWN;
    }
}

uint16_t wz_driver::getSnTX_FSR(int s)
{
    /*
        The WZ hardware can update the two halves of the register independently,
        even when the counts aren't being updated.
        The datasheet specs that you should read this value twice until both values
        match
    */
    uint16_t val = 0, val1 = 0;

    do
    {
        val1 = readSnTX_FSR(s);

        if (val1 != 0)
            val = readSnTX_FSR(s);

    } while (val != val1);

    return val;
}

uint16_t wz_driver::getSnRX_RSR(int s)
{
    /*
        The WZ hardware can update the two halves of the register independently,
        even when the counts aren't being updated.
        The datasheet specs that you should read this value twice until both values
        match
    */
    uint16_t val = 0, val1 = 0;

    do
    {
        val1 = readSnRX_RSR(s);

        if (val1 != 0)
            val = readSnRX_RSR(s);

    } while (val != val1);

    return val;
}

uint16_t wz_driver::write(uint16_t addr, const uint8_t* buf, uint16_t len)
{
    SPI.waitForTransfer();
    SPI.beginTransaction(SPI_ETHERNET_SETTINGS);
    uint8_t cmd[8];

    if (chip == 55)
    {
        setSS();
        if (addr < 0x100)
        {
            // common registers 00nn
            cmd[0] = 0;
            cmd[1] = addr & 0xFF;
            cmd[2] = ACC_WRITE | CREG_PTR;
        }
        else if (addr < 0x8000)
        {
            // socket registers  10nn, 11nn, 12nn, 13nn, etc
            cmd[0] = 0;
            cmd[1] = addr & 0xFF;
            cmd[2] = ((addr >> 3) & 0xE0) | ACC_WRITE | SREG_PTR;
        }
        else if (addr < 0xC000)
        {
            // transmit buffers  8000-87FF, 8800-8FFF, 9000-97FF, etc
            //  10## #nnn nnnn nnnn
            cmd[0] = addr >> 8;
            cmd[1] = addr & 0xFF;
            cmd[2] = ACC_WRITE | TX_PTR;  // 16K buffers
        }
        else
        {
            // receive buffers
            cmd[0] = addr >> 8;
            cmd[1] = addr & 0xFF;
            cmd[2] = ACC_WRITE | RX_PTR;  // 16K buffers
        }

        if (len <= 5)
        {
            for (uint8_t i = 0; i < len; i++)
            {
                cmd[i + 3] = buf[i];
            }
            SPI.transfer(cmd, len + 3);
        }
        else
        {
            SPI.transfer(cmd, 3);
            SPI.transfer(buf, NULL, len);
        }
        resetSS();
    }
    SPI.waitForTransfer();
    SPI.endTransaction();
    return len;
}

uint16_t wz_driver::read(uint16_t addr, uint8_t* buf, uint16_t len)
{
    uint8_t cmd[4];

    if (chip == 55)
    {
        setSS();
        if (addr < 0x100)
        {
            // common registers 00nn
            cmd[0] = 0;
            cmd[1] = addr & 0xFF;
            cmd[2] = CREG_PTR | ACC_READ;
        }
        else if (addr < 0x8000)
        {
            // socket registers  10nn, 11nn, 12nn, 13nn, etc
            cmd[0] = 0;
            cmd[1] = addr & 0xFF;
            cmd[2] = ((addr >> 3) & 0xE0) | SREG_PTR | ACC_READ;
        }
        else if (addr < 0xC000)
        {
            // transmit buffers  8000-87FF, 8800-8FFF, 9000-97FF, etc
            //  10## #nnn nnnn nnnn
            cmd[0] = addr >> 8;
            cmd[1] = addr & 0xFF;
            cmd[2] = TX_PTR | ACC_READ;  // 16K buffers
        }
        else
        {
            // receive buffers
            cmd[0] = addr >> 8;
            cmd[1] = addr & 0xFF;
            cmd[2] = RX_PTR | ACC_READ;  // 16K buffers
        }

        SPI.transfer(cmd, 3);

        memset(buf, 0, len);
        SPI.transfer(buf, len);

        resetSS();
    }
    return len;
}

void wz_driver::write8(uint16_t addr, uint8_t data)
{
    write(addr, data);
}

uint8_t wz_driver::read8(uint16_t addr)
{
    return read(addr);
}

void wz_driver::write16(uint16_t address, uint16_t data)
{
    uint8_t buf[2];
    buf[0] = data >> 8;
    buf[1] = data & 0xFF;
    write(address, buf, 2);
}

uint16_t wz_driver::read16(uint16_t address)
{
    uint8_t buf[2];
    read(address, buf, 2);
    return (buf[0] << 8) | buf[1];
}

uint16_t wz_driver::writeN(uint16_t _addr, uint8_t _cb, const uint8_t* _buf, uint16_t _len)
{
    setSS();
    SPI.transfer(_addr >> 8);
    SPI.transfer(_addr & 0xFF);
    SPI.transfer(_cb);
    SPI.transfer(_buf, NULL, _len);
    resetSS();

    return _len;
}

uint16_t wz_driver::readN(uint16_t _addr, uint8_t _cb, uint8_t* _buf, uint16_t _len)
{
    setSS();
    SPI.transfer(_addr >> 8);
    SPI.transfer(_addr & 0xFF);
    SPI.transfer(_cb);
    SPI.transfer(NULL, _buf, _len);
    resetSS();

    return _len;
}

void wz_driver::writeSn_TxBuf(int s, const uint8_t* data, uint16_t len, uint16_t data_offset)
{
    uint16_t ptr = readSnTX_WR(s);
    uint8_t cntl_byte = ((ACC_WRITE | TX_PTR) + (s << 5));
    ptr += data_offset;
    writeN(ptr, cntl_byte, data, len);
    ptr += len;
    writeSnTX_WR(s, ptr);
}

void wz_driver::readSn_RxBuf(int s, uint8_t* data, uint16_t len, uint8_t peek)
{
    uint16_t ptr = readSnRX_RD(s);
    uint8_t cntl_byte = ((RX_PTR | ACC_READ) + (s << 5));
    readN((uint16_t)ptr, cntl_byte, (uint8_t*)data, len);
    if (!peek)
    {
        ptr += len;
        writeSnRX_RD(s, ptr);
    }
}

void wz_driver::execSnCR(int s, uint8_t _cmd)
{
    /* send command and then wait for it to complete */
    writeSnCR(s, _cmd);
    while (readSnCR(s))
        ;
}
