/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
*/

/* /dev/net0 */

/*
 *  WIZNET W5500 Ethernet I/F special file
 *  minor device is only checked to make sure it's 0
 *
 *  Most low level handling is abstracted into w5500.cpp
 */

extern "C"
{
#include "../wz.h"

#include "../../net/protocol.h"
#include "../dev.h"
#include "../ifcom.h"

    void debug_print(const char* line);
    void debug_printline(const char* line);
    extern void debug_dump(unsigned char* data, int len);

    int ether_dissect(int psw, int family, int type, int proto, struct iface* ifs, unsigned char* data);
}

#include "../w5500.hpp"

#include <Arduino.h>

#define ETHER_PIN_CS SS  // 10

#define ALLOW_CONTINUOUS_READ 1

struct iface wziface[1];
struct iface* wzifrs[] = {
  &wziface[0],
  NULL};
unsigned char rxbuf[MAC_RAW_MTU];

volatile int linkstat = LNK_UNKOWN;

extern "C" int wzlnk(unsigned short dev, int flag)
{
    if (major(dev) != DEV_MAJOR_WZ)
        return -1;

    if (minor(dev) != 0)
        return -1;

    /* wait for busy flag to close */
    while (wziface[0].ifr_status & IFS_BUSY)
        ;

    /* set that we're busy */
    wziface[0].ifr_status |= IFS_BUSY;

    /* check link status */
    linkstat = WZ.getLNK();

    if (linkstat == LNK_UP)
        wziface[0].ifr_status |= IFS_LINKUP;
    else
        wziface[0].ifr_status &= ~IFS_LINKUP;

    wziface[0].ifr_status &= ~IFS_BUSY;

    return 0;
}

/*
    Called by kernel to initialise the phy (aka probe)
*/
extern "C" int wzinit(unsigned short dev, int flag, unsigned short mode)
{
    if (major(dev) != DEV_MAJOR_WZ)
        return -1;

    if (minor(dev) != 0)
        return -1;

    /* device is already open or being opened, you can't open it again... */
    if (wziface[0].ifr_status & (IFS_ISOPEN | IFS_WOPEN))
        return -1;

    /* wait for busy flag to close */
    while (wziface[0].ifr_status & IFS_BUSY)
        ;

    /* set that we're busy */
    wziface[0].ifr_status |= IFS_BUSY;

    /* clear any old left over info */
    memset(&wziface[0], 0, sizeof(struct iface));

    /* link our buffer to the interface */
    memset(rxbuf, 0, MAC_RAW_MTU);
    wziface[0].ifr_data = (struct iframe*)&rxbuf[0];

    /* flag that we're opening the interface */
    wziface[0].ifr_status |= IFS_WOPEN;
    wziface[0].ifr_status |= IFS_BUSY;

    /* init the wiznet chip */
    if (!WZ.init(ETHER_PIN_CS))
    {
        wziface[0].ifr_status = 0;
        wziface[0].ifr_status &= ~IFS_BUSY;
        return -1;
    }

    /* defaults for chip */
    wziface[0].ifr_metric = 4;
    wziface[0].ifr_mtu = MAC_RAW_MTU;
    wziface[0].ifr_type = IFT_ETHER;

    /* call common code to finish off the init */
    if_init(dev, &wziface[0]);

    /* and done! */
    wziface[0].ifr_status &= ~IFS_BUSY;
    return 0;
}

/*
    Called by the kernel to shutdown the phy
*/
extern "C" int wzoff(unsigned short dev, int flag)
{
    if (major(dev) != DEV_MAJOR_WZ)
        return -1;

    if (minor(dev) != 0)
        return -1;

    /* device isn't opened yet... */
    if (!(wziface[0].ifr_status & IFS_ISOPEN))
        return -1;

    /* wait for busy flag to close */
    while (wziface[0].ifr_status & IFS_BUSY)
        ;

    wziface[0].ifr_status |= IFS_BUSY;

    /* nothing to do except call common code to close the device and clear flags */
    if_off(&wziface[0]);

    wziface[0].ifr_status &= ~IFS_BUSY; /* this will already be blank. */

    return 0;
}

/*
    called by poll routine - loads all pending packets from the phy through to sockets via switching
*/
extern "C" int wzload()
{
    /* device isn't opened yet... */
    if (!(wziface[0].ifr_status & IFS_ISOPEN))
        return -1;

    /* link not up or it's not running */
    if (!(wziface[0].ifr_status & IFS_LINKUP))
        return -1;

    if (!(wziface[0].ifr_flags & IFF_UP))
        return -1;

    if (!(wziface[0].ifr_flags & IFF_RUNNING))
        return -1;

    /* wait for busy flag to close */
    while (wziface[0].ifr_status & IFS_BUSY)
        ;

    /* set that we're busy */
    wziface[0].ifr_status |= IFS_BUSY;

    unsigned short rx_avail = 0;

#if ALLOW_CONTINUOUS_READ
    /* While there is data available to read */
    while ((rx_avail = WZ.getSnRX_RSR(0)) > 0)
    {
#else
    /* if there is data available to read */
    if ((rx_avail = WZ.getSnRX_RSR(0)) > 0)
    {
#endif

        /* read the first two bytes of the packet (the length) */
        WZ.readSn_RxBuf(0, (unsigned char*)wziface[0].ifr_data, 2);
        WZ.execSnCR(0, Sn_CR_RECV);

        int rlen = ntohs(wziface[0].ifr_data->ifhead.h_len);

        if (rlen > 0)
        {
            if (rlen > sizeof(struct ifhdr))
                rlen -= sizeof(struct ifhdr);

            if (rlen > wziface[0].ifr_mtu)
            {
                if (wziface[0].ifr_flags & IFF_DEBUG)
                    debug_printline("WZ: Rejected Packet (larger than MTU)");

                /* if packet would be too large, just flush it away */
                WZ.flushRxBuf(0, rlen);
                WZ.execSnCR(0, Sn_CR_RECV);

                wziface[0].ifr_status &= ~IFS_BUSY;
                return -1;
            }

            /* read the ethernet header of the packet */
            WZ.readSn_RxBuf(0, wziface[0].ifr_data->buf, sizeof(struct ether_header));
            WZ.execSnCR(0, Sn_CR_RECV);

            struct ethframe* ether = (struct ethframe*)wziface[0].ifr_data;

            if (wziface[0].ifr_flags & IFF_PROMISC ||                                     /* Promiscious mode */
                (ether->ehead.h_dest[0] == 0xFF && ether->ehead.h_dest[5] == 0xFF) ||     /* The destination address is a broadcast address */
                memcmp(ether->ehead.h_dest, wziface[0].ifr_hwaddr.m_addr, ETH_ALEN) == 0) /* The destination address is this if's address */
            {
                /* read rest of packet */
                WZ.readSn_RxBuf(0, ether->buf, rlen - sizeof(struct ether_header));
                WZ.execSnCR(0, Sn_CR_RECV);

                if (wziface[0].ifr_flags & IFF_DEBUG)
                    debug_printline("WZ: Received packet");

                // You can't have a socket at this level, so just send straight to dissector for ETH_ANY
                /* for performance, we don't use the ifcom stuff,
                   as that wouldn't allow us to dump ether packets not meant for us
                   without reading them */
                proto_dissect(PS_ETH_ANY, PF_PACKET, SOCK_RAW, htons(ETH_P_INTERNAL_ANY), &wziface[0], (unsigned char*)wziface[0].ifr_data);
            }
            else
            {
                if (wziface[0].ifr_flags & IFF_DEBUG)
                    debug_printline("WZ: Rejected packet (wrong address)");

                /* dump rest of packet */
                WZ.flushRxBuf(0, rlen - sizeof(struct ether_header));
                WZ.execSnCR(0, Sn_CR_RECV);
            }
        }
    }

    wziface[0].ifr_status &= ~IFS_BUSY;
    return 0;
}

/*
    called by socket layer when there's a frame that's been built and needs to be written out
*/
extern "C" int wzwrite(unsigned short dev, const struct iframe* frame)
{
    if (major(dev) != DEV_MAJOR_WZ)
        return -1;

    if (minor(dev) != 0)
        return -1;

    /* device isn't opened yet... */
    if (!(wziface[0].ifr_status & IFS_ISOPEN))
        return -1;

    /* link not up or it's not running */
    if (!(wziface[0].ifr_status & IFS_LINKUP))
        return -1;

    /* wait for busy flag to close */
    while (wziface[0].ifr_status & IFS_BUSY)
        ;

    /* set that we're busy */
    wziface[0].ifr_status |= IFS_BUSY;

    int wlen = ntohs(frame->ifhead.h_len);
    if (wlen > 0)
    {
        wlen -= sizeof(struct ifhdr);

        /* wait for space in the chip's buffer */
        while (1)
        {
            int freesize = WZ.getSnTX_FSR(0);
            int sr = WZ.readSnSR(0);

            /* if either of these is the case, something went very wrong */
            if (sr == Sn_SR_CLOSED || sr == Sn_SR_CLOSING)
            {
                wziface[0].ifr_status &= ~IFS_BUSY;
                return -1;
            }

            /* SPACE! */
            if (wlen <= freesize)
            {
                break;
            }
        }

        /* write out buffer part of frame */
        WZ.writeSn_TxBuf(0, (unsigned char*)&frame->buf[0], wlen);
        WZ.execSnCR(0, Sn_CR_SEND);

        /* check that send finished, or if there was an error */
        while (1)
        {
            uint8_t ir = WZ.readSnIR(0);
            if (ir & Sn_IR_SEND_OK)
            {
                WZ.writeSnIR(0, Sn_IR_SEND_OK);
                break;
            }

            if (ir & Sn_IR_TIMEOUT)
            {
                WZ.writeSnIR(0, Sn_IR_TIMEOUT);
                wziface[0].ifr_status &= ~IFS_BUSY;
                return -1;
            }
        }
    }

    wziface[0].ifr_status &= ~IFS_BUSY;
    return wlen;
}

/*
    called by user layer to change settings on the interface
*/
extern "C" int wzioctl(unsigned short dev, int cmd, void* addr, int flag)
{
    if (major(dev) != DEV_MAJOR_WZ)
        return -1;

    if (minor(dev) != 0)
        return -1;

    /* device isn't opened yet... */
    if (!(wziface[0].ifr_status & IFS_ISOPEN))
        return -1;

    /* wait for busy flag to close */
    while (wziface[0].ifr_status & IFS_BUSY)
        ;

    /* set that we're busy */
    wziface[0].ifr_status |= IFS_BUSY;

    /* do common ioctl code */
    if (if_ioctl(&wziface[0], cmd, addr, flag) < 0)
    {
        wziface[0].ifr_status &= ~IFS_BUSY;
        return -1;
    }

    /* write out addressing config -> the wiznet chip will handle ARP for us */
    WZ.writeSHAR(wziface[0].ifr_hwaddr.m_addr);
    WZ.writeSIPR((unsigned char*)&wziface[0].ifr_addr.s_addr);
    WZ.writeSUBR((unsigned char*)&wziface[0].ifr_netmask.s_addr);
    WZ.writeGAR((unsigned char*)&wziface[0].ifr_gateaddr.s_addr);

    /* if we've been brought online */
    if (wziface[0].ifr_flags & IFF_UP)
    {
        if (wziface[0].ifr_flags & IFF_PROMISC)
        {
            /* actually do the registers and (re)open the socket */
            WZ.execSnCR(0, Sn_CR_CLOSE);
            delay(1);
            WZ.writeSnMR(0, Sn_MR_MACRAW);  //| Sn_MR_MF | Sn_MR_ND
            WZ.execSnCR(0, Sn_CR_OPEN);
            WZ.writeSnRX_SIZE(0, 16);
            WZ.writeSnTX_SIZE(0, 16);

            /* make sure that the socket is configured properly */
            // if (WZ.readSnMR(0) != (Sn_MR_MACRAW))
            // {
            //     wziface[0].ifr_status &= ~IFS_BUSY;
            //     return -1;
            // }
        }
        /* if we're not in promiscious mode, enable address filter on PHY */
        else
        {
            /* actually do the registers and (re)open the socket */
            WZ.execSnCR(0, Sn_CR_CLOSE);
            delay(1);
            WZ.writeSnMR(0, Sn_MR_MACRAW | Sn_MR_MF | Sn_MR_ND);
            WZ.execSnCR(0, Sn_CR_OPEN);
            WZ.writeSnRX_SIZE(0, 16);
            WZ.writeSnTX_SIZE(0, 16);

            // /* make sure that the socket is configured properly */
            // if (WZ.readSnMR(0) != (Sn_MR_MACRAW | Sn_MR_MF | Sn_MR_ND))
            // {
            //     wziface[0].ifr_status &= ~IFS_BUSY;
            //     return -1;
            // }
        }
    }

    wziface[0].ifr_status &= ~IFS_BUSY;

    /* update link status (outside busy locks as sets this itself)*/
    wzlnk(dev, 0);

    return 0;
}

/*
    Called regularly, or on interrupt, calls the function to load all current packets into sockets
*/
extern "C" int wzpoll(unsigned short dev, int flag)
{
    if (major(dev) != DEV_MAJOR_WZ)
        return -1;

    if (minor(dev) != 0)
        return -1;

    /* device isn't opened yet... */
    if (!(wziface[0].ifr_status & IFS_ISOPEN))
        return -1;

    /* load arrived packets */
    wzload();

    return 0;
}
