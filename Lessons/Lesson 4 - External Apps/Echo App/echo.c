/* infinite character echo using syscalls to be loaded as separate program */

#define sys_ent_none  0
#define sys_ent_write 1
#define sys_ent_read  2
#define nsysent       3

__attribute__((noinline)) int syscall(register int sysnum, register int a, register int b, register int c, int d, int e, int f)
{
    register int _r6 __asm__("r6") = (f);
    register int _r5 __asm__("r5") = (e);
    register int _r4 __asm__("r4") = (d);
    register int _r3 __asm__("r3") = (c);
    register int _r2 __asm__("r2") = (b);
    register int _r1 __asm__("r1") = (a);
    register int _r0 __asm__("r0") = (sysnum);
    asm volatile("svc #5"
                 : "=r"(_r0)
                 : "r"(_r0), "r"(_r1), "r"(_r2), "r"(_r3), "r"(_r4), "r"(_r5), "r"(_r6)
                 : "memory");
    return _r0;
}

/* write-like syscall */
int write(const char* string, int length)
{
    /* call system with write call */
    int r = syscall(sys_ent_write, (int)string, (int)length, 0, 0, 0, 0);
    return r;
}

/* read-like syscall */
int read(char* buf, int length)
{
    /* call system with read call */
    int r = syscall(sys_ent_read, (int)buf, (int)length, 0, 0, 0, 0);
    return r;
}

/*  
    Please note... there's actually a bug in the config for the compiler when in PIE mode which
    means that global pointers end up put in the binary as absolute references and don't 
    get initialised as relative values (local pointers work fine, even though the pointer
    and contents end up in the same places)

    This version of the GCC compiler was never really designed for our raw fPIE compilation and execution,
    as a result, global strings must be const char[] and not const char*
*/

int psize = 7;
const char prompt[] = "echo> ";

int main(void)
{
    /* write the prompt out */
    write(prompt, psize);

    char rc = 0;
    int r = 0;

    while (1)
    {
        rc = 0;
        read(&rc, 1);  /* if read has nothing then we would get back an error -1, and rc would be left as is */
        write(&rc, 1); /* but if rc is still 0, then we can write 0 and it doesn't cause a problem, so no error checking needed... */

        /* If we get a 3, that's CTRL+C, so treat as clean exit and return psize+1 */
        if (rc == 3)
            return ++psize;

        /* if we get a 4, then that's CTRL+D, so treat as a kill and return psize-1 */
        if (rc == 4)
            return --psize;

        /* delay, otherwise repetitive SVC calls slow our CPU down like mad */
        int x = 5000;
        while (x--)
            ;
    }
}

/* the linker sets this as the entry point to the executable */
__attribute__((noinline)) int _start(void)
{
    return main();
}
