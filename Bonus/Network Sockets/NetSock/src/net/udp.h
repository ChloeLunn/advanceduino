/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

#if !H_UDP
#define H_UDP 1

#include "if.h"

#ifndef __packed
#define __packed __attribute__((__packed__))
#endif

#ifndef __aligned
#define __aligned(x) __attribute__((__aligned__(x)))
#endif

/* ------------------------------------------------------------------------- */

struct udphdr {
    unsigned short uh_sport; /* source port */
    unsigned short uh_dport; /* destination port */
    unsigned short uh_ulen;  /* length */
    unsigned short uh_sum;   /* checksum */
} __packed;

struct udpframe {
    struct ifhdr ifhead;       /* interface header (length in network order) */
    struct ether_header ehead; /* ethernet header */
    struct ip iphead;          /* ip header */
    struct udphdr udphead;     /* udp header */
    unsigned char buf[1];      /* packet buffer */
} __packed;

struct udp6frame {
    struct ifhdr ifhead;       /* interface header (length in network order) */
    struct ether_header ehead; /* ethernet header */
    struct ip6 ip6head;        /* ip header */
    struct udphdr udphead;     /* udp header */
    unsigned char buf[1];      /* packet buffer */
} __packed;

/* ------------------------------------------------------------------------- */

#endif
