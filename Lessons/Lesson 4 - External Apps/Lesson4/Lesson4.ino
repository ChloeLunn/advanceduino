/*
    This example is basically the userapp example, however it instead loads a proper
    elf file.

    You'll need a FAT formatted SD card for this one, and then compile the echo app and
    copy it across to the top directory.

    General/easy reading on loading ELF files can be found here:
    https://wiki.osdev.org/ELF#Loading_ELF_Binaries
    https://wiki.osdev.org/ELF_Tutorial
    https://en.wikipedia.org/wiki/Executable_and_Linkable_Format

    Detailed spec information can be found here:
    http://www.skyfree.org/linux/references/ELF_Format.pdf
    https://docs.oracle.com/cd/E23824_01/html/819-0690/glcfv.html
    https://docs.oracle.com/cd/E19683-01/816-1386/chapter6-83432/index.html


    Please note: This example is currently set to have 128KiB of program space.
    If you are going to use this on SAMD21 boards, you will need to reduce this
    and make sure that your app will fit.

    At the moment, the make for the app assumes SAMD51
*/

/* This will be the file we load from the SD card */
#define APP_NAME "echo"
#define APP_DIR  "/echo"

#include "elf.h"

#include <Arduino.h>
#include <SdFat.h>
#include <Variant.h>
#include <sdios.h>
#include <variant.h>

#if !defined(__CORTEX_M)
#error Sorry, I don't recogmise this target...
#endif

#include <string.h>

/* SD card object */
SdFat sd;

#define Ki *1024 /* quick macro to do the times for Kis */

#define STACK_SIZE (1 Ki)
#define PROG_SIZE  (10 Ki)

unsigned short program_space[PROG_SIZE]; /* 20KiB program space  */
unsigned int program_stack[STACK_SIZE];  /* 4KiB stack */
unsigned int program_size = 0;           /* loaded size of program */

/* simple c typedef for the start function prototype */
typedef int (*app_start_t)(void);

/*
    Loading an elf file when it's a statically-linked executable is actually
    *really* simple, despite some examples that end up showing dynamic linking.

    As long as you're not needing to dynamically link it at runtime to your
    sketch or other files (libraries) and it is actually static and you've used
    real system calls to call your kernel functions, then...

    Essentially an elf is split into three types of header (excluding the main
    file/elf header) and when you load an executable you only need to load from
    one.

    The program header includes 1. if you need to load it, 2. how much space it
    will take once loaded, 3. how much space it takes in the file, 4. where to
    copy it to, and if you have memory management or protection unit, how to
    set the permissions on it.

    That's it. You just iterate through the list and load the ones appropriate.

    If you had a relocatable or dynamically linked file, then this would be
    more complicated, as you would then need to use the section and symbol
    tables and headers to load the names and positions of code and variables
    that need to be adjusted to your setup.

    We don't need to load anything other than program segments that have
    headers that mark them as needing to be loaded with PH_LOAD

    In an actual OS we also have to consider the PT_INTERP option. This program
    segment contains the path to a program that we need to also load and then
    link and run to properly work this file.
    The kernel elf loader is usually a really basic exec loader, and all
    dynamically linked elfs and more complicated programs will use that.
*/

/* print an elf header's info */
void print_elf(struct elf32_header* elf)
{
    Serial.printf("%s", "\nIdentity:");
    Serial.printf("%s", "\r\n---------\r\n");
    Serial.printf("Magic       = 0x%02X, '%c', '%c', '%c'\r\n", elf->e_ident.e_magic[0], elf->e_ident.e_magic[1], elf->e_ident.e_magic[2], elf->e_ident.e_magic[3]);
    Serial.printf("Class       = %s\r\n", ((elf->e_ident.e_class == 2) ? "64-bit" : "32-bit"));
    Serial.printf("Data Type   = %s\r\n", ((elf->e_ident.e_data == 1) ? "Little Endian" : "Big Endian"));
    Serial.printf("Version     = %u\r\n", elf->e_ident.e_version);

    switch (elf->e_ident.e_abi)
    {
    case ELF_OSABI_SYSV:
        Serial.println("OS/ABI      = UNIX SysV");
        break;
    case ELF_OSABI_LINUX:
        Serial.println("OS/ABI      = Linux");
        break;
    case ELF_OSABI_FREEBSD:
    case ELF_OSABI_OPENBSD:
        Serial.println("OS/ABI      = BSD");
        break;
    case ELF_OSABI_ARM_AEABI:
        Serial.println("OS/ABI      = ARM (AEBI)");
        break;
    case ELF_OSABI_ARM:
        Serial.println("OS/ABI      = ARM");
        break;
    case ELF_OSABI_STANDALONE:
        Serial.println("OS/ABI      = Standalone");
        break;
    default:
        Serial.printf("OS/ABI      = %u\r\n", elf->e_ident.e_abi);
        break;
    }
    Serial.printf("ABI Version = %u\r\n", elf->e_ident.e_abi_version);

    if (elf->e_ident.e_class == 2)
    {
        Serial.println("\nELF64 dissector not implemented.");
        return;
    }
    else
    {
        Serial.printf("%s", "Ident extensions: ");
        for (int i = 0; i < 7; i++)
        {
            Serial.printf("%02X ", elf->e_ident.e_pad[i]);
        }
        Serial.println("");

        Serial.printf("%s", "\nHeader:");
        Serial.printf("%s", "\r\n-------\r\n");
        switch (elf->e_type)
        {
        case ET_REL:
            Serial.println("File Type   = Relocatable");
            break;
        case ET_EXEC:
            Serial.println("File Type   = Executable");
            break;
        case ET_DYN:
            Serial.println("File Type   = Shared (dynamic)");
            break;
        case ET_CORE:
            Serial.println("File Type   = Core Dump");
            break;
        case ET_NONE:
            Serial.println("File Type   = None");
            break;
        default:
            Serial.printf("File Type   = 0x%04X\r\n", elf->e_type);
            break;
        }
        switch (elf->e_machine)
        {
        case EM_ARM:
            Serial.println("Mach Type   = ARM");
            break;
        case EM_AMD64:
            Serial.println("Mach Type   = AMD64");
            break;
        case EM_AARCH64:
            Serial.println("Mach Type   = AARCH64");
            break;
        case EM_386:
            Serial.println("Mach Type   = i386");
            break;
        case EM_486:
            Serial.println("Mach Type   = i486");
            break;
        case EM_IA_64:
            Serial.println("Mach Type   = IA-64");
            break;
        case EM_NONE:
            Serial.println("Mach Type   = None");
            break;
        default:
            Serial.printf("Mach Type   = 0x%04X\r\n", elf->e_machine);
            break;
        }
        Serial.printf("Version     = 0x%02X\r\n", elf->e_version);
        Serial.printf("Entry point = 0x%04X\r\n", elf->e_entry);
        Serial.printf("Prog Header = %u (byte offset in file)\r\n", elf->e_phoff);
        Serial.printf("Sect Header = %u (byte offset in file)\r\n", elf->e_shoff);
        Serial.printf("Mach Flags  = 0x%08X\r\n", elf->e_flags);
        Serial.printf("Header Size = %u (bytes)\r\n", elf->e_ehsize);
        Serial.printf("Prog Hd Siz = %u (bytes)\r\n", elf->e_phentsize);
        Serial.printf("Prog Hd Num = %u \r\n", elf->e_phnum);
        Serial.printf("Sect Hd Siz = %u (bytes)\r\n", elf->e_shentsize);
        Serial.printf("Sect Hd Num = %u\r\n", elf->e_shnum);
        Serial.printf("String Idx  = %u\r\n", elf->e_shstrndx);
        Serial.println("");
    }
}

/* load the elf file at path into mem */
int loader(const char* path, void* mem, unsigned int* entry, unsigned int msize)
{
    SdFile file;

    /* open the elf file, we don't need to write to it, so read only is fine */
    if (!file.open(path, O_RDONLY))
    {
        Serial.println("Error opening file.");
        return -1;
    }

    /* load in the initial elf header */
    struct elf32_header elf_head;
    file.read(&elf_head, sizeof(struct elf32_header));

    /* now we do some sanity checks to make sure it's an elf file and right type */

    /* Magic number doesn't match */
    if (memcmp(elf_head.e_ident.e_magic, elf_magic, 4) != 0)
    {
        Serial.println("File is not an elf.");
        file.close();
        return -1;
    }

    /* if not 32-bit */
    if (elf_head.e_ident.e_class != ELF_CLASS_32)
    {
        Serial.println("Not 32-bit.");
        file.close();
        return -1;
    }

    /* if not little-endian with 2s compliment negatives */
    if (elf_head.e_ident.e_data != ELF_ENCODE_2LSB)
    {
        Serial.println("Not correct data encoding.");
        file.close();
        return -1;
    }

    /* if not SysV ABI (or none, same thing) */
    if (elf_head.e_ident.e_abi != ELF_OSABI_SYSV)
    {
        Serial.println("Not correct ABI.");
        file.close();
        return -1;
    }

    /* if not an executable */
    if (elf_head.e_type != ET_EXEC)
    {
        /*
            Other options to watch out for would be:

            EL_REL for a relocatable file, designed for pre-static-linking
            EL_DYN for shared object files, designed for dynamic linking

            but these need different handling, so we'll ignore them for now
        */
        Serial.println("Not correct type of elf.");
        file.close();
        return -1;
    }

    /* if not for arm processors */
    if (elf_head.e_machine != EM_ARM)
    {
        /*
            We can fine out if it's arm thumb and v7e-m like we
            want. but we'll have to load more info.
        */
        Serial.println("Not an ARM program.");
        file.close();
        return -1;
    }

    /* if not version 1 (the only version) */
    if (elf_head.e_version != 1)
    {
        Serial.println("Invalid elf version.");
        file.close();
        return -1;
    }

    print_elf(&elf_head);

    struct elf32_pheader phdr;

    /* seek to the program headers */
    file.seek(elf_head.e_phoff);

    /* The first part of loading is to now load the minimum address and the
       executable size */
    unsigned int base = 0xFFFFFFFF;
    unsigned int size = 0;

    /*
        Loop 1: grab the lowest address needed, this tells us the base
        reference to the memory for when we start copying segments into
        memory.

        This won't make a _huge_ difference as usually the program will
        be offset from address 0, however on the off chance that
        the program segments we load will actually start higher up, we
        can instead shift them all down a bit using this value so that
        the program takes up less space
    */
    for (int i = 0; i < elf_head.e_phnum; i++)
    {
        /* seek to program header i */
        file.seek(elf_head.e_phoff + (elf_head.e_phentsize * i));

        /* and read the header */
        file.read(&phdr, sizeof(struct elf32_pheader));

        /* if not a loadable type, ignore it */
        if (phdr.p_type != PT_LOAD)
        {
            continue;
        }

        /* if it has more data in the file than it will take up... */
        if (phdr.p_filesz > phdr.p_memsz)
        {
            /*
                We do this error check now because there's no point finishing
                the loading with an invalid program segment.
            */
            Serial.println("Invalid program segment found.");
            file.close();
            return -1;
        }

        if (phdr.p_vaddr < base)
        {
            base = phdr.p_vaddr;
        }
    }

    Serial.printf("Base offset in memory is %u\r\n", base);

    /*
        Loop 2: grab the maximum program size, this doesn't tell us much,
        but if we were using 'malloc' or mmap or similar, then this would
        be needed before the next loop
    */
    for (int i = 0; i < elf_head.e_phnum; i++)
    {
        /* seek to program header i */
        file.seek(elf_head.e_phoff + (elf_head.e_phentsize * i));

        /* and read the header */
        file.read(&phdr, sizeof(struct elf32_pheader));

        /* if not a loadable type, ignore it */
        if (phdr.p_type != PT_LOAD)
        {
            continue;
        }

        /* offset from the base + size */
        if ((phdr.p_vaddr - base) + phdr.p_memsz > size)
        {
            size = (phdr.p_vaddr - base) + phdr.p_memsz;
        }
    }

    if (size <= msize)
    {
        Serial.printf("Loaded program size will be %u\r\n", size);
    }
    else
    {
        Serial.printf("Program too big for memory %u > %u\r\n", size, msize);
        file.close();
        return -1;
    }

    /* Loop 3: load the program sections into memory */
    for (int i = 0; i < elf_head.e_phnum; i++)
    {
        /* seek to program header i */
        file.seek(elf_head.e_phoff + (elf_head.e_phentsize * i));

        /* and read the header */
        file.read(&phdr, sizeof(struct elf32_pheader));

        /* if not a loadable type, ignore it */
        if (phdr.p_type != PT_LOAD)
        {
            continue;
        }

        Serial.printf("Loading program segment %i\r\n", i);
        Serial.printf("\tFile offset   = 0x%08X\r\n", phdr.p_offset);
        Serial.printf("\tVirtual Addr  = 0x%08X\r\n", phdr.p_vaddr);
        Serial.printf("\tSegment flags = 0x%08X\r\n", phdr.p_flags);
        Serial.printf("\tVirtual Addr  = 0x%08X\r\n", phdr.p_vaddr);
        Serial.printf("\tSegment size  = 0x%08X\r\n", phdr.p_memsz);
        Serial.printf("\tOn-disk size  = 0x%08X\r\n", phdr.p_filesz);
        Serial.printf("\tAlignment     = 0x%08X\r\n", phdr.p_align);

        if (!(phdr.p_vaddr % phdr.p_align == phdr.p_offset % phdr.p_align))
        {
            Serial.printf("Inavlid alignment on segment.\r\n");
            file.close();
            return -1;
        }

        /* Offset in memory from the address in the program segment */
        unsigned int mem_offset = (phdr.p_vaddr - base);

        /* Clear the memory for this segment */
        memset(mem + mem_offset, 0, phdr.p_memsz);

        /*
            Note how we use filesz and memsz differently.
            Filesz tells the read how much data to copy
            Memsz tells it how much room it should take once loaded

            The difference is cleared, but it's fine that we cleared the lot as
            we then write over it with the data if there is some
        */

        /* Load data from file if required */
        if (phdr.p_filesz)
        {
            /* seek to the actual program data in elf */
            file.seek(phdr.p_offset);

            /* and read it into the memory */
            file.read(mem + mem_offset, phdr.p_filesz);

            /*
                We could watch for memory alignment here, the phdr does contain
                the information about alignment, however we can usually assume
                that the alignment happened before the final static exec was
                made, so the addresses and offsets should be aligned as required
            */
        }

        /*
            The program header also contains a bit mask for permissions:
            Read, Write, and Executable.

            The M4 does actually have an MPU, so we could do this... however we
            won't right now as it will just add complication, and it's not as
            simple as it first appears when it comes to privilaged vs
            unprivilaged masking.

            A text segment will commonly have PF_X|PF_R, and a data segment
            will usually have PF_X|PF_W|PF_R
        */

        if (phdr.p_flags & PF_W)
        {
            /* Set the area as writable */
        }

        if (phdr.p_flags & PF_R)
        {
            /*
                We can often assume this one, but there is a slim chance that
                their will be a segment that may be set as non-readable
             */
        }

        if (phdr.p_flags & PF_X)
        {
            /* Set the area as executable */
        }
    }

    /* set the value in entry to the entry point in the now-loaded memory */
    (*entry) = (elf_head.e_entry - base);

    Serial.printf("Final entry offset    = 0x%08X\r\n", (elf_head.e_entry - base));

    /*
        We can now close the file as we've loaded all the data from it

        And we will return the size of the data loaded
    */

    file.close();

    return (int)size;
}

/* Arduino setup */
void setup()
{
    /* This test needs the tty for doing the echo app */
    while (!Serial)
    {
        /* we throttle this so that if the bootloader needs to be entered we can do that */
        delay(100);
    }

    /* we'll force a delay for a bit just for stability */
    delay(1000);

    /* Try and initialise the SD card */
    if (!sd.begin(SDCARD_SS_PIN, SD_SCK_MHZ(12)))
    {
        Serial.println("Error initialising SD card.");
        return;
    }

    Serial.printf("Opening application %s from %s into mem 0x%08X\r\n", APP_NAME, APP_DIR, (unsigned int)&program_space);

    /*
        unlike the userapp we have an entry point definition
        from the elf format.
        If the loader works, this gets filled in automatically and give us the _byte_ offset for the program's _start function
    */
    unsigned int entry_point = 0;

    /* Load elf here */
    program_size = loader(APP_DIR, program_space, &entry_point, PROG_SIZE * 2);

    if (program_size > 0 && program_size <= PROG_SIZE * 2)
    {
        Serial.printf("Loaded %u bytes of program.\r\n", program_size);

        int fun_ret = -1;

        /* then execute the program! */
        Serial.println("Launch!");

        /* convert program space to a byte pointer for a moment */
        unsigned char* prog_bytes = (unsigned char*)&program_space[0];

        /* add the offset to our entry point */
        prog_bytes = prog_bytes + entry_point;

        /* convert the address to an int. We don't need the OR 1 like before, because the entry point has added this for us */
        unsigned int prog_addr = (unsigned int)prog_bytes;

        Serial.printf("Launching from Address  0x%08X\r\n", (unsigned int)prog_addr);

        /* set stack pointer to end of stack space (counts down) */
        __set_MSP((uint32_t)&program_stack[(STACK_SIZE)-1]);

        /* we do this call in C, just to make it slightly cleaner to get return codes and pass arguments (not that we're passing args right now) */
        fun_ret = ((app_start_t)prog_addr)();

        Serial.print("\r\nApp returned: ");
        Serial.println(fun_ret);
    }
    else if (program_size)
    {
        Serial.println("Error loading program.");
    }
    else
    {
        Serial.println("Program was empty.");
    }
}

/* does nothing, the app has finished */
void loop()
{
}

/* ~~~~ SYSTEM CALL CODE BELOW HERE SAME AS USERAPP EXAMPLE ~~~~ */

#define sys_ent_none  0
#define sys_ent_write 1
#define sys_ent_read  2
#define nsysent       3

struct svc_context {
    int r0;
    int r1;
    int r2;
    int r3;
    int r12;
    int lr;
    int ret;
    int xPSR;
};

/* kernel system write function (must be noinline to kill SVC interrupt) */
extern "C" int __attribute__((noinline)) sys_write(const char* buf, int length)
{
    char* b2 = (char*)buf;

    int wlen = length;

    while (wlen > 0)
    {
        Serial.write(*b2++);
        wlen--;
    }

    return length - wlen;
}

/* kernel system read function (must be noinline to kill SVC interrupt) */
extern "C" int __attribute__((noinline)) sys_read(char* buf, int length)
{
    char* b2 = (char*)buf;

    int rlen = length;

    while (rlen > 0)
    {
        int x = Serial.read();
        if (x > 0)
            *b2++ = (char)x;
        rlen--;
    }

    return length - rlen;
}

/* unroll syscall arguments, do the actual call, and then return */
extern "C" void SVC_Handler_Main(struct svc_context* context)
{
    register int _r4 __asm("r4");
    register int _r5 __asm("r5");
    register int _r6 __asm("r6");

    unsigned int svc_number = 0;
    svc_number = (((unsigned int*)((context->ret)))[-1] >> 16) & 0xFF;

    /* if we don't recognise it, then we can't process it, so quit but print an error */
    if (svc_number != 5)
    {
        Serial.printf("Error: Unknown SVC number (%x hex / %u dec)\r\n", svc_number, svc_number);
        context->r0 = -1;
        return;
    }
    switch (context->r0)
    {
    case sys_ent_write:
        context->r0 = sys_write((const char*)context->r1, (int)context->r2);
        break;
    case sys_ent_read:
        context->r0 = sys_read((char*)context->r1, (int)context->r2);
        break;
    default:
        Serial.printf("Error: Unknown syscall (%x hex / %u dec)\r\n", context->r0, context->r0);
        context->r0 = -1;
        break;
    }
}

/* Interrupt handler */
extern "C" void SVC_Handler(void)
{
#if defined(__CORTEX_M) && (__CORTEX_M == 4)
    __asm(
      ".global SVC_Handler_Main\n"
      "TST lr, #4\n"
      "ITE EQ\n"
      "MRSEQ r0, MSP\n"
      "MRSNE r0, PSP\n"
      "B SVC_Handler_Main\n");
#elif defined(__CORTEX_M) && (__CORTEX_M == 0)
    __asm(
      "MRS     R0, MSP\t\n" /* Read MSP */
      "B       SVC_Handler_Main\t\n");
#endif
}
