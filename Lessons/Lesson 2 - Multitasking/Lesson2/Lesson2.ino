/*
Copyright 2022 Chloe Lunn

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
    This is example code showing how to implement basic co-operative FIFO
    multitasking on a macrokernel.

    A macrokernel is where your OS programs are contained inside the kernel binary
    itself, which is a good description of how we can use the Arduino IDE to build
    up a mini OS from these examples.

    The basic difference being that instead of having a binary file you load and
    then call, your programs are called via function pointers as they're compiled
    into the kernel.

    And on that note... maybe you should just look it up yourself because I suck at
    explaining this...

    By FIFO we mean that we're not going to give processes a priority, we're just
    going to launch them in order, by co-operative we mean that the process will
    need to manually swap to the scheduler (yield) instead of it being swapped on
    a timer interrupt.

    So it's a lot simpler than a full-fat OS, but still shows the basic principle

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! THIS CODE IS FOR ADAFRUIT-STYLE SAMD51 BOARDS AND WILL NOT RUN ON AVR !!
    !! ARDUINO BOARDS OR SAMD21 BOARDS                                       !!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*/

#include <Arduino.h>
#include <Variant.h>
#if defined(__CORTEX_M) && (__CORTEX_M == 0)
#error Sorry, m0/SAMD21 isn't supported on this example...
#elif defined(__CORTEX_M) && (__CORTEX_M == 4)
#include <core_cm4.h>
#endif
#include <string.h>

/* ~~~~~~~~~~~~~~~~ PROCESS CONTROL STRUCTS ~~~~~~~~~~~~~~~~ */

/*
    In order to swap tasks we need to be able to save and restore register contents.
    We can do this by having an int array (like used by jmpbuf and label_t), but for carity
    we're going to give these nice register names in a struct.

    See https://developer.arm.com/documentation/dui0473/i/CJAJBFHC for general register info
*/
struct __attribute__((packed)) cpu_context {
    /* We're using unions to allow us to see the other names of these registers, but we won't use all the names. */

    /* Argument registers in r0-r3 */
    union {
        int r0;
        int a1;
    };
    union {
        int r1;
        int a2;
    };
    union {
        int r2;
        int a3;
    };
    union {
        int r3;
        int a4;
    };

    /*
        Because of the calling conventions used by ARM and the compiler,
        we're not actually going to save registers 0-3 during process swap as we
        can't guarantee that they'll be "clean" and need to stay "clean",
        but we're going to keep them in our struct so that if you call a new process
        we can pass a few arguments to it and from it and so that we can capture return codes
        during exit.

        Don't forget, that if you want more than 4 to be passed, you can always pass a pointer
        to a struct or array with more arguments.
     */

    /* Rest of low-side registers 4,5,6,7 */

    union {
        int r4;
        int v1;
    };
    union {
        int r5;
        int v2;
    };
    union {
        int r6;
        int v3;
    };
    union {
        int r7;
        int v4;
    };

    /* High-side registers 8,9,10,11,12 */
    union {
        int r8;
        int v5;
    };
    union {
        int r9;
        int v6;
    };
    union {
        int r10;
        int v7;
    };

    /* Frame Pointer register (counts as high-side) */
    union {
        int r11;
        int v8;
        int fp;
    };
    /*
        By convention, register 11 (on armv7 like the SAMD51) is also used as the frame pointer;
        the pointer to an on-stack copy of the registers at the time a function was called.

        We DO want to save this, as we need it for when we return out of scheduler and back into
        the task.
    */

    /* Scratchpad/intraprocedure register (counts as high-side) */
    union {
        int r12;
        int ip;
    };

    /*
        Like registers 0-3, we're not going to save/recall r12
        as it's the scratchpad register, it's just here for completeness.
    */

    /* Processor control registers */
    union {
        int r13;
        int sp; /* Stack pointer */
    };
    union {
        int r14;
        int lr; /* Link register */
    };
    union {
        int r15;
        int pc; /* Program counter */
    };

    /*
        And again, we're not going to use the pc, it's just here for completeness and spacing.
    */

    /*
        On the hardware floating point implementations (e.g. SAMD51) you then also have 16 more registers, s0-s15
        If you want to preserve these through task switches or do floats on your tasks, then you will need to implement this.
    */
};

/*
    Exercise: Implement & test the save and recall of floating point registers. (you may want to read the rest of this first)
*/

/* To manage a process we're going to have a struct instance for each one that holds it's information and stack */

/* We're going to give each one a name, this would usually be filled in with the command/file name on a *NIX system */
#define PROC_NAME_SIZE (64)

struct cpu_process {
    int p_state;                  /* current process status */
    int p_id;                     /* process id number (pid_t) */
    char p_name[PROC_NAME_SIZE];  /* give the process a friendly name */
    struct cpu_context p_context; /* saved, swappable registers */

    /*
        For the sake of simplicity, we're going to slap a 256-word stack onto the process now
        and declare a fixed array of processes instead of creating a dynamic memory page allocator.
    */
#define PROC_STACK_SIZE (256)
    int p_stack[PROC_STACK_SIZE];
};

#define PROCESS_UNUSED   0 /* process is currently unused */
#define PROCESS_RUNNABLE 1 /* process is ready to be run, but isn't running (aka IDLE) */
#define PROCESS_RUNNING  2 /* process is currently running */
#define PROCESS_DEAD     3 /* process has been killed, but not cleared yet */

/*
    To call a process with a function pointer, we need to know what it looks like.
    So this is our basic prototype for a function to be converted to a process.
 */
typedef int (*proc_function_t)(int, int, int, int);

/* and then some status info variables */
static volatile int total_processes = 0;
static volatile int running_processes = 1;
static struct cpu_process* current_process = NULL;

/*
    And finally, we're creating a process table/task-list. As we've pre-allocated a stack per task,
    plus the size for the control structs, we're going to try and keep it small,
    so let's say 4 tasks maximum.

    You most likely have more than enough RAM on a SAMD51 board to do 150 or so tasks with 256-word stacks, though.

    Being 4 also allows us to do a trick later where we do an AND 0x03 to wrap the index back to 0 after an increment,
    we could have 8 and &0x7 as well, but for now let's keep it simple.
 */

#define NUM_PROCESSES (4)
static struct cpu_process proc_list[NUM_PROCESSES];

/* ~~~~~~~~~~~~~~~~ PROCESS CONTROL CODE ~~~~~~~~~~~~~~~~ */

/* some simple functions for swapping pids to process list indexes and v.v. */
int __attribute__((inline)) pidx_to_pid(int pidx)
{
    /*
        Exercise: Why can't we assume a relationship between the index of processes in our array and the pid?
        Think about what you may have to do with a disk if you start running out of RAM.
    */

    if (pidx < 0 || pidx >= NUM_PROCESSES)
        return -1;
    return proc_list[pidx].p_id;
}

int __attribute__((inline)) pid_to_pidx(int pid)
{
    for (int pidx = 0; pidx < NUM_PROCESSES; pidx++)
    {
        /* Exercise: why do we have to check that it is unused? */
        if (proc_list[pidx].p_id == pid && proc_list[pidx].p_state != PROCESS_UNUSED)
        {
            return pidx;
        }
    }
    return -1;
}

/* finds the first free process in the list, returns -1 if none found, or index (pidx) if found */
int find_free_process()
{
    /* for each process in the list */
    for (int i = 0; i < NUM_PROCESSES; i++)
    {
        /* if the process is unused, then we can use this one */
        if (proc_list[i].p_state == PROCESS_UNUSED)
        {
            return i;
        }
    }
    return -1;
}

/* declaration only: schedule the next task */
int schedule();

/* yielding is just asking the scheduler to check for the next process, so no need for more than a macro */
#define yield_process() schedule()
/*
    On Arm, you also have a dedicated interrupt PendSV which is specifically designed to allow for context switches
    when you have FIFO/Co-op multitasking or when you want to yield while no other tasks are running.

    But we're not going to worry about setting that up right now, because it adds a lot of extra complication.

    On a pre-emptive system, we can use the SysTick interrupt to produce the interrupt for context switches, e.g.
    for a Round Robin scheduler like the original Linux 0.01 scheduler:
    https://github.com/zavg/linux-0.01/blob/master/kernel/sched.c
*/

/* create a process that will run the function passed, returns new process id or -1 if error */
int create_process(char* name, proc_function_t func, int a1, int a2, int a3, int a4)
{
    /* try to find a process, if unavailable, return with error. */
    int pidx = find_free_process();
    if (pidx == -1)
        return -1;

    /*
        We're going to do a pre-increment to create the pid, as we're going to keep 0 free to
        avoid conflict with a typical "0" is blank, and because the main function is
        technically process 0.
     */
    proc_list[pidx].p_id = ++total_processes;

    /* copy the name in */
    proc_list[pidx].p_name[0] = 0;
    strncpy(proc_list[pidx].p_name, name, PROC_NAME_SIZE);

    /* clear the swappable registers */
    memset(&proc_list[pidx].p_context, 0, sizeof(struct cpu_context));

    /* prep the args to be used on first launch */
    proc_list[pidx].p_context.a1 = a1; /* Yes, a1 and not a0! */
    proc_list[pidx].p_context.a2 = a2; /* That is because a1-a4 are alternative names */
    proc_list[pidx].p_context.a3 = a3; /* for arm registers r0-r3 when used as arguments */
    proc_list[pidx].p_context.a4 = a4; /* and it just makes it all match up a little neater */

    /* set the function as the first program address to be executed */
    proc_list[pidx].p_context.pc = (int)func;

    /*
        We also set the link register to the same address so that returning from the task switcher jumps us to
        the same pc.
     */
    proc_list[pidx].p_context.lr = (int)func;

    /*
        On a preemptive system, you usually have the saved pc/lr point to a kernel function that re-enables the
        scheduler and then returns to jump to the saved pc in a different cpu_context register.

        If you've got a regular kernel where tasks aren't just compiled-in function pointers, then
        it's basically the same except you set the pc/lr to the address of the program copied into RAM
    */

    /* clear the stack */
    memset(&proc_list[pidx].p_stack, 0, PROC_STACK_SIZE * sizeof(int));

    /* set the stack pointer (works top down) to the end of the process stack */
    proc_list[pidx].p_context.sp = (int)&proc_list[pidx].p_stack[PROC_STACK_SIZE - 1];

    /* mark as now runnable */
    proc_list[pidx].p_state = PROCESS_RUNNABLE;

    /* Exercise: if the scheduler was pre-emptive, how could we stop this same process being reused by a different thread calling this same function before we finish? */

    /* and increment the number of running processes */
    running_processes++;

    /* Exercise: why don't we jump/swap into the new process right now? */

    /* and return the process id */
    return proc_list[pidx].p_id;
}

/* swap one program context with another */
extern "C" void __attribute__((noinline)) __attribute__((naked)) swap_registers(struct cpu_context* old_context, struct cpu_context* new_context)
{
    /*
        Exercise: how does this differ from Linux, and why?
     */

    __asm(
      /* "ip" is the "intraprocedure register", and is r12, so you can see why we don't save/recall it */
      "add    ip, r0, #16\t\n"     /* put the old process into IP, then add the ofset to the first saveable register */
      "stmia  ip!, {r4 - r11}\t\n" /* store the registers r4-r11 into the address in ip */
      "add    ip, ip, #4\r\n"      /* skip ip passed the context version of r12 */
      "str    sp, [ip], #4\t\n"    /* store the stack pointer */
      "str    lr, [ip], #4\t\n"    /* store the link register */

      /*
          Wait? so we don't store the program counter?
          Nope, because the pc will be automatically restored with the link register on return.
          There's no point to setting the pc to inside this function, just to have it wiped out on return
      */

      "add    ip, r1, #0\t\n"      /* put the new process into r4 and add offset to first callable register */
      "ldmia  ip!, {r0 - r11}\t\n" /* load all the saved registers into real registers */
      "add    ip, ip, #4\r\n"      /* skip ip passed the context version of r12 */
      "ldr    sp, [ip], #4 \t\n"   /* load the stack register from the context */
      "ldr    lr, [ip], #4 \t\n"   /* and the link register (return address) */
      "bx     lr\t\n"              /* and branch/return back to the new link register address */
    );

    /*
        Implementations of this for desktop arm architectures usually have to load/store registers
        one-by-one, but we've made use of stmia and ldmia calls which aren't always available on those.
        See https://developer.arm.com/documentation/dui0068/b/BABEFCIB
    */
}

/* swap a program context with itself to grab an updated frame - we haven't used this in the end */
extern "C" void __attribute__((inline)) __attribute__((naked)) save_registers(struct cpu_context* context)
{
    swap_registers(context, context);

    /* Exercise: why does this work? */
}

/* load a new process by pid */
int load_process(int pid)
{
    int pidx = pid_to_pidx(pid);

    /* if pid doesn't exist, return error */
    if (pidx < 0)
        return -1;

    /* if we're trying to swap to the same process, don't */
    if (current_process->p_id == pid)
    {
        return -1;
    }

    /* if trying to load a process that isn't in a runnable state, error */
    if (proc_list[pidx].p_state != PROCESS_RUNNABLE)
    {
        return -1;
    }

    /* prep the context for swap */
    struct cpu_context* old_context = &current_process->p_context;

    /* set the current process to the new one */
    current_process = &proc_list[pidx];

    /* mark new process as running */
    current_process->p_state = PROCESS_RUNNING;

    /* swap contexts */
    swap_registers(old_context, &(proc_list[pidx].p_context));

    return 0;
}

/* exit a process (done from inside the process) */
int exit_process(int code)
{
    /* set return value */
    current_process->p_context.r0 = code;

    /* mark process as dead */
    current_process->p_state = PROCESS_DEAD;

    /*
        This is convention for many OSes and allows a parent thread to get
        return/exit codes and resource use information from a child.
    */

    running_processes--;

    /* and then we need to find and swap to a new process */
    yield_process();

    /* only if something has truly messed up can we have got here. */
    return -1;
}

/* basically exit, but called from a different process */
int kill_process(int pid)
{
    int pidx = pid_to_pidx(pid);

    if (pidx < 0)
        return -1;

    if (proc_list[pidx].p_state != PROCESS_RUNNABLE)
    {
        return -1;
    }

    /* He's dead, Jim. */
    proc_list[pidx].p_state = PROCESS_DEAD;

    running_processes--;

    /* and that's all we need to do. */
    return 0;
}

/* wait for a process to finish and then return it's return code */
int wait_process(int pid)
{
    int pidx = pid_to_pidx(pid);

    /* if pid doesn't exist, return error */
    if (pidx < 0)
    {
        Serial.println("Can't wait on non-existant process");
        return -1;
    }

    /* if trying to wait on a process that isn't in a runnable or dead state, error */
    if (proc_list[pidx].p_state == PROCESS_UNUSED)
    {
        Serial.println("Can't wait on an unused process");

        /* Exercise: why don't we check for PROCESS_RUNNING as well? */

        return -1;
    }

    /* wait for process to die */
    while (proc_list[pidx].p_state != PROCESS_DEAD)
    {
        yield_process();
    }

    /* and now we can mark it as unused */
    proc_list[pidx].p_state = PROCESS_UNUSED;

    /* Exercise: implement a way to check if more than one process is waiting on the same process, and stop it from marking it as unused prematurely */

    /* Exercise: add a check to the scheduler so that if a dead, but not unused, process isn't checked against a wait after so long then it marks it unused */

    /* and finally, we can return it's exit code (and you see part of why we kept r0 in our context) */
    return proc_list[pidx].p_context.r0;
}

/* find the next schedulable process and swap to it */
int schedule()
{
    /* NULL is a special case as it's the first launch of the schedule function */
    if (current_process == NULL)
    {
        /* try each process */
        for (int pidx = 0; pidx < NUM_PROCESSES; pidx++)
        {
            /* if there's a runnable one */
            if (proc_list[pidx].p_state == PROCESS_RUNNABLE)
            {
                Serial.print("Start first process: pid ");
                Serial.println(proc_list[pidx].p_id);

                /* set the current process to the new one */
                current_process = &proc_list[pidx];

                /* mark new process as running */
                current_process->p_state = PROCESS_RUNNING;

                /* We don't have to mark the current/old process as not running, because it's NULL */

                /* call function */
                ((proc_function_t)current_process->p_context.lr)(current_process->p_context.a1, current_process->p_context.a2, current_process->p_context.a3, current_process->p_context.a4);

                Serial.println("Failed on first call.");

                /* we shouldn't get here, because we'll have ended up inside the function loaded into the lr register of the context */
            }
        }
    }
    else
    {
        /* mark current as runnable and not running (If it was running) */
        if (current_process->p_state == PROCESS_RUNNING)
        {
            current_process->p_state = PROCESS_RUNNABLE;

            /* Exercise: why do we only change it if it was marked as running? */
        }

        /* the & 0x03 works to wrap from process 3 to process 0 because there are only 4 processes,
           so pidx has to be 00, 01, 10, or 11 */

        int pidx = (pid_to_pidx(current_process->p_id) + 1) & 0x3; /* start on next index */

        for (int loops; loops < NUM_PROCESSES; loops++)
        {
            /* Exercise: Implement a simple priority tester so that low priority tasks aren't loaded as often as high priority tasks */

            if (proc_list[pidx].p_state == PROCESS_RUNNABLE)
            {
                Serial.print("Switching to process ");
                Serial.println(proc_list[pidx].p_id);

                load_process(proc_list[pidx].p_id);
                return 0;
            }
            pidx = (pidx + 1) & 0x3;
        }

        /* Exercise: implement a clean way to handle no running/runnable processes left */
    }

    /* if we got here either there were no processes or swap failed, so return error */
    return -1;
}

/* ~~~~~~~~~~~~~~~~ PROCESS FUNCTION ~~~~~~~~~~~~~~~~ */

static volatile int rando_pid = 0;

/* print a bunch of random numbers, forever */
int rando_printo(int seed)
{
    Serial.println("Started rando printo");
    srand(seed);

    while (1)
    {
        Serial.print(current_process->p_name);
        Serial.print(": ");
        Serial.println((unsigned int)rand(), 16);
        delay(500);
        yield_process();
    }

    /* we shouldn't get here, but if we do then exit properly */
    exit_process(-1);
    return -1;
}

/* print a string char by char */
int char_by_char(const char* str)
{
    char* ptr = (char*)str;
    while (*ptr)
    {
        Serial.write(*ptr);
        ptr++;
        delay(250);
        yield_process();
    }
    exit_process(3);

    /* Exercise: Why doesn't return work for ending a thread? */
    return 3;
}

/* create a new process from inside a process, then wait for it to finish */
int create_from_process()
{
    int child_pid = create_process("char", (proc_function_t)char_by_char, (int)"Hello, world!", 0, 0, 0);

    if (child_pid == -1)
    {
        Serial.println("Error in create child process.");
    }

    Serial.println("Waiting for child to finish...");
    yield_process();

    /* wait for child to finish */
    int child_return = wait_process(child_pid);

    Serial.print("Child has returned with ");
    Serial.println(child_return);

    yield_process();

    int kill_return = kill_process(1);

    Serial.print("Killed a process, return code was ");
    Serial.println(kill_return);

    while (1)
    {
        yield_process();
    }

    exit_process(0);
    return 0;
}

/* Exercise: Create a process function that can check which other processes are running */

/* ~~~~~~~~~~~~~~~~ KERNEL SIDE CODE ~~~~~~~~~~~~~~~~ */

/* main... */
void setup()
{
    /* This test uses the terminal to show the multitasking, so we'll wait for a connection before continuing */
    while (!Serial)
    {
        /* we throttle this so that if the bootloader needs to be entered we can do that */
        delay(100);
    }

    Serial.println("Setting up processes");

    /* clear process list */
    memset((void*)&proc_list, 0, sizeof(struct cpu_process) * NUM_PROCESSES);

    /* Create a rando print process */
    rando_pid = create_process("Rando", (proc_function_t)rando_printo, 1, 0, 0, 0);

    /* and print if something went wrong */
    if (rando_pid < 0)
    {
        Serial.println("Error creating Rando process");
    }
    else
    {
        Serial.print("Rando process created on pid ");
        Serial.println(rando_pid);
    }

    /* Create a rando print process - again */
    rando_pid = create_process("Rando 2 ", (proc_function_t)rando_printo, 1, 0, 0, 0);

    /* and print if something went wrong */
    if (rando_pid < 0)
    {
        Serial.println("Error creating Rando 2 process");
    }
    else
    {
        Serial.print("Rando 2 process created on pid ");
        Serial.println(rando_pid);
    }

    int mother_pid = create_process("Double Dip", (proc_function_t)create_from_process, 0, 0, 0, 0);
    if (mother_pid < 0)
    {
        Serial.println("Error creating Double Dip process");
    }
    else
    {
        Serial.print("Double Dip process created on pid ");
        Serial.println(mother_pid);
    }

    /* and schedule the first process in */
    schedule();

    /* If we got here, something catastrophically failed */

    Serial.println("Error: premature schedular exit.");
}

/* This never executes because we jump straight into our first task, but we need to keep it to stop the compiler getting upset */
void loop()
{
}
