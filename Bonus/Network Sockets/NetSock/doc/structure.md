Interface Driver receives packet

Common code does some light dissection of packet at interface level
    Raw level before filtering handed to raw sockets
    Ethernet level handed to ethernet sockets
    IP level handed to inet sockets
    Known IP protocols also handed over with protocol type set

Common socket code will route/handle ethernet/IP level 

Packets also handed off to protocol switchways to be handled by those

Driver switchway scanned for polls
    |
     -> Polls trigger common IF code
        |
         -> Common IF code calls socket receive on PF_PACKET SOCK_RAW ETH_P_ANY (0)
            |
             -> Ethernet dissector checks whether packet can be processed, then calls socket receive on PF_PACKET SOCK_RAW ETH_P_ALL and the actual protocol, strips packet back to just ether layer
                |
                 -> IP dissector checks whether packet can be processed, then calls socket receive on PF_INET SOCK_RAW IPPROTO_IP and actual protocol
                    |
                     -> IP protocol code dissects to see if valid, strips packet back to just ip layer
                        |
                        -> Socket code finds sockets matching the family, type, and protocol
                           |
                           -> Calls final protocol's 'filter' which will call socket code to load data into socket

User calls to read a packet
    |
     -> Socket read buffer is read, or 0 length data, or errors are returned


User calls to create packet
    |
     -> Common socket code switches based on level (raw, ethernet, or inet)
        |
         -> if raw, then sends packet as is on bound interface
        |
         -> Socket code calculates list of matching interfaces
         -> Socket code creates base frame with ethernet header if > raw
            |
             -> Socket code calls protocol code to add headers for IP
             -> Socket code calls matching inner protocol code to add those headers
             -> Socket code adds write payload to frame
                |
                 -> Socket code calls interface drivers for list found above
                    |
                     -> Interface drivers send/reject packet as required
    |
     -> errors collated and returned
             
            
Read is faster than write.
