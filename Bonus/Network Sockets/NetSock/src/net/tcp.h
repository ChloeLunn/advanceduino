/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

#if !H_TCP
#define H_TCP 1

#include "if.h"

/*
V.OS only provides a header for TCP.
Implementation should be done in userspace.
*/

#ifndef __packed
#define __packed __attribute__((__packed__))
#endif

#ifndef __aligned
#define __aligned(x) __attribute__((__aligned__(x)))
#endif

/* ------------------------------------------------------------------------- */

#define TH_FIN  0x01
#define TH_SYN  0x02
#define TH_RST  0x04
#define TH_PUSH 0x08
#define TH_ACK  0x10
#define TH_URG  0x20
#define TH_ECE  0x40
#define TH_CWR  0x80

struct tcphdr {
    unsigned short th_sport; /* source port */
    unsigned short th_dport; /* destination port */
    unsigned int th_seq;     /* sequence number */
    unsigned int th_ack;     /* acknowledgement number */
    unsigned char th_off;    /* data offset (offset = (th_off >> 8 )+ th_off) */
    unsigned char th_flags;  /* flags */
    unsigned short th_win;   /* window */
    unsigned short th_sum;   /* checksum */
    unsigned short th_urp;   /* urgent pointer */
} __packed;

#define TCP_MAX_SEG  536  /* Max segment size (ipv4) */
#define TCP6_MAX_SEG 1220 /* Max segment size (ipv6) */

#define MAX_SACK_BLKS 6 /* Max # SACK blocks stored at receiver side */
#define TCP_MAX_SACK  4 /* MAX # SACKs sent in any segment */

struct tcpframe {
    struct ifhdr ifhead;       /* interface header (length in network order) */
    struct ether_header ehead; /* ethernet header */
    struct ip iphead;          /* ip header */
    struct tcphdr tcphead;     /* udp header */
    unsigned char buf[1];      /* packet buffer */
} __packed;

struct tcp6frame {
    struct ifhdr ifhead;       /* interface header (length in network order) */
    struct ether_header ehead; /* ethernet header */
    struct ip6 ip6head;        /* ip header */
    struct tcphdr tcphead;     /* udp header */
    unsigned char buf[1];      /* packet buffer */
} __packed;

/* ------------------------------------------------------------------------- */

#endif
