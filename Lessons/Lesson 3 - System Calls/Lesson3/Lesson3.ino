/*
Copyright 2022 Chloe Lunn

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
    This is example code showing how to implement basic system calls on a
    SAMD21 or SAMD51 device that is using the arduino IDE toolchain

    The liberal use of "extern "C"" is not an acciedent and is because the
    linker expects SVC and register access code to be in C for it to work
    correctly.

    This example implements a partially cooked TTY readline with a read
    system call to handle the read and print from the cooked buffer.

    It also uses the "write" system call to print a simple prompt.

    The TTY cooker uses a LF to indicate end of line, if you're on a system
    with CR newlines, then you can do an LF using CTRL + J

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! THIS CODE IS FOR ADAFRUIT-STYLE SAMD BOARDS AND WILL NOT RUN ON AVR-BASED !!
    !! ARDUINO BOARDS.                                                           !!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/

/*  ARM SVC and C register access references:
==============================================================

ARM Compiler armcc User Guide Version 5.06
https://developer.arm.com/documentation/dui0472/m/Compiler-Features/Compiler-support-for-accessing-registers-using-named-register-variables?lang=en

ARM: How to Write an SVC Function
https://developer.arm.com/documentation/ka004005/latest

*/

#include <Arduino.h>
#include <Variant.h>
#if defined(__CORTEX_M) && (__CORTEX_M == 0)
#include <core_cm0plus.h>
#elif defined(__CORTEX_M) && (__CORTEX_M == 4)
#include <core_cm4.h>
#endif
#include <string.h>

/* ~~~~~~~~~~~~~~~~ SVC CALLING CODE ~~~~~~~~~~~~~~~~ */

/*
    SVC calling code/number to use when producing SVC interrupt,
    this allows us to avoid other SVC calls that may be produced by
    libraries we're ignoring.

    In theory, this number can be the full 9-bit implicit value,
    but some compilers get upset if it's more than 4-bits (cos thumb).
    It also makes it easier to extract later if it's less than 8-bits
    (Don't forget this includes the sign bit!)
*/

static const int svc_call_code = 5;

/*
    Stack after SVC interrupt contains:

    [0] [1] [2] [3] [4]  [5] [6]  [7]
    r0, r1, r2, r3, r12, LR, ret, xPSR

    For our implementation, the first argument (r0)/[0] is sys call num
    Then syscall arguments in [1], [2], [3], r4, r5, r6

    This struct is used to allow us to point to these values with friendlier names.
*/
struct svc_context {
    int r0;
    int r1;
    int r2;
    int r3;
    int r12;
    int lr;
    int ret;
    int xPSR;

    /*
        On the hardware floating point implementations you then also have 17 more registers after this, s0-s15 + fpsr
        but as we're not using it, and you can usually just assume floating points are preserved through the call
        so we're not going to bother including them.
    */
};

/* This macro isn't actually used in the code, but it shows how we can embedd a simple SVC call into any C function */
#define SVC(code) asm volatile("svc %[immediate]" ::[immediate] "I"(code))

/* we'll predefine a bunch of tiny syscall numbers
   this is just to make it easier to read things later
 */
#define sys_ent_none  0
#define sys_ent_write 1
#define sys_ent_read  2
#define sys_ent_yeild 3
#define nsysent       4

/* ~~~~~~~~~~~~~~~~ TTY COOKER BUFFERS/VARS ~~~~~~~~~~~~~~~~ */

#define COOKED_MAX 100
char cooked_tty[COOKED_MAX];
char* cook = &cooked_tty[0];
volatile int cooked = 0;

/* ~~~~~~~~~~~~~~~~ KERNEL SIDE CODE (PART 1) ~~~~~~~~~~~~~~~~ */

/* Exercise: How would you implement a system call that expects a long long argument? */

/* kernel system write function (must be noinline to kill SVC interrupt) */
extern "C" int __attribute__((noinline)) sys_write(const char* string, int length)
{
    /*
        Exercise: What would happen if string had no terminating 0?
        Can you think of a better way to implement this function
    */

    int tlen = strlen(string);

    /* create a temporary buffer to force it to print no further than length */
    char pstring[length + 1];
    pstring[0] = 0;
    strncpy(pstring, string, length);

    /* set the terminating 0 to stop the printf from overflowing the buffer */
    pstring[length] = 0;

    /* and actually print the string */
    Serial.printf("%s", pstring);

    /* return the shortest length */
    if (tlen < length)
    {
        return tlen;
    }
    return length;
}

/* kernel system partially-cooked readline function (must be noinline to kill SVC interrupt) */
extern "C" int __attribute__((noinline)) sys_read(char* buf, int length)
{
    /*
        The real call would sleep the thread and close interrupt until data was available
        unless the NONBLOCK flag was set but we don't actually have a scheduler in this example,
        so we'll make do and assume non-blocking.
    */

    /* if there is a cooked line available */
    if (cooked)
    {
        /* read it into the passed buffer */
        int rlen = strlen(cooked_tty) + 1;
        strncpy(buf, cooked_tty, length);

        /* clear cooked */
        cook = &cooked_tty[0];
        *cook = 0;
        cooked = 0;

        /* return the shortest length */
        if (rlen < length)
        {
            return rlen;
        }

        return length;
    }

    return -1; /* return "error" if no cooked line available */
}

/* unroll syscall arguments, do the actual call, and then return */
extern "C" void SVC_Handler_Main(struct svc_context* context)
{
    /*
        System call arguments 4, 5, and 6 are stored in the registers,
        so we need to declare them now to avoid the compiler clobbering them with
        parts of this function or it's calls
    */
    register int _r4 __asm("r4");
    register int _r5 __asm("r5");
    register int _r6 __asm("r6");

    /* set SVC to 0 as temp */
    unsigned int svc_number = 0;

    /*
        Extract svc code from the value of instruction before return PC (should be "svc n")
        Unlike the ARM example for "full-fat" processors which casts to chars and loads
        the 8-bit value that way, we do this by loading the full 32-bit value and then
        shift/AND because the Cortex-M0/M0+ implementations often don't allow unaligned
        access.
        Doing it the arm example way is a 'neat' way to crash an M0/M0+ microcontroller.
    */
    svc_number = (((unsigned int*)((context->ret)))[-1] >> 16) & 0xFF;

    /* if we don't recognise it, then we can't process it, so quit but print an error */
    if (svc_number != svc_call_code)
    {
        /*
           In the real kernel, we would set the error code for the kernel and user to a number
           so that it can be checked and then printed with perror manually.
         */

        /* Exercise: Implement error codes, error strings, and a perror system call */

        Serial.printf("Error: Unknown SVC number (%x hex / %u dec)\r\n", svc_number, svc_number);
        context->r0 = -1;
        return;
    }

    /*
       The real kernel may do this for some "common" syscalls, but will actually use the sysent table index
       to find and call a function pointer without worrying about the specific function for each value for
       most calls.
     */
    switch (context->r0)
    {
    case sys_ent_write:
        context->r0 = sys_write((const char*)context->r1, (int)context->r2);
        break;
    case sys_ent_read:
        context->r0 = sys_read((char*)context->r1, (int)context->r2);
        break;
    default:
        /*
           In the real kernel, we would set the error code for the kernel and user to a number
           so that it can be checked and then printed with perror manually.
         */
        Serial.printf("Error: Unknown syscall (%x hex / %u dec)\r\n", context->r0, context->r0);
        context->r0 = -1;
        break;
    }

    /* explicit return, for clarity only */
    return;
}

/* handle receiving the system call interrupt */
extern "C" void SVC_Handler(void)
{
    /*
        Like all ARM interrupts, the SVC is only cleared once a branch with return link (BL)
        is called.
        However, we do this inside the SVC_Handler_Main function to avoid the interrupt clearing before we
        actually handle it.
        It can also break the SVC return as it then starts messing with the stack before we've processed it.
    */
#if defined(__CORTEX_M) && (__CORTEX_M == 4)
    /*
        In theory, this code should also work for any "full-fat" arm processor,
        However we don't have one so we're not going to worry about it for now.
        This tests the operating mode of the processor,
        copies the stack pointer for the current mode into the first argument,
        and then calls the actual handler.
    */
    __asm(
      ".global SVC_Handler_Main\n"
      "TST lr, #4\n"
      "ITE EQ\n"
      "MRSEQ r0, MSP\n"
      "MRSNE r0, PSP\n"
      "B SVC_Handler_Main\n");
#elif defined(__CORTEX_M) && (__CORTEX_M == 0)
    /*
        Cortex-M0 and M0+ are a lot simpler than M4s above,
        We don't have as many instructions available and often they only
        have the MSP implemented, and no process (protected) mode of
        operation.
        So we just need to load the stack pointer and call.
    */
    __asm(
      "MRS     R0, MSP\t\n" /* Read MSP */
      "B       SVC_Handler_Main\t\n");
#endif
}

/* read and partially cook a line from the tty into the cooked_tty buffer */
void ttycook()
{
    /* Exercise: Can you think of a better way to handle it if it's already been cooked? */
    if (!cooked)
    {
        /* Read a paritally-cooked line */
        while (Serial.available())
        {
            /* Read the available character */
            int r = Serial.read();

            /* if it was valid (-1 is invalid, 0 is ignored) */
            if (r > 0)
            {
                /* ASCII 7-bit only */
                *cook = r & 0xFF;

                /* if a printable, non-newline character */
                if (isprint(*cook) && *cook != '\r' && *cook != '\n')
                {
                    /* Echo the character back to the user */
                    Serial.printf("%c", *cook);
                    /* Increment the cook to the next character */
                    cook++;
                }

                /* if a new line */
                if (*cook == '\n')
                {
                    Serial.printf("\r\n"); /* echo back the newline with implicit CR */
                    *cook = 0;             /* set end of string */
                    cooked = 1;            /* flag buffer as cooked */
                    return;                /* exit cooker */
                }

                /* Exercise: What other cooking is needed on a tty? Can you implement them here? */

                /* if we've filled a buffer, overflow back to the beginning of it */
                if (&cooked_tty[COOKED_MAX] == cook)
                {
                    Serial.printf("\r"); /* do a CR to show visual confirmation of buffer wipeout */

                    /* Exercise: Implement a proper line clear here with as little code as possible */

                    /* then reset us back to a "blank" cooked tty */
                    cook = &cooked_tty[0];
                    *cook = 0;
                    cooked = 0;

                    /* Exercise: Can you think of a better way to handle a buffer overflow without losing data? */

                    /* hacky... print the prompt again as we just wiped it out... */
                    Serial.print("Read > ");
                    return;
                }
            }
        }
    }
}

/* ~~~~~~~~~~~~~~~~ USER SIDE APPLICATION CALLS ~~~~~~~~~~~~~~~~ */

/* syscall declaration */
extern "C" int __attribute__((noinline)) syscall(register int sysnum, register int a, register int b, register int c, int d, int e, int f);

/* write-like syscall */
extern "C" int write(const char* string, int length)
{
    /* call system with write call */
    int r = syscall(sys_ent_write, (int)string, (int)length, 0, 0, 0, 0);
    return r;
}

/* read-like syscall */
extern "C" int read(char* buf, int length)
{
    /* call system with read call */
    int r = syscall(sys_ent_read, (int)buf, (int)length, 0, 0, 0, 0);
    return r;
}

/* do actual syscall interrupt (copies args into registers, then triggers SVC) */
extern "C" __attribute__((noinline)) int syscall(register int sysnum, register int a, register int b, register int c, int d, int e, int f)
{
    /*
        The order of the registers in this function is critical, as is loading them in this order.
        The assembly/operations produced can't handle all the registers, or may optimise to avoid using them directly
        i.e. when we do things like _r6 = var, then it may actually change the value in _r0 (previously sysnum)

        So we load them up from highest to lowest, and then immediatley jump to the SVC interrupt to avoid any issues
    */

    /*
        On armv7 the calling convention is that R11 is used as the frame pointer, which means we could use R7 like the Linux kernel
        to do the system call number, and then put 6/7 arguments into registers 0-6.

        However, on armv6-m, on the Cortex-M0/M0+ processors, register 7 is used as the frame pointer because they don't have direct
        access to high-side registers in the same way, so we need to make sure that we don't use R7 if this code is to "just work"
        on both SAMD21 and SAMD51 chips.
     */

    register int _r6 __asm__("r6") = (f);
    register int _r5 __asm__("r5") = (e);
    register int _r4 __asm__("r4") = (d);
    register int _r3 __asm__("r3") = (c);
    register int _r2 __asm__("r2") = (b);
    register int _r1 __asm__("r1") = (a);
    register int _r0 __asm__("r0") = (sysnum);
    asm volatile("svc %[immediate]"
                 : "=r"(_r0)
                 : [immediate] "I"(svc_call_code), "r"(_r0), "r"(_r1), "r"(_r2), "r"(_r3), "r"(_r4), "r"(_r5), "r"(_r6)
                 : "memory");  //: "r0", "r1", "r2", "r3", "r4", "r5", "r7", "memory");

    /*
        If all has gone well, _r0 has been filled in from the svc_context in the handler,
        so we can return it now as the return from our syscall.
    */
    return _r0;
}

/* ~~~~~~~~~~~~~~~~ KERNEL SIDE CODE (PART 2) ~~~~~~~~~~~~~~~~ */

/*
    We've called this "kernel side", but in reality it's a mix
    of what you'd do user side and kernel side.

    It calls the tty cooker function, but also then uses our user-side
    system calls to actually read that.
*/

/* main... */
void setup()
{
    /* This test is all about using the tty, so we'll wait for a connection before continuing */
    while (!Serial)
    {
        /* we throttle this so that if the bootloader needs to be entered we can do that */
        delay(100);
    }

    /* we'll force a delay for a bit just for stability */
    delay(1000);

    /* reset the cooked tty vars */
    cook = &cooked_tty[0];
    *cook = 0;
    cooked = 0;

    /* and print a prompt using the write system call */
    write("Read > ", strlen("Read > "));
}

/* inifinite loop */
void loop()
{
    /* This function would normally be called on interrupt in kernel space. */
    ttycook();

    /* Use our system call to try and read a line from the tty into the buffer */
    char buf[COOKED_MAX];
    buf[0] = 0;
    int len = read(buf, COOKED_MAX);

    /* If we read a string, then print it! */
    if (len > 0)
    {
        Serial.printf("Read length: %i; Read string: %s\r\n", len, buf);
        write("Read > ", strlen("Read > "));

        /* Exercise: Is the read length correct? If yes, why? If no, can you change this print to do what you expected? */
    }

    /*
        We'll throttle the loop by 100ms each pass so that the SVC isn't constantly called
        Otherwise, the USB interrupt never gets handled for long enough...
     */
    delay(100);
}
