/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

/*
   Common network interface code

   These functions are called by network drivers when they need to do common handling

*/

#include "../ifcom.h"

#include "../../net/ether.h"
#include "../../net/if.h"
#include "../../net/ip.h"
#include "../../net/protocol.h"
#include "../../net/socket.h"
#include "../dev.h"

extern void debug_printline(const char* line);
extern void debug_print(const char* line);
extern void debug_dump(unsigned char* data, int len);

extern int sock_filter(int ps, int family, int type, int proto, struct iface* ifs, unsigned char* data);

#include <string.h>

#define println(s)
#define printf(...)

#define DEBUG_IFACES 0

int if_index = 1;

int if_flush(struct iface* ifs)
{
    /* shouldn't need to do anything at this layer */
    return 0;
}

int if_discard(struct iface* ifs)
{
    /* shouldn't need to do anything at this layer */
    return 0;
}

int if_ioctl(struct iface* ifs, int cmd, void* addr, int flag)
{
    return 0;
}

/*
    Finalises interface opening and sets a default name
*/
int if_init(unsigned short dev, struct iface* ifs)
{
    ifs->ifr_dev = dev;

    ifs->ifr_ifindex = if_index;
    if_index++;

    /* set up a default name */
    memset(ifs->ifr_name, 0, IFNAMSIZ);
    strcpy(ifs->ifr_name, "net");
    if (if_index < 10)
    {
        ifs->ifr_name[4] = '0' + (if_index - 1); /* names start from 0, index starts from 1 */
    }
    else
    {
        ifs->ifr_name[4] = '0' + ((if_index - 1) / 10);
        ifs->ifr_name[5] = '0' + ((if_index - 1) % 10);
    }

    ifs->ifr_flags |= IFF_RUNNING;

    ifs->ifr_status &= ~IFS_WOPEN;
    ifs->ifr_status |= IFS_ISOPEN;

    return 0;
}

/*
    Closes an interface
*/
int if_off(struct iface* ifs)
{
    /*
        flush transmit buffer, and discard anything in receive buffer
    */
    if_flush(ifs);
    if_discard(ifs);

    ifs->ifr_flags = 0;
    ifs->ifr_status = 0;

    return 0;
}

/*
    Loads packet information off the frame in the iface information, and then
    hands it off to the socket layer with some protocol layer hints
*/
int if_receive(struct iface* ifs, struct iframe* frame)
{
    /* if not marked up or running, then leave */
    if (!(ifs->ifr_flags & IFF_UP) || !(ifs->ifr_flags & IFF_RUNNING))
        return -1;

    /* save packet length */
    int rlen = ntohs(frame->ifhead.h_len);

    if (rlen > 0)
        rlen -= sizeof(struct ifhdr);

    /* this is bad. this means we received a packet longer than the mtu... */
    if (rlen > ifs->ifr_mtu)
    {
        if (ifs->ifr_flags & IFF_DEBUG)
            debug_printline("IF: Packet larger than MTU");

        return -1;
    }

    switch (ifs->ifr_type)
    {
    case IFT_ETHER:
    case IFT_WLAN:
        {
            // You can't have a socket at this level, so just send straight to dissector for ETH_ANY
            proto_dissect(PS_ETH_ANY, PF_PACKET, SOCK_RAW, htons(ETH_P_INTERNAL_ANY), ifs, (unsigned char*)ifs->ifr_data);
        }
        break;
    default:
        break;
    }

    return 0;
}

/*
    Checks if this frame can be sent on this interface.
    If not returns -1, if it can then it returns 0
*/
int if_transmit(struct iface* ifs, unsigned char* data, int len)
{
    /* if not marked up or running, then leave */
    if (!(ifs->ifr_flags & IFF_UP) || !(ifs->ifr_flags & IFF_RUNNING))
        return -1;

    if (len > ifs->ifr_mtu)
    {
        /* too large for mtu */
        return -1;
    }

    /* move the data back so that we can stick the ifhdr on it */
    struct ifhdr* ifdata = (struct ifhdr*)(data);
    unsigned char* payload = (unsigned char*)(data + sizeof(struct ifhdr));
    int payload_len = len + sizeof(struct ifhdr);

    memmove(payload, data, len);

    /* set the length in the frame header */
    ifdata->h_len = htons(payload_len);

    if (len > 0)
    {
        return dev_netwrite(ifs->ifr_dev, (struct iframe*)data);
    }

    return 0;
}

/* just builds a list of iface pointers from dev sw */
int if_list(struct iface** ifs, int len)
{
    int added = 0;
    /* foreach driver */
    for (int i = 0; added < len && i < DEV_MAJOR_MAX; i++)
    {
        /* if this driver has a list of interfaces */
        if (netsw[i].nw_ifs)
        {
            struct iface** sp = &netsw[i].nw_ifs[0];
            struct iface* s;
            /* foreach interface */
            while ((s = *sp++) && added < len)
            {
                /* add it */
                ifs[added++] = s;
            }
        }
    }
    return 0;
}
