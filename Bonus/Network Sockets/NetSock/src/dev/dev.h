/*
V.OS Open Source License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use of this software in source and binary forms, with or
without modification, are permitted for any use and free of charge provided
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of the distribution's
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modifications to a redistribution covered under these licensing terms
   must be provided freely and free of charge in source form to any party
   who requests the modifications. Only the modified sections of this
   software need to be made available if the redistribution is included in a
   larger work.

5. Any modifications to a redistribution covered under these licensing terms
   must be published using this same license. Only the modified sections of
   this software need to be published using this license if the
   redistribution is included in a larger work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This license is a modification of The BSD Licence to more closely follow the
terms of the MPL-2.0, but may be incompatible in some cases.
Please follow the license terms carefully.
 */

/*
    Device driver switchway
*/

#if !H_DEV
#define H_DEV 1

#include "../net/if.h"

#define makedev(maj, min) ((((maj) & 0xFF) << 8) | ((min) & 0xFF))
#define major(dev)        (((dev) >> 8) & 0xFF)
#define minor(dev)        (((dev) >> 0) & 0xFF)

typedef int (*netinit_t)(unsigned short dev, int flag, unsigned short mode);
typedef int (*netoff_t)(unsigned short dev, int flag);
typedef int (*netwrite_t)(unsigned short dev, const struct iframe* frame);
typedef int (*netioctl_t)(unsigned short dev, int cmd, void* addr, int flag);
typedef int (*netpoll_t)(unsigned short dev, int flag);
typedef int (*netlnk_t)(unsigned short dev, int flag);

struct netdevsw {
    netinit_t nw_init;
    netoff_t nw_off;
    netwrite_t nw_write;
    netioctl_t nw_ioctl;
    netpoll_t nw_poll;
    netlnk_t nw_lnk;
    struct iface** nw_ifs;
};

#define DEV_MAJOR_CON (0) /* 0 is used for 'any'/invalid and so needs to be reserved */
#define DEV_MAJOR_WZ  (1)
#define DEV_MAJOR_MAX (2)

extern struct netdevsw netsw[DEV_MAJOR_MAX];

extern int dev_init(unsigned short dev, int flag, unsigned short mode);
extern int dev_poll(unsigned short dev, int flag);
extern int dev_lnk(unsigned short dev, int flag);
extern int dev_ioctl(unsigned short dev, int cmd, void* addr, int flag);
extern int dev_netwrite(unsigned short dev, const struct iframe* frame);

#endif
